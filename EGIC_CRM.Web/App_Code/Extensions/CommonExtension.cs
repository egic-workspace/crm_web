﻿using System.Data;
using System.Web.UI.WebControls;

public static class CommonExtension
{
    //public static void FillDataSource(this DropDownList ddl, DataSet data)
    //{
    //    FillDataSource(ddl,data.Tables[0]);
    //}

    public static void FillDataSource(this DropDownList ddl, DataTable data = null)
    {
        if (data != null)
            ddl.DataSource = data;
        else

            ddl.Items.Clear();

        ddl.DataTextField = "Text";
        ddl.DataValueField = "Value";
        ddl.DataBind();

        ddl.Items.Insert(0, new ListItem("اختر...", ""));
    }

    public static void SetSelectedValue (this DropDownList ddl, object selectedValue)
    {
        try
        {
            if (selectedValue != null && !string.IsNullOrWhiteSpace(selectedValue.ToString()))
                ddl.SelectedValue = selectedValue.ToString();
            else
                ddl.SelectedIndex = 0;
        }
        catch
        {
            ddl.SelectedValue = "";
        }
    }
}