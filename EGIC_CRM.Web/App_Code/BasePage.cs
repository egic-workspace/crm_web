﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for BasePage
/// </summary>
public class BasePage : System.Web.UI.Page
{
    public string CurrentUsername { get { return Session[AppConsts.CURRENT_USERNAME] != null ? Session[AppConsts.CURRENT_USERNAME].ToString() : string.Empty; } }


    public BasePage():base()
    {
    }

}