﻿using System;
using System.Data;

public class LookupService : BaseService
{

    #region CTOR

    public LookupService() : base()
    {
    }

    #endregion

    #region Methods

    public DataTable GetAllBranches()
    {
        try
        {
            SQL = "SELECT BRANCH Value, BRANCH Text FROM BRANCHS";
            return GetDataAsDataTable(SQL);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, nameof(GetAllBranches));
        }
        return null;
    }

    public DataTable GetAllInspectionsDescriptions()
    {
        try
        {
            SQL = "SELECT Name Value, Name Text FROM EG_V_DESC";
            return GetDataAsDataTable(SQL);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, nameof(GetAllBranches));
        }
        return null;
    }

    //public DataTable GetAllInspectionsTypes()
    //{
    //    try
    //    {
    //        SQL = "SELECT Code Value, Data_Text Text FROM EG_BASIC_DATA WHERE DATA_TYPE = 'PLACE_TYPE'";
    //        return GetDataAsDataTable(SQL);
    //    }
    //    catch (Exception ex)
    //    {
    //        LogError(ex.Message, nameof(GetAllBranches));
    //    }
    //    return null;
    //}

    public DataTable GetAllInspectionsPlacesTypes()
    {
        try
        {
            SQL = "SELECT Code Value, Code Text FROM BASIC_CODES WHERE C_TYPE = 'مكان المعاينة' ORDER BY 1";
            return GetDataAsDataTable(SQL);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, nameof(GetAllBranches));
        }
        return null;
    }

    public DataTable GetAllGovernorates()
    {
        try
        {
            SQL = "SELECT CODE Value, NAME Text FROM GOVS";
            return GetDataAsDataTable(SQL);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, nameof(GetAllBranches));
        }
        return null;
    }

    public DataTable GetAllCities(int governorateCode)
    {
        try
        {
            SQL = $"SELECT CODE Value, NAME Text FROM CITIES WHERE GOV_CODE = {governorateCode}";
            return GetDataAsDataTable(SQL);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, nameof(GetAllBranches));
        }
        return null;
    }

    public DataTable GetAllDistricts(int cityCode)
    {
        try
        {
            SQL = $"SELECT CODE Value, NAME Text FROM DISTRICT WHERE CITY_CODE = {cityCode}";
            return GetDataAsDataTable(SQL);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, nameof(GetAllBranches));
        }
        return null;
    }

    public DataTable GetAllShops(string shopName = null, int shopCode = 0, int governorateCode = 0, int cityCode = 0, int districtCode = 0)
    {
        try
        {
            SQL = $"SELECT CODE Value, NAME Text FROM EG_SHOPS WHERE (CODE > 0 {(governorateCode > 0 ? $" AND GOV_CODE = {governorateCode}" : "")} {(cityCode > 0 ? $" AND CITY_CODE = {cityCode}" : "")} {(districtCode > 0 ? $" AND DIST_CODE = {districtCode}" : "")} {(!string.IsNullOrWhiteSpace(shopName) && shopName.Length > 1 ? $" AND NAME like '%{shopName}%'" : "")} {(shopCode > 0 ? $" ) OR CODE = {shopCode}" : ")")} ";
            return GetDataAsDataTable(SQL);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, nameof(GetAllBranches));
        }
        return null;
    }

    public DataTable GetAllGroups(int governorateCode = 0, int cityCode = 0, int districtCode = 0)
    {
        try
        {
            SQL = $"SELECT CODE Value, GROUP_NAME Text FROM EG_SALES_REPS WHERE GROUP_NAME IS NOT NULL AND NAME IS NOT NULL ORDER BY GROUP_NAME";
            return GetDataAsDataTable(SQL);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, nameof(GetAllBranches));
        }
        return null;
    }

    public string GetSalesRepName(int groupCode)
    {
        try
        {
            SQL = $"SELECT NAME FROM EG_SALES_REPS WHERE GROUP_NAME IS NOT NULL AND NAME IS NOT NULL AND CODE = {groupCode}";
            var data = _oracleAcess.ExecuteScalar(SQL);
            if (data != null)
            {
                return data.ToString();
            }
        }
        catch (Exception ex)
        {
            LogError(ex.Message, nameof(GetAllBranches));
        }
        return null;
    }

    public DataTable GetAllGroupsManagers(int groupCode)
    {
        try
        {
            SQL = $"SELECT CODE Value, NAME Text FROM EG_GROUP_MANG WHERE NAME IS NOT NULL AND SREP_CODE = {groupCode}";
            return GetDataAsDataTable(SQL);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, nameof(GetAllBranches));
        }
        return null;
    }

    public DataTable GetAllInspectionReps(int managerCode = 0)
    {
        try
        {
            SQL = $"SELECT CODE Value, NAME Text FROM EG_REPS WHERE NAME IS NOT NULL AND CODE > 0 {(managerCode > 0 ? $"AND MANG_CODE = {managerCode}" : "")}";
            return GetDataAsDataTable(SQL);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, nameof(GetAllBranches));
        }
        return null;
    }

    #endregion
}