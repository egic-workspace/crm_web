﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

/// <summary>
/// Summary description for UserService
/// </summary>
public class UserService : BaseService
{
    #region CTOR

    public UserService():base()
    {
    }

    #endregion

    #region Utilities

    #endregion

    #region Methods

    public List<MainMenuItem> GetUserModules(string username)
    {
        if (string.IsNullOrWhiteSpace(username))
            throw new ArgumentNullException("username");

        string sql =
            string.Format(
                @"SELECT 
                    APP_DESC, 
                    APP_NAME,
                    APP_LINK 
                FROM 
                    APPS 
                WHERE 
                    (APP_NAME IN(SELECT APP_NAME FROM MODULES WHERE MODULE_TYPE IN('W','I') AND (MODULE_NAME IN(SELECT MODULE_NAME FROM USER_MOD WHERE USER_NAME = UPPER('{0}')) OR UPPER('{0}') = 'APP')) 
                    AND STATUS = 'N' AND EXISTS (SELECT 1 FROM USERS WHERE USER_NAME = UPPER('{0}') AND ( NVL(STATUS,'N') = 'N' OR UPPER('{0}') = 'APP')))
                ORDER BY 1
"
        , username);

        List<MainMenuItem> items = new List<MainMenuItem>();
        var data = _oracleAcess.ExecuteDataTable(sql);
        if (data.Rows.Count > 0)
            foreach (DataRow item in data.Rows)
                items.Add(new MainMenuItem()
                {
                    Text = item["APP_DESC"].ToString(),
                    URL = item["APP_NAME"].ToString()
                });
        return items;
    }

    public List<MainMenuItem> GetUserMainMenuItems(string username, string moduleName)
    {
        if (string.IsNullOrWhiteSpace(username) || string.IsNullOrWhiteSpace(moduleName))
            throw new ArgumentNullException("username/moduleName");

        string sql =
            string.Format(
                @"SELECT 
                    MENU_LABEL, 
                    MODULE_DESC TEXT, 
                    APP_NAME, 
                    MODULE_NAME, 
                    MODULE_LINK URL
                FROM 
                    MODULES 
                WHERE 
                    MENU_LABEL IS NOT NULL 
                AND ( ( MODULE_NAME IN( SELECT MODULE_NAME FROM USER_MOD WHERE USER_NAME = UPPER('{0}') AND MODULE_TYPE = 'W' )) OR (UPPER('{0}') = 'APP' AND MODULE_TYPE IN('I','W')))
                AND APP_NAME = '{1}' ORDER BY MENU_LABEL"
        , username, moduleName);

        List<MainMenuItem> items = new List<MainMenuItem>();
        var data = _oracleAcess.ExecuteDataTable(sql);
        if (data.Rows.Count > 0)
            foreach (DataRow row in data.Rows)
            {
                string menuLabel = row["MENU_LABEL"].ToString();
                var menuItem = items.FirstOrDefault(x => x.Text == menuLabel);
                if (menuItem != null)
                    menuItem.SubItems.Add(new MainMenuItem
                    {
                        Text = row["Text"].ToString(),
                        URL = row["MODULE_NAME"].ToString()
                    });
                else
                    items.Add(new MainMenuItem()
                    {
                        Text = row["MENU_LABEL"].ToString(),
                        SubItems = new List<MainMenuItem> {
                            new MainMenuItem
                            {
                                Text = row["Text"].ToString(),
                                URL = row["MODULE_NAME"].ToString()
                            }
                        }
                    });
            }
        return items;
    }

    public string ValidateCredential(string username, string password)
    {
        if (string.IsNullOrWhiteSpace(username) || string.IsNullOrWhiteSpace(password))
            return "يجب ادخال اسم المستخدم وكلمة المرور";

        string sql = string.Format("SELECT CHECK_LOGED_USER('{0}','{1}') FROM DUAL", username, password);
        string result = _oracleAcess.ExecuteScalar(sql).ToString();

        int _result = 0;
        if (!int.TryParse(result, out _result) || _result == 0)
            return "برجاء التحقق من اسم المستخدم وكلمة المرور";

        return string.Empty;
    }

    public List<MainMenuItem> GetUserMainMenuItemsAIO(string username)
    {
        if (string.IsNullOrWhiteSpace(username))
            throw new ArgumentNullException("username");

        string sql =
            string.Format(
                @"SELECT 
                    A.APP_DESC MODULE_NAME,
                    M.MENU_LABEL MAIN_MENU_TEXT,
                    MODULE_DESC MENU_ITEM_TEXT,
                    MODULE_LINK MENU_ITEM_URL
                FROM 
                    MODULES M , 
                    APPS A
                WHERE 
                    M.APP_NAME = A.APP_NAME
                    AND MENU_ITEM IS NOT NULL
                    AND ( (MODULE_NAME IN( SELECT MODULE_NAME FROM USER_MOD WHERE USER_NAME = UPPER('{0}'))
                    AND MODULE_TYPE = 'W') 
                    OR (UPPER('{0}') = 'APP' AND MODULE_TYPE IN('I','W') ))
                    AND MENU_LABEL IS NOT NULL
                ORDER BY 1,2"
        , username);

        List<ModuleRow> items = new List<ModuleRow>();
        var data = _oracleAcess.ExecuteDataTable(sql);
        if (data.Rows.Count > 0)
            foreach (DataRow row in data.Rows)
                items.Add(new ModuleRow(row));

        List<MainMenuItem> itemsI = new List<MainMenuItem>();
        List<MainMenuItem> itemsII = new List<MainMenuItem>();
        List<MainMenuItem> itemsIII = new List<MainMenuItem>();


        foreach (var itemI in items.GroupBy(g1 => g1.ModuleText))
        {
            itemsII = new List<MainMenuItem>();
            foreach (var itemII in itemI.GroupBy(g2 => g2.MainMenuText))
            {
                itemsIII = new List<MainMenuItem>();
                foreach (var itemIII in itemII)
                    itemsIII.Add(new MainMenuItem { Text = itemIII.MenuItemText, URL = itemIII.MenuItemUrl });

                itemsII.Add(new MainMenuItem { Text = itemII.Key, SubItems = itemsIII });
            }
            itemsI.Add(new MainMenuItem { Text = itemI.Key, SubItems = itemsII });
        }

        return itemsI;
    }

    #endregion
}