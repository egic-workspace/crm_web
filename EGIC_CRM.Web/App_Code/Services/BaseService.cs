﻿using System;
using System.Configuration;
using System.Data;
using System.IO;

/// <summary>
/// Summary description for BaseService
/// </summary>
public class BaseService
{

    internal const string DATE_FORMAT_ORECAL = "yyyy/mm/dd hh24:mi";
    internal const string DATE_FORMAT_NET = "yyyy/MM/dd HH:mm";

    #region Fields

    internal readonly OracleAcess _oracleAcess;
    internal string SQL = "";

    private DataSet Ds = new DataSet();
    private DataTable dt = new DataTable();

    #endregion

    #region CTOR

    public BaseService()
    {
        string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        _oracleAcess = new OracleAcess(connectionString);
    }

    #endregion

    #region Internal Functions

    internal DataSet GetDataAsDataSet(string sqlQuery)
    {
        Ds.Clear();
        dt = _oracleAcess.ExecuteDataTable(sqlQuery);
        Ds.Tables.Add(dt);
        return Ds;
    }

    internal DataTable GetDataAsDataTable(string sqlQuery)
    {
        return _oracleAcess.ExecuteDataTable(sqlQuery);
    }

    internal void LogError(string msg, string functionName)
    {
        StreamWriter sr = new StreamWriter(@"c:\LogError.txt", true);
        string txt = "Time: " + DateTime.Now.ToString() + "\r\n";
        txt += "Function Name: " + functionName + "\r\n";
        txt += "Error Message: " + msg + "\r\n";
        txt += "================================= *** =================================\r\n";
        sr.WriteLine(txt);
        sr.Close();
    }

    #endregion
}