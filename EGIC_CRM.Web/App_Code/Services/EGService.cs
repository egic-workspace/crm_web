﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Text;

/// <summary>
/// Summary description for EGService
/// </summary>
public class EGService : BaseService
{
    #region CTOR

    public EGService() : base()
    {
    }

    #endregion

    #region Methods

    #region query

    public DataSet Bind_Query_Grid()
    {
        try
        {
            SQL = "select name , active , decode (active ,'Y' ,'نشط', 'N', 'غير نشط') as active_desc , decode (qry_type ,'C' ,'شكوي', 'Q', 'استفسار' , 'S' , 'اقتراح') as qry_type_desc , qry_type , serv_dept , decode (send_mail ,'Y' ,'تم الإرسال', 'N', 'لم يتم الإرسال') as sendM_desc , send_mail , comp_class from eg_query_types where rownum < 100    ";
            return GetDataAsDataSet(SQL);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_Query_Grid >> Fill qury types to GridView");
        }
        return null;
    }

    public int Save_Query_Types(string v_name, string v_active, string v_qry_type, string v_serv_dept, string v_send_mail, string v_comp_class)
    {
        int val = 0;
        try
        {
            SQL = " insert into eg_query_types (name, active, qry_type, serv_dept, send_mail, comp_class) ";
            SQL += " values  ( '" + v_name + "', '" + v_active + "', '" + v_comp_class + "' , '" + v_serv_dept + "' , '" + v_send_mail + "','" + v_qry_type + "' ) ";
            val = _oracleAcess.ExecuteNonQuery(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, " Save_Query_Types ");
            val = 0;
        }

        return val;
    }

    public int Update_Query_Types(string P_name, string v_name, string v_active, string v_qry_type, string v_serv_dept, string v_send_mail, string v_comp_class)
    {
        int val = 0;
        try
        {
            SQL = " Update eg_query_types set name = '" + v_name + "' , active = '" + v_active + "' , qry_type ='" + v_comp_class + "', serv_dept = '" + v_serv_dept + "', send_mail ='" + v_send_mail + "', comp_class = '" + v_qry_type + "' where ( name = '" + P_name + "' ) ";
            val = _oracleAcess.ExecuteNonQuery(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, " Update_Query_Types ");
            val = 0;
        }

        return val;
    }

    public int Delete_Query_Types(string v_name, string v_active)
    {
        int val = 0;
        try
        {
            SQL = " delete from eg_query_types where name =  '" + v_name + "' and  active = '" + v_active + "' ";

            val = _oracleAcess.ExecuteNonQuery(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, " Delete_Query_Types ");
            val = 0;
        }

        return val;
    }

    #endregion

    #region inspections

    public DataTable BindViewingTree(string mob, int ronum = 21)
    {
        try
        {
            SQL = " SELECT CUST_CODE,CUST_NAME,PARENT FROM( SELECT C.CODE CUST_CODE,C.NAME CUST_NAME , null AS PARENT FROM EG_CUSTOMERS C WHERE (C.TEL = '" + mob + "' OR C.TEL2 = '" + mob + "' OR C.MOBILE = '" + mob + "') UNION ALL SELECT ORDER_CODE, to_char(ORDER_CODE) ,CUST_CODE FROM ( SELECT CODE ORDER_CODE,OWNER_NAME ,CUST_CODE  FROM EG_VIEWING WHERE CUST_CODE IN(SELECT CODE FROM EG_CUSTOMERS C2 WHERE (C2.TEL = '" + mob + "' OR C2.TEL2 = '" + mob + "' OR C2.MOBILE = '" + mob + "')) ORDER BY CODE DESC) ) WHERE ROWNUM <= " + ronum + " ";
            return GetDataAsDataTable(SQL);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_Viewing_Tree <<");
        }
        return null;
    }

    // inspections
    public DataTable GetAllInspectionData(string code)
    {
        if (int.TryParse(code, out int _code) && _code > 0)
            return GetAllInspectionData(_code);
        return null;
    }

    public DataTable GetAllInspectionData(int code = 0, string owner_mob = null)
    {
        try
        {
            SQL = $@"SELECT 
                        v.CODE inspection_code,
                        v.CUST_CODE,
                        c.NAME CUST_NAME,
                        v.CODE||' - '||nvl(v.OWNER_NAME,v.PLN_DATE) inspection_name,
                        v.ADDRESS inspection_address,
                        v.GROUP_MANG_CODE,
                        v.REP_CODE inspection_rep_code,
                        v.PLN_DATE inspection_planed_date,
                        v.FROM_TIME,
                        v.TO_TIME,
                        v.FEEDBACK,
                        v.POINTS,
                        v.REC_DATE inspection_rec_date,
                        v.USER_NAME inspection_creator,
                        v.GUR_STATUS,
                        v.GUR_REASON grantee_status_reason,
                        v.GOV_CODE inspection_gov_code,
                        v.CITY_CODE inspection_city_code,
                        v.TOTAL_POINTS,
                        v.GRU_DATE,
                        v.V_SOURCE inspection_source,
                        v.OWNER_NAME,
                        v.COMP_ADDRESS,
                        v.TEL owner_tel,
                        v.FAX,
                        v.MOB owner_mobile,
                        v.NOTES inspection_notes,
                        v.V_DESC inspection_description,
                        v.PRN_NO inspection_printed_NO,
                        v.SHOP_CODE buy_shop_code,
                        v.CALLER_NAME,
                        v.CALLER_TEL,
                        v.VIEW_STATUS,
                        v.CALLER_TYPE,
                        v.REP_POINTS,
                        v.V_YEAR inspection_year,
                        v.CERT_REC,
                        v.PRINTED,
                        v.DIST_CODE inspection_dist_code,
                        v.ST_NOTES inspection_status_note,
                        v.DE_NOTES data_entry_notes,
                        v.ORDER_STATUS,
                        v.CANCEL_REASON inspection_reject_reason,
                        v.DET_ORDER,
                        v.PARENT_NO,
                        v.PRINT_REQ,
                        v.PDA_STATUS,
                        v.VIEW_SERIAL,
                        v.REP_REJECT_REASON,
                        v.ESTKMAL_CODE,
                        v.SALES_CODE,
                        v.LAST_UPDT,
                        v.SV_CODE,
                        v.SHOP_NOTE,
                        v.VIEW_LEVEL,
                        v.REV_NOTES,
                        v.STOP_CERT stop_printing_reoprts,
                        v.STOP_RSN stop_printing_reoprts_reason,
                        v.GV_CODE,
                        v.SUPER_CODE,
                        v.CERT_REQ_STATUS,
                        v.WC_CNT,
                        v.KITCHEN_CNT,
                        v.VIEW_TYPE,
                        v.BY_REP,
                        v.QA_REP_NAME inspection_qa_resp_name,
                        v.SEND_TIME,
                        v.BRANCH inspection_branch,
                        v.SETUP_COMP,
                        v.KES_TYPE,
                        v.AREA_NAME,
                        v.VIEW_SITUATION inspection_situation,
                        v.CC_SHOP_CODE,
                        v.CC_MAX_SHOP,
                        v.CC_OTHER_SHOP,
                        v.HASR_STATUS,
                        v.HASR_REASON,
                        v.JOKER_NOTES,
                        v.SERV_NO,
                        v.ACTUAL_DATE,
                        v.ORG_CODE,
                        v.ORG_GUR_STATUS,
                        v.PLACE_TYPE,
                        v.LATITUDE,
                        v.LONGTUDE,
                        v.TIME_NOTES,

                        s.gov_code shop_gov_code,
                        s.city_code shop_city_code,
                        s.dist_code shop_dist_code,

                        r.name sales_rep_name,

                        v.CALL_CUST,
                        c.CUST_TYPE  ,
                        c.NAME ,
                        c.WORK_ADDRESS,
                        c.HOME_ADDRESS,
                        c.TEL,
                        c.TEL2,
                        c.MOBILE,
                        c.ID_NO,
                        c.USER_NAME plumber_creator,
                        c.REC_DATE plumber_rec_date,
                        c.GOV_CODE   ,
                        c.CITY_CODE  ,
                        c.DIST_CODE  ,
                        c.TITLE,
                        c.LAST_DATE  ,
                        c.OLD_CD,
                        c.CD   ,
                        c.BIRTH_DATE ,
                        c.RELIGION   ,
                        c.BRANCH,
                        c.PRINT_STATUS,
                        c.SHOP_CODE  ,
                        c.FIRST_DATE ,
                        c.BIRTH_GOV birth_gov_code,
                        c.RFM_RES,
                        c.RFM_NEXT_DATE,
                        c.RFM_ORD_NO ,
                        c.RFM_USER   ,
                        c.FOLD_NO,
                        c.PAGE_NO,
                        c.BIRTH_CITY birth_city_code,
                        c.BIRTH_LOV birth_dist_code,
                        c.STATUS,
                        c.REASON,
                        c.FLAG ,
                        c.CATEG,
                        c.PHOTO_NAME ,
                        c.LAST_UPDT  ,
                        c.FIRSTS,
                        c.GROUP_CODE ,
                        c.REP_NO,
                        c.MANG_NO,
                        c.CUST_CLASS ,
                        c.CLOSE_DATA ,
                        c.FEAST_GOV  ,
                        c.CC_NOTES   ,
                        c.PHOTO,
                        c.NATIONALITY,
                        c.GENDER,
                        c.PHOTO_UPDATE_BY  ,
                        c.PHOTO_UPDATE_DATE,
                        c.SERV_NO,
                        c.TEL_WHATS  ,
                        c.TEL2_WHATS ,
                        c.MOBILE_WHATS,
                        c.SMART_PHONE,
                        YEAR_BAL(c.code,to_number(to_char(sysdate,'yyyy'))) balance,
                        YEAR_POINTS(c.code,to_number(to_char(sysdate,'yyyy'))) total_points 
                    FROM
                        eg_viewing v,
                        eg_customers c,
                        eg_sales_reps r,
                        eg_shops s
                    WHERE v.cust_code = c.code (+)
                        AND v.shop_code = s.code (+)
                        AND r.code (+) = c.group_code ";

            if (code > 0)
                SQL += $" AND v.code = {code} ";

            if (!string.IsNullOrWhiteSpace(owner_mob))
                SQL += $" AND (v.TEL = {owner_mob} OR v.MOB = {owner_mob})";

            return GetDataAsDataTable(SQL);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, nameof(GetAllInspectionData));
        }
        return null;
    }

    public DataTable GetPlumberLatestInspections(string cust_code, int pageSize = 20)
    {
        try
        {
            SQL = $@"SELECT 
                        v.CODE inspection_code,
                        v.CODE||' - '||nvl(v.OWNER_NAME,v.PLN_DATE) inspection_name,
                        v.CUST_CODE,
                        v.ADDRESS inspection_address,
                        v.GROUP_MANG_CODE,
                        v.REP_CODE ,
                        v.PLN_DATE inspection_planed_date,
                        v.FROM_TIME,
                        v.TO_TIME,
                        v.FEEDBACK,
                        v.POINTS,
                        v.REC_DATE inspection_rec_date,
                        v.USER_NAME inspection_creator,
                        v.GUR_STATUS,
                        v.GUR_REASON grantee_status_reason,
                        v.GOV_CODE inspection_gov_code,
                        v.CITY_CODE inspection_city_code,
                        v.TOTAL_POINTS,
                        v.GRU_DATE,
                        v.V_SOURCE inspection_source,
                        v.OWNER_NAME,
                        v.COMP_ADDRESS,
                        v.TEL owner_tel,
                        v.FAX,
                        v.MOB owner_mobile,
                        v.NOTES inspection_notes,
                        v.V_DESC inspection_description,
                        v.PRN_NO inspection_printed_NO,
                        v.SHOP_CODE buy_shop_code,
                        v.CALLER_NAME,
                        v.CALLER_TEL,
                        v.VIEW_STATUS,
                        v.CALLER_TYPE,
                        v.REP_POINTS,
                        v.V_YEAR inspection_year,
                        v.CERT_REC,
                        v.PRINTED,
                        v.DIST_CODE inspection_dist_code,
                        v.ST_NOTES inspection_status_note,
                        v.DE_NOTES data_entry_notes,
                        v.ORDER_STATUS,
                        v.CANCEL_REASON inspection_reject_reason,
                        v.DET_ORDER,
                        v.PARENT_NO,
                        v.PRINT_REQ,
                        v.PDA_STATUS,
                        v.VIEW_SERIAL,
                        v.REP_REJECT_REASON,
                        v.ESTKMAL_CODE,
                        v.SALES_CODE,
                        v.LAST_UPDT,
                        v.SV_CODE,
                        v.SHOP_NOTE,
                        v.VIEW_LEVEL,
                        v.REV_NOTES,
                        v.STOP_CERT stop_printing_reoprts,
                        v.STOP_RSN stop_printing_reoprts_reason,
                        v.GV_CODE,
                        v.SUPER_CODE,
                        v.CERT_REQ_STATUS,
                        v.WC_CNT,
                        v.KITCHEN_CNT,
                        v.VIEW_TYPE,
                        v.BY_REP,
                        v.QA_REP_NAME inspection_qa_resp_name,
                        v.SEND_TIME,
                        v.BRANCH inspection_branch,
                        v.SETUP_COMP,
                        v.KES_TYPE,
                        v.AREA_NAME,
                        v.VIEW_SITUATION inspection_situation,
                        v.CC_SHOP_CODE,
                        v.CC_MAX_SHOP,
                        v.CC_OTHER_SHOP,
                        v.HASR_STATUS,
                        v.HASR_REASON,
                        v.JOKER_NOTES,
                        v.SERV_NO,
                        v.ACTUAL_DATE,
                        v.ORG_CODE,
                        v.ORG_GUR_STATUS,
                        v.PLACE_TYPE,
                        v.LATITUDE,
                        v.LONGTUDE,
                        v.TIME_NOTES
                    FROM
                        eg_viewing v
                    WHERE 
                        v.cust_code = {cust_code} and rownum <= {pageSize}
                    ORDER BY CODE DESC";

            return GetDataAsDataTable(SQL);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, nameof(GetAllInspectionData));
        }
        return null;
    }

    public bool SaveInspectionData(out string errorMessage,
        string code,
        string owner_name,
        string inspection_address,
        string inspection_gov_code,
        string inspection_city_code,
        string inspection_dist_code,
        string buy_shop_code,

        string inspection_planed_date,
        string inspection_branch,
        string inspection_rep_code,
        string cust_code,
        string from_time,
        string to_time,
        string feedback,
        string inspection_creator,
        string gur_status,
        string grantee_status_reason,
        string inspection_source,
        string owner_tel,
        string owner_mobile,
        string inspection_description,
        string inspection_printed_no,
        string caller_name,
        string caller_tel,
        string view_status,
        string caller_type,
        string rep_points,
        string inspection_status_note,
        string data_entry_notes,
        string order_status,
        string inspection_reject_reason,
        string stop_printing_reoprts,
        string stop_printing_reoprts_reason,
        string view_type,
        string inspection_qa_resp_name,
        string inspection_situation,
        string place_type,
        string currentUsername,

        string time_notes = null,
        string group_mang_code = null,
        string inspection_notes = null,
        string problem = null,
        string problem_desc = null,
        string estkmal_code = null,
        string shop_note = null,
        string view_level = null,
        string rev_notes = null,
        string wc_cnt = null,
        string kitchen_cnt = null,
        string by_rep = null,
        string cc_shop_code = null,
        string cc_max_shop = null,
        string cc_other_shop = null,
        string hasr_status = null,
        string hasr_reason = null,
        string joker_notes = null,
        string actual_date = null,
        string org_gur_status = null,
        string org_code = null)
    {
        int effectedRows = 0;
        errorMessage = "";
        try
        {
            var inspectionData = GetAllInspectionData(code);
            if (inspectionData != null && inspectionData.Rows.Count > 0)
                SQL = $@"UPDATE EG_VIEWING SET
                        cust_code = '{cust_code}',
                        owner_name='{owner_name}',
                        gov_code ='{inspection_gov_code}',
                        city_code ='{inspection_city_code}',
                        dist_code ='{inspection_dist_code}',
                        address ='{inspection_address}',
                        shop_code ='{buy_shop_code}',
                        pln_date = to_date('{DateTime.Parse(inspection_planed_date).ToString(DATE_FORMAT_NET)}','{DATE_FORMAT_ORECAL}'),

                        from_time= to_date('{DateTime.Parse($"{inspection_planed_date} {from_time}").ToString(DATE_FORMAT_NET)}','{DATE_FORMAT_ORECAL}'),
                        to_time= to_date('{DateTime.Parse($"{inspection_planed_date} {to_time}").ToString(DATE_FORMAT_NET)}','{DATE_FORMAT_ORECAL}'),
                        gur_status='{gur_status}',
                        gur_reason ='{grantee_status_reason}',
                        rep_code = '{inspection_rep_code}',
                        v_source ='{inspection_source}',
                        tel ='{owner_tel}',
                        mob ='{owner_mobile}',
                        feedback='{feedback}',
                        st_notes ='{inspection_status_note}',
                        de_notes ='{data_entry_notes}',
                        v_desc ='{inspection_description}',
                        prn_no ='{inspection_printed_no}',
                        caller_name='{caller_name}',
                        caller_tel='{caller_tel}',
                        view_status='{view_status}',
                        caller_type='{caller_type}',
                        rep_points='{rep_points}',
                        order_status='{order_status}',
                        cancel_reason ='{inspection_reject_reason}',
                        stop_cert ='{stop_printing_reoprts}',
                        stop_rsn ='{stop_printing_reoprts_reason}',
                        qa_rep_name ='{inspection_qa_resp_name}',
                        branch ='{inspection_branch}',
                        view_situation ='{inspection_situation}',
                        place_type='{place_type}',

                        {(group_mang_code == null ? "" : $"group_mang_code='{group_mang_code}',")}
                        {(inspection_notes == null ? "" : $"notes ='{inspection_notes}',")}
                        {(rev_notes == null ? "" : $"rev_notes='{rev_notes}',")}
                        {(joker_notes == null ? "" : $"joker_notes='{joker_notes}',")}
                        {(time_notes == null ? "" : $"time_notes='{time_notes}',")}
                        {(problem == null ? "" : $"problem='{problem}',")}
                        {(problem_desc == null ? "" : $"problem_desc='{problem_desc}',")}
                        {(estkmal_code == null ? "" : $"estkmal_code='{estkmal_code}',")}
                        {(shop_note == null ? "" : $"shop_note='{shop_note}',")}
                        {(view_level == null ? "" : $"view_level='{view_level}',")}
                        {(wc_cnt == null ? "" : $"wc_cnt='{wc_cnt}',")}
                        {(kitchen_cnt == null ? "" : $"kitchen_cnt='{kitchen_cnt}',")}
                        {(view_type == null ? "" : $"view_type='{view_type}',")}
                        {(by_rep == null ? "" : $"by_rep='{by_rep}',")}
                        {(cc_shop_code == null ? "" : $"cc_shop_code='{cc_shop_code}',")}
                        {(cc_max_shop == null ? "" : $"cc_max_shop='{cc_max_shop}',")}
                        {(cc_other_shop == null ? "" : $"cc_other_shop='{cc_other_shop}',")}
                        {(hasr_status == null ? "" : $"hasr_status='{hasr_status}',")}
                        {(hasr_reason == null ? "" : $"hasr_reason='{hasr_reason}',")}
                        {(actual_date == null ? "" : $"actual_date='{actual_date}',")}
                        {(org_code == null ? "" : $"org_code='{org_code}',")}
                        {(org_gur_status == null ? "" : $"org_gur_status='{org_gur_status}',")}

                        last_updt=sysdate
                    WHERE CODE = {code}";
            else
                SQL = $@"INSERT INTO EG_CUSTOMERS 
                                (code,
                                cust_code,
                                owner_name,
                                gov_code,
                                city_code,
                                dist_code ,
                                address,
                                shop_code ,
                                pln_date,

                                rep_code ,
                                from_time,
                                to_time,
                                feedback,
                                gur_status,
                                gur_reason,
                                v_source,
                                tel,
                                mob,
                                v_desc ,
                                prn_no ,
                                caller_name,
                                caller_tel,
                                view_status,
                                caller_type,
                                rep_points,
                                v_year ,
                                st_notes,
                                de_notes,
                                order_status,
                                cancel_reason ,
                                stop_cert ,
                                stop_rsn ,
                                view_type,
                                by_rep,
                                qa_rep_name ,
                                branch ,
                                view_situation ,
                                place_type,
                                view_serial,
                                pda_status,
                                user_name,

                                {(time_notes == null ? "" : "time_notes,")}
                                {(group_mang_code == null ? "" : "group_mang_code,")}
                                {(inspection_notes == null ? "" : "notes,")}
                                {(problem == null ? "" : "problem,")}
                                {(problem_desc == null ? "" : "problem_desc,")}
                                {(estkmal_code == null ? "" : "estkmal_code,")}
                                {(shop_note == null ? "" : "shop_note,")}
                                {(view_level == null ? "" : "view_level,")}
                                {(rev_notes == null ? "" : "rev_notes,")}
                                {(wc_cnt == null ? "" : "wc_cnt,")}
                                {(kitchen_cnt == null ? "" : "kitchen_cnt,")}
                                {(cc_shop_code == null ? "" : "cc_shop_code,")}
                                {(cc_max_shop == null ? "" : "cc_max_shop,")}
                                {(cc_other_shop == null ? "" : "cc_other_shop,")}
                                {(hasr_status == null ? "" : "hasr_status,")}
                                {(hasr_reason == null ? "" : "hasr_reason,")}
                                {(joker_notes == null ? "" : "joker_notes,")}
                                {(actual_date == null ? "" : "actual_date,")}
                                {(org_code == null ? "" : "org_code,")}
                                {(org_gur_status == null ? "" : "org_gur_status,")}
                                rec_date) 
                            VALUES 
                                ((SELECT max(code)+1 FROM EG_VIEWING),
                                '{cust_code}',
                                '{owner_name}',
                                '{inspection_gov_code}',
                                '{inspection_city_code}',
                                '{inspection_dist_code}',
                                '{inspection_address}',
                                '{buy_shop_code}',
                                to_date('{DateTime.Parse(inspection_planed_date).ToString(DATE_FORMAT_NET)}','{DATE_FORMAT_ORECAL}'),

                                '{inspection_rep_code }',
                                 to_date('{DateTime.Parse($"{inspection_planed_date} {from_time}").ToString(DATE_FORMAT_NET)}','{DATE_FORMAT_ORECAL}'),
                                 to_date('{DateTime.Parse($"{inspection_planed_date} {to_time}").ToString(DATE_FORMAT_NET)}','{DATE_FORMAT_ORECAL}'),
                                '{feedback}',
                                '{gur_status}',
                                '{grantee_status_reason}',
                                '{inspection_source}',
                                '{owner_tel}',
                                '{owner_mobile}',
                                '{inspection_description }',
                                '{inspection_printed_no }',
                                '{caller_name}',
                                '{caller_tel}',
                                '{view_status}',
                                '{caller_type}',
                                '{rep_points}',
                                '{DateTime.Parse(inspection_planed_date).Year}',
                                '{inspection_status_note}',
                                '{data_entry_notes}',
                                '{order_status}',
                                '{inspection_reject_reason }',
                                '{stop_printing_reoprts}',
                                '{stop_printing_reoprts_reason}',
                                '{view_type}',
                                '{by_rep}',
                                '{inspection_qa_resp_name }',
                                '{inspection_branch }',
                                '{inspection_situation }',
                                '{org_code}',
                                '{org_gur_status}',
                                '{place_type}',
                                1,
                                0,
                                {(time_notes == null ? "" : $"'{time_notes}',")}
                                {(group_mang_code == null ? "" : $"'{group_mang_code}',")}
                                {(inspection_notes == null ? "" : $"'{inspection_notes}',")}
                                {(problem == null ? "" : $"'{problem}',")}
                                {(problem_desc == null ? "" : $"'{problem_desc}',")}
                                {(estkmal_code == null ? "" : $"'{estkmal_code}',")}
                                {(shop_note == null ? "" : $"'{shop_note}',")}
                                {(view_level == null ? "" : $"'{view_level}',")}
                                {(rev_notes == null ? "" : $"'{rev_notes}',")}
                                {(wc_cnt == null ? "" : $"'{wc_cnt}',")}
                                {(kitchen_cnt == null ? "" : $"'{kitchen_cnt}',")}
                                {(cc_shop_code == null ? "" : $"'{cc_shop_code}',")}
                                {(cc_max_shop == null ? "" : $"'{cc_max_shop}',")}
                                {(cc_other_shop == null ? "" : $"'{cc_other_shop}',")}
                                {(hasr_status == null ? "" : $"'{hasr_status}',")}
                                {(hasr_reason == null ? "" : $"'{hasr_reason}',")}
                                {(joker_notes == null ? "" : $"'{joker_notes}',")}
                                {(actual_date == null ? "" : $"'{actual_date}',")}
                                {(org_code == null ? "" : $"'{org_code}',")}
                                {(org_gur_status == null ? "" : $"'{org_gur_status}',")}

                                '{currentUsername}',
                                sysdate)";


            effectedRows = _oracleAcess.ExecuteNonQuery(SQL);
            errorMessage = string.Empty;
        }
        catch (Exception ex)
        {
            errorMessage = ex.Message;
            LogError(ex.Message, nameof(SaveInspectionData));
        }
        return effectedRows > 0;
    }


    // Inspection Prints
    public DataTable GetAllInspectionPrints(int inspectionCode)
    {
        if (inspectionCode < 1)
            return null;

        try
        {
            SQL = $@"SELECT 
                        CODE,
                        VIEW_CODE,
                        PRN_TYPE,
                        PRN_DATE,
                        USER_NAME,
                        RECEIVER_NAME,
                        ISSUED,
                        VIEW_SERIAL,
                        RECEIVER_CAT,
                        PRN_SERIAL
                    FROM
                        eg_viewing_prn
                    WHERE view_code = {inspectionCode}";
            return GetDataAsDataTable(SQL);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, nameof(GetAllInspectionPrints));
        }
        return null;
    }
    public bool UpdateInspectionPrint(int printCode, string receiver_name)
    {
        if (printCode < 1 || string.IsNullOrWhiteSpace(receiver_name))
            return false;

        try
        {
            SQL = $"UPDATE EG_VIEWING_PRN SET RECEIVER_NAME = '{receiver_name}' WHERE CODE = {printCode}";
            int effectedRows = _oracleAcess.ExecuteNonQuery(SQL);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, nameof(UpdateInspectionPrint));
        }
        return false;
    }


    // Inspection Products
    public DataTable GetAllInspectionProducts(int inspectionCode)
    {
        if (inspectionCode < 1)
            return null;

        try
        {
            SQL = $@"SELECT 
                        p.CODE,
                        p.VIEW_CODE,
                        p.CUST_CODE,
                        p.ITEM_CODE,
                        p.QTY,
                        p.NOTES,
                        p.POINTS,
                        p.TOTAL_POINTS,
                        p.CARD_NO,
                        p.USER_NAME,
                        p.REC_DATE,
                        p.SHOP_CODE,
                        p.VIEW_SERIAL,
                        p.SEND_TIME,
                        p.QTY_BEF_UPDATE,
                        p.UPDATE_REASON,
                        p.UPDATE_RESP,
                        p.UPDATE_NO,
                        p.PDA_VERSION,
                        p.DB_VERSION,
                        i.ITEM_TYPE,
                        i.CODE PRODUCT_CODE,
                        i.NAME PRODUCT_NAME
                    FROM
                        eg_viewing_det p,
                        eg_items i
                    WHERE i.code = p.item_code
                        and p.view_code = {inspectionCode}";
            return GetDataAsDataTable(SQL);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, nameof(GetAllInspectionProducts));
        }
        return null;
    }

    public DataTable GetInspectionProduct(int productCode)
    {
        if (productCode < 1)
            return null;

        try
        {
            SQL = $@"SELECT 
                        *
                    FROM
                        eg_viewing_det p
                    WHERE 
                        p.code = {productCode}";
            return GetDataAsDataTable(SQL);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, nameof(GetInspectionProduct));
        }
        return null;
    }

    public bool UpdateInspectionProduct(
        string product_code,
        string ITEM_CODE,
        int QTY,
        string NOTES,
        string CARD_NO,
        string USER_NAME,
        string UPDATE_REASON,
        string UPDATE_RESP,
        string UPDATE_NO,
        string QTY_BEF_UPDATE)
    {
        if (string.IsNullOrWhiteSpace(product_code) || QTY < 1)
            return false;

        int TOTAL_POINTS = 0, POINTS = 0;
        var points = _oracleAcess.ExecuteScalar($"SELECT POINTS FROM EG_ITEMS WHERE CODE = {ITEM_CODE}");
        if (points != null && int.TryParse(points.ToString(), out POINTS) && POINTS > 0)
            TOTAL_POINTS = QTY * POINTS;

        try
        {
            SQL = $@"UPDATE EG_VIEWING_DET SET 
                        ITEM_CODE = '{ITEM_CODE}',
                        QTY = '{QTY}',
                        NOTES = '{NOTES}',
                        POINTS = '{POINTS}',
                        TOTAL_POINTS= '{TOTAL_POINTS}',
                        CARD_NO= '{CARD_NO}',
                        QTY_BEF_UPDATE= '{QTY_BEF_UPDATE}',
                        UPDATE_REASON= '{UPDATE_REASON}',
                        UPDATE_RESP= '{UPDATE_RESP}',
                        UPDATE_NO= '{UPDATE_NO}'
                    WHERE CODE = {product_code}";
            int effectedRows = _oracleAcess.ExecuteNonQuery(SQL);
            return effectedRows > 0;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, nameof(UpdateInspectionProduct));
        }
        return false;
    }

    public bool DeleteInspectionProduct(int product_code)
    {
        if (product_code < 1)
            return false;

        try
        {

            SQL = $"DELETE FROM EG_VIEWING_DET WHERE CODE = {product_code}";
            int effectedRows = _oracleAcess.ExecuteNonQuery(SQL);
            return effectedRows > 0;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, nameof(DeleteInspectionProduct));
        }
        return false;
    }

    public bool InsertInspectionProduct(
        string inspection_code,
        string product_code,
        string ITEM_CODE,
        int QTY,
        string NOTES,
        string CARD_NO,
        string USER_NAME,
        string UPDATE_REASON,
        string UPDATE_RESP,
        string UPDATE_NO,
        string QTY_BEF_UPDATE)
    {

        if (string.IsNullOrWhiteSpace(inspection_code) || string.IsNullOrWhiteSpace(product_code) || QTY < 1)
            return false;

        int TOTAL_POINTS = 0, POINTS = 0;
        var points = _oracleAcess.ExecuteScalar($"SELECT POINTS FROM EG_ITEMS WHERE CODE = {ITEM_CODE}");
        if (points != null && int.TryParse(points.ToString(), out POINTS) && POINTS > 0)
            TOTAL_POINTS = QTY * POINTS;

        try
        {
            SQL = $@"INSERT INTO EG_VIEWING_DET 
                        (ITEM_CODE,
                        QTY,
                        NOTES,
                        POINTS,
                        TOTAL_POINTS,
                        CARD_NO,
                        QTY_BEF_UPDATE,
                        UPDATE_REASON,
                        UPDATE_RESP,
                        UPDATE_NO,
                        USER_NAME,
                        REC_DATE)
                    values (
                        '{ITEM_CODE}',
                        '{QTY}',
                        '{NOTES}',
                        '{POINTS}',
                        '{TOTAL_POINTS}',
                        '{CARD_NO}',
                        '{QTY_BEF_UPDATE}',
                        '{UPDATE_REASON}',
                        '{UPDATE_RESP}',
                        '{UPDATE_NO}',
                        '{USER_NAME}',
                        sysdate)";
            int effectedRows = _oracleAcess.ExecuteNonQuery(SQL);
            return effectedRows > 0;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, nameof(InsertInspectionProduct));
        }
        return false;
    }

    #endregion

    #region plumbers

    public DataTable GetAllPlumbersData(string mobile, int pageSize = 20)
    {
        if (string.IsNullOrWhiteSpace(mobile))
            return null;
        try
        {
            SQL = $@"SELECT 
                        CODE,
                        CUST_CODE,
                        CUST_NAME,
                        ORDER_CODE,
                        OWNER_NAME,
                        CUST_TYPE 
                    FROM(
                        SELECT 
                            C.CODE CODE,
                            C.NAME CUST_NAME,
                            V.CODE ORDER_CODE,
                            V.CUST_CODE CUST_CODE,
                            V.OWNER_NAME,
                            CASE '{mobile}' WHEN C.MOBILE THEN 'CUST' WHEN C.TEL THEN 'CUST' WHEN C.TEL2 THEN 'CUST' WHEN V.MOB THEN 'OWNER' WHEN V.TEL THEN 'OWNER' END CUST_TYPE 
                        FROM 
                            EG_CUSTOMERS C,
                            EG_VIEWING V 
                        WHERE 
                            V.CUST_CODE (+) = C.CODE 
                            AND (C.TEL = '{mobile}' OR C.TEL2 = '{mobile}' OR C.MOBILE = '{mobile}' OR V.MOB = '{mobile}' OR V.TEL = '{mobile}')
                        ORDER BY 
                            V.CODE DESC
                        )
                    WHERE ROWNUM <= {pageSize}";
            return GetDataAsDataTable(SQL);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, nameof(GetAllPlumbersData));
        }
        return null;
    }

    public DataTable SearchForPlumber(string text, out string callerType)
    {
        callerType = "";
        try
        {
            DataTable data = null;
            SQL = $"SELECT CODE CUST_CODE,NAME CUST_NAME FROM EG_CUSTOMERS WHERE CODE = {text} OR MOBILE = '{text}' OR TEL = '{text}' OR TEL2 = '{text}'";
            data = GetDataAsDataTable(SQL);
            callerType = "المتصل سباك";
            if (data == null || data.Rows.Count == 0)
            {
                SQL = $"SELECT CODE CUST_CODE,NAME CUST_NAME FROM EG_CUSTOMERS WHERE CODE IN (SELECT CUST_CODE FROM EG_VIEWING WHERE MOB = '{text}' OR TEL = '{text}')";
                data = GetDataAsDataTable(SQL);
                callerType = "المتصل مالك";
                if (data == null || data.Rows.Count == 0)
                {
                    // سباك غير معروف
                    SQL = $"SELECT CODE CUST_CODE,NAME CUST_NAME FROM EG_CUSTOMERS WHERE CODE = 8397";
                    data = GetDataAsDataTable(SQL);
                    callerType = "";
                }
            }
            return data;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, nameof(GetAllPlumbersData));
        }
        return null;
    }

    public DataTable GetPlumberByInspectionCode(string inspectionCode)
    {
        try
        {
            DataTable data = null;
            SQL = $"SELECT CODE CUST_CODE,NAME CUST_NAME FROM EG_CUSTOMERS WHERE CODE IN (SELECT CUST_CODE FROM EG_VIEWING WHERE CODE = '{inspectionCode}' )";
            data = GetDataAsDataTable(SQL);
            return data;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, nameof(GetAllPlumbersData));
        }
        return null;
    }

    public DataTable GetAllPlumbersData(
        int pageSize = 100,
        int code = 0,
        string plumberName = null,
        string plumberMobile = null,
        string plumberID = null,
        string plumberAddress = null,
        string plumberPhone = null,
        string plumberNickname = null,
        string plumberPhoneII = null,
        string plumberPlace = null,
        string plumberBirthdate = null,
        string plumberDeactivationResion = null,
        string plumberSalesRep = null,
        string plumberBranch = null,
        string plumberShopCode = null,
        string plumberWorkGovCode = null,
        string plumberWorkCityCode = null,
        string plumberWorkDistCode = null,
        string plumberReligion = null,
        string plumberIsActive = null,
        string plumberGroupManagerCode = null,
        string plumberGroupCode = null,
        string plumberInspectionRepCode = null
        )
    {
        try
        {
            SQL = $"SELECT * FROM EG_CUSTOMERS WHERE {(code > 0 ? $"CODE = {code}" : "CODE > 0")} ";

            StringBuilder sqlSB = new StringBuilder();
            if (!string.IsNullOrWhiteSpace(plumberName))
                sqlSB.Append($" and name like '%{plumberName}%'");

            if (!string.IsNullOrWhiteSpace(plumberMobile))
                sqlSB.Append($" and mobile like '%{plumberMobile}%'");

            if (!string.IsNullOrWhiteSpace(plumberID))
                sqlSB.Append($" and id_no like '%{plumberID}%'");

            if (!string.IsNullOrWhiteSpace(plumberAddress))
                sqlSB.Append($" and home_address like '%{plumberAddress}%'");

            if (!string.IsNullOrWhiteSpace(plumberPhone))
                sqlSB.Append($" and tel like '%{plumberPhone}%'");

            if (!string.IsNullOrWhiteSpace(plumberPhoneII))
                sqlSB.Append($" and tel2 like '%{plumberPhoneII}%'");

            if (!string.IsNullOrWhiteSpace(plumberIsActive))
                sqlSB.Append($" and status = '{plumberIsActive}'");

            if (!string.IsNullOrWhiteSpace(plumberReligion))
                sqlSB.Append($" and RELIGION = '{plumberReligion}'");

            if (string.IsNullOrWhiteSpace(sqlSB.ToString()))
                sqlSB.Append($" and rownum < { pageSize + 1 }");

            sqlSB.Append(" order by code desc");


            SQL += sqlSB.ToString();
            return GetDataAsDataTable(SQL);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, nameof(GetAllPlumbersData));
        }
        return null;
    }

    public DataTable GetPlumberDataByCode(int code)
    {
        try
        {
            SQL = $@"SELECT 
                        c.code,
                        c.code cust_code,
                        c.name,
                        c.mobile,
                        c.id_no,
                        c.home_address,
                        c.work_address,
                        c.tel,
                        c.tel2,
                        c.title,
                        c.birth_date,
                        c.RELIGION,
                        c.STATUS,
                        c.REASON,
                        c.GOV_CODE,
                        g.name gov_name,
                        c.CITY_CODE,
                        ci.name city_name,
                        c.DIST_CODE,
                        d.name dist_name,
                        c.BRANCH,
                        c.SHOP_CODE,
                        s.name shop_name,
                        c.GROUP_CODE,
                        r.group_name,
                        r.name sales_rep_name,
                        c.MANG_NO,
                        c.REP_NO,
                        c.REC_DATE,
                        c.REC_DATE plumber_rec_date,
                        c.PHOTO,
                        c.nationality,
                        c.gender,
                        c.birth_gov birth_gov_code,
                        c.birth_city birth_city_code,
                        c.birth_lov birth_dist_code,
                        c.USER_NAME plumber_creator,
                        c.USER_NAME,
                        YEAR_BAL(c.code,to_number(to_char(sysdate,'yyyy'))) balance,
                        YEAR_POINTS(c.code,to_number(to_char(sysdate,'yyyy'))) total_points 
                    FROM 
                        EG_CUSTOMERS c 
                        left join govs g on g.code = c.GOV_CODE 
                        left join CITIES ci on ci.code = c.CITY_CODE 
                        left join DISTRICT d on d.code = c.DIST_CODE 
                        left join eg_shops s on s.code = c.SHOP_CODE 
                        left join eg_sales_reps r on r.code = c.group_code 
                    WHERE c.CODE = {code}";
            return GetDataAsDataTable(SQL);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, nameof(GetPlumberDataByCode));
        }
        return null;
    }

    public bool DeletePlumber(string code)
    {
        if (!int.TryParse(code, out int _code) || _code == 0)
            return false;
        try
        {
            SQL = $"DELETE FROM EG_CUSTOMERS WHERE CODE = {code}";
            int effictedRows = _oracleAcess.ExecuteNonQuery(SQL);
            return effictedRows > 0;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, nameof(DeletePlumber));
        }
        return false;
    }

    public DataTable GetPlumberDataByCode(string code)
    {
        if (!int.TryParse(code, out int _code) || _code == 0)
            return null;

        return GetPlumberDataByCode(_code);
    }

    public bool SavePlumberData(out string errorMessage,
        string code,
        string name,
        string work_address,
        string home_address,
        string tel,
        string tel2,
        string mobile,
        string id_no,
        string gov_code,
        string city_code,
        string dist_code,
        string shop_code,
        string branch,
        string birth_date,
        string religion,
        string birth_gov,
        string birth_city,
        string birth_lov,
        string status,
        string reason,
        string nickname,
        string group_code,
        string rep_no,
        string mang_no,
        string feast_gov,
        byte[] photo,
        string gender,
        string nationality,
        string currentUsername)
    {
        int effectedRows = 0;
        try
        {
            var plumberData = GetPlumberDataByCode(code);
            if (plumberData != null && plumberData.Rows.Count > 0)
                SQL = $@"UPDATE EG_CUSTOMERS SET
                        name = '{name}',
                        mobile = '{mobile}',
                        home_address = '{home_address}',
                        id_no = '{id_no}',
                        last_updt = '{currentUsername}',
                        work_address = '{work_address}',
                        tel = '{tel}',
                        tel2 = '{tel2}',
                        gov_code = '{gov_code}',
                        city_code = '{city_code}',
                        dist_code = '{dist_code}',
                        shop_code = '{shop_code}',
                        title = '{nickname}',
                        branch = '{branch}',
                        religion = '{religion}',
                        {(string.IsNullOrWhiteSpace(birth_date) ? "" : $"birth_date = to_date('{DateTime.Parse(birth_date).ToString(DATE_FORMAT_NET)}','{DATE_FORMAT_ORECAL}'),")}
                        birth_gov = '{birth_gov}',
                        birth_city = '{birth_city}',
                        birth_lov = '{birth_lov}',
                        status = '{status}',
                        reason = '{reason}',
                        group_code = '{group_code}',
                        rep_no = '{rep_no}',
                        mang_no = '{mang_no}',
                        {(photo != null && photo.Length > 0 ? $"photo=:IMAGE,photo_update_by='{currentUsername}',photo_update_date=sysdate," : "")}
                        feast_gov = '{feast_gov}',
                        gender = '{gender}',
                        nationality = '{nationality}',
                        LAST_DATE = sysdate
                    WHERE CODE = {code}";
            else
                SQL = $@"INSERT INTO EG_CUSTOMERS 
                                (code, 
                                name,
                                mobile, 
                                id_no, 
                                home_address,
                                work_address,
                                tel,
                                tel2,
                                gov_code,
                                city_code,
                                dist_code,
                                shop_code,
                                branch,
                                birth_date,
                                religion,
                                birth_gov,
                                birth_city,
                                birth_lov,
                                status,
                                reason,
                                title,
                                group_code,
                                rep_no,
                                mang_no,
                                {(photo != null && photo.Length > 0 ? "photo,photo_update_by,photo_update_date," : "")}
                                cust_type,
                                gender,
                                nationality,
                                user_name,
                                rec_date) 
                            VALUES 
                                ((SELECT max(code)+1 FROM EG_CUSTOMERS),
                                '{name}',
                                '{mobile}', 
                                '{id_no}', 
                                '{home_address}',
                                '{work_address}',
                                '{tel}',
                                '{tel2}',
                                '{gov_code}',
                                '{city_code}',
                                '{dist_code}',
                                '{shop_code}',
                                '{branch}',
                                {(string.IsNullOrWhiteSpace(birth_date) ? "''," : $"to_date('{DateTime.Parse(birth_date).ToString(DATE_FORMAT_NET)}','{DATE_FORMAT_ORECAL}'),")}
                                '{religion}',
                                '{birth_gov}',
                                '{birth_city}',
                                '{birth_lov}',
                                '{status}',
                                '{reason}',
                                '{nickname}',
                                '{group_code}',
                                '{rep_no}',
                                '{mang_no}',
                                {(photo != null && photo.Length > 0 ? $":IMAGE,'{currentUsername}',sysdate," : "")}
                                'S',
                                '{gender}',
                                '{nationality}',
                                '{currentUsername}',
                                sysdate)";

            if (photo != null && photo.Length > 0)
            {
                OracleParameter blobParameter = new OracleParameter();
                blobParameter.OracleDbType = OracleDbType.LongRaw;
                blobParameter.ParameterName = "IMAGE";
                blobParameter.Value = photo;
                effectedRows = _oracleAcess.ExecuteNonQuery(SQL, new OracleParameter[] { blobParameter });
            }
            else
                effectedRows = _oracleAcess.ExecuteNonQuery(SQL);
            errorMessage = string.Empty;
        }
        catch (Exception ex)
        {
            errorMessage = ex.Message;
            LogError(ex.Message, nameof(SavePlumberData));
        }
        return effectedRows > 0;
    }

    #endregion

    #endregion
}