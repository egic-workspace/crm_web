﻿public class AppConsts
{
    public const string CURRENT_USERNAME = nameof(CURRENT_USERNAME);
    public const string CURRENT_PAGE_URL = nameof(CURRENT_PAGE_URL);
    public const string MAIN_MENU_ITEMS = nameof(MAIN_MENU_ITEMS);
}