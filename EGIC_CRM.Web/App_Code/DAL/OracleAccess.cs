﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for OracleAcess
/// </summary>
public class OracleAcess
{
    private string _connectionString;

    public OracleConnection oracc;

    private bool WithTransaction;

    private OracleTransaction _transaction;

    public OracleAcess()
    {
        WithTransaction = false;
        _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        //return the value    
        oracc = new OracleConnection(_connectionString);

    }
    public OracleAcess(string connectionString)
    {
        WithTransaction = false;
        if (!string.IsNullOrEmpty(connectionString))
            _connectionString = connectionString;

        //return the value    
        oracc = new OracleConnection(_connectionString);

    }

    public OracleTransaction Transaction
    {
        get { return this._transaction; }
        set { this._transaction = value; }
    }

    public int ExecuteNonQuery(string sql)
    {
        OracleCommand cmd = new OracleCommand(sql, oracc);
        if (oracc.State != ConnectionState.Open)
        {
            oracc.Open();
        }
        if ((WithTransaction))
        {
            cmd.Transaction = Transaction;
        }
        int retval = cmd.ExecuteNonQuery();
        if ((!WithTransaction))
        {
            oracc.Close();
        }
        return retval;
    }

    public int ExecuteNonQuery(string sql, OracleParameter[] @params)
    {
        OracleCommand cmd = new OracleCommand(sql, oracc);
        for (int i = 0; i <= @params.Length - 1; i++)
        {
            cmd.Parameters.Add(@params[i]);
        }

        if (oracc.State != ConnectionState.Open)
        {
            oracc.Open();
        }
        if ((WithTransaction))
        {
            cmd.Transaction = Transaction;
        }
        int retval = cmd.ExecuteNonQuery();
        if ((!WithTransaction))
        {
            oracc.Close();
        }
        return retval;
    }

    public void SaveData(DataTable dt, string Sql)
    {
        OracleDataAdapter Adp = new OracleDataAdapter(Sql, oracc);
        OracleCommandBuilder CmAdp = new OracleCommandBuilder(Adp);

        Adp.InsertCommand = CmAdp.GetInsertCommand();
        Adp.DeleteCommand = CmAdp.GetDeleteCommand();
        Adp.UpdateCommand = CmAdp.GetUpdateCommand();
        Adp.Update(dt);
    }

    public void BeginTransaction()
    {
        WithTransaction = true;
        if (oracc.State != ConnectionState.Open)
        {
            oracc.Open();
        }
        Transaction = oracc.BeginTransaction();
    }

    public void Rollback()
    {
        WithTransaction = false;
        Transaction.Rollback();
        if (oracc.State != ConnectionState.Closed)
        {
            oracc.Close();
        }
    }

    public void Commit()
    {
        WithTransaction = false;
        Transaction.Commit();
        if (oracc.State != ConnectionState.Closed)
        {
            oracc.Close();
        }
    }

    public object ExecuteScalar(string sql, OracleParameter[] @params)
    {
        OracleConnection cnn = new OracleConnection(_connectionString);
        OracleCommand cmd = new OracleCommand(sql, cnn);
        for (int i = 0; i <= @params.Length - 1; i++)
        {
            cmd.Parameters.Add(@params[i]);
        }

        if (cnn.State != ConnectionState.Open)
        {
            cnn.Open();
        }
        if ((WithTransaction))
        {
            cmd.Transaction = Transaction;
        }
        object retval = cmd.ExecuteScalar();
        if ((!WithTransaction))
        {
            cnn.Close();
        }

        return retval;
    }

    public object ExecuteScalar(string sql)
    {
        oracc = new OracleConnection(_connectionString);
        OracleCommand cmd = new OracleCommand(sql, oracc);
        if (oracc.State != ConnectionState.Open)
        {
            oracc.Open();
        }
        if ((WithTransaction))
        {
            cmd.Transaction = Transaction;
        }
        object retval = cmd.ExecuteScalar();
        if ((!WithTransaction))
        {
            oracc.Close();
        }
        return retval;
    }

    public DataTable ExecuteDataTable(string sql)
    {
        DataTable dt = new DataTable();
        OracleDataAdapter da = new OracleDataAdapter(sql, oracc);
        da.Fill(dt);
        return dt;
    }

    public DataTable ExecuteDataTable(string sql, OracleParameter[] @params)
    {
        DataTable dt = new DataTable();
        OracleDataAdapter da = new OracleDataAdapter(sql, _connectionString);
        for (int i = 0; i <= @params.Length - 1; i++)
        {
            da.SelectCommand.Parameters.Add(@params[i]);
        }
        da.Fill(dt);
        return dt;
    }

    public DataSet ExecuteDataSet(string sql)
    {
        DataSet ds = new DataSet();
        OracleDataAdapter da = new OracleDataAdapter(sql, _connectionString);
        da.Fill(ds);

        return ds;
    }

    public int Excute_PRC(string PRC, OracleParameter[] @params)
    {
        OracleCommand cmd = new OracleCommand();
        cmd.Connection = oracc;
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = PRC;

        for (int i = 0; i <= @params.Length - 1; i++)
        {
            cmd.Parameters.Add(@params[i]);
        }

        if (oracc.State != ConnectionState.Open)
        {
            oracc.Open();
        }
        if ((WithTransaction))
        {
            cmd.Transaction = Transaction;
        }
        int retval = cmd.ExecuteNonQuery();
        if ((!WithTransaction))
        {
            oracc.Close();
        }
        return retval;
    }
}