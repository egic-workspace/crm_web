﻿(function (global, undefined) {
    var demo = {};

    function OnClientFilesUploaded(sender, args) {
        $find(demo.ajaxManagerID).ajaxRequest();
    }

    function serverID(name, id) {
        demo[name] = id;
    }

    global.serverID = serverID;

    global.OnClientFilesUploaded = OnClientFilesUploaded;
})(window);
$(document).ready(function () {
    //collapseToggleBox('insert-form', false);
    hideShowActivationReasonDiv();

    function reloadGrid() {
        $('[id$="RadGrid1"]').focus();
        $('[id$="RefreshButton"]').click().focus();
    }

    $(document).on('change', '[id$="ddl_isActive"]', function () {
        hideShowActivationReasonDiv();
    });

    $(document).on('click', '#btnCancelSave', function () {
        resetForm();
        reloadGrid();
    });

    $(document).on('keypress', '[type="text"]', function (e) {
        if (e.keyCode === 13) {
            reloadGrid();
            return false;
        }
    });

    $(document).on('keypress', '[type="tel"]', function (e) {
        if (e.keyCode === 13) {
            reloadGrid();
            return false;
        }
    });

    $(document).on('keypress', '[type="number"]', function (e) {
        if (e.keyCode === 13) {
            reloadGrid();
            return false;
        }
    });
    $(document).bind('keydown', "f4", function assets() {

        //collapseToggleBox('insert-form', true);
        $('[id$="txt_plumber_name"]').focus();
        return false;
    });
    $(document).bind('keydown', "f6", function assets() {
        //collapseToggleBox('insert-form', true);
        $('[id$="txt_plumber_name"]').focus();
        return false;
    });
    $(document).bind('keydown', "f7", function assets() {
        //collapseToggleBox('insert-form', true);
        $('[id$="txt_plumber_name"]').focus();
        return false;
    });
    $(document).bind('keydown', "f8", function assets() {
        reloadGrid();
        return false;
    });
    $(document).bind('keydown', "f10", function assets() {
        $('[id$="btn_save"]').click();
        return false;
    });

    $('[id$="ddl_work_shop"]').select2({
        ajax: {
            url: "../../API.ashx?page=GetShops",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    shopname: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, params) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                params.page = params.page || 1;

                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.items.length
                    }
                };
            },
            cache: true
        },
        placeholder: 'اختر...',
        minimumInputLength: 2
    });
    
});

function hideShowActivationReasonDiv() {
    var ddl_isActive = $('[id$="ddl_isActive"]'),
        div_deactivation_resion = $("#div_deactivation_resion");
    if (ddl_isActive.val() === "موقوف") {
        div_deactivation_resion.show();
    }
    else {
        div_deactivation_resion.hide();
    }
}
function resetForm() {
    $('[id$="hidden_code"]').val('');
    $('[id$="txt_plumber_name"]').val('');
    $('[id$="txt_plumber_mobile"]').val('');
    $('[id$="txt_plumber_ID"]').val('');
    $('[id$="txt_home_address"]').val('');

    $('[id$="txt_plumber_phone"]').val('');
    $('[id$="txt_plumber_phone_2"]').val('');
    $('[id$="txt_plumber_birthdate"]').val('');
    $('[id$="txt_sales_rep"]').val('');

    $('[id$="txt_creator_name"]').val('');
    $('[id$="txt_rec_date"]').val('');
    $('[id$="txt_deactivation_reason"]').val('');
    $('[id$="txt_plumber_place"]').val('');

    $('[id$="ddl_isActive"]').val('');
    $('[id$="ddl_religion"]').val('');
    $('[id$="ddl_branch"]').val('');
    $('[id$="ddl_work_gov"]').val('');
    $('[id$="ddl_work_city"]').val('');
    $('[id$="ddl_work_dist"]').val('');
    $('[id$="ddl_group_manager"]').val('');
    $('[id$="ddl_inspection_rep"]').val('');
    $('[id$="ddl_group_name"]').val('').trigger('change.select2');
    $('[id$="ddl_work_shop"]').val('').trigger('change.select2');
    $('[id$="imgPlumberID"]').attr('src', '');
}