﻿$(function () {
    $(document).bind('keydown', 'Ctrl+s', function assets() {
        save();
        return false;
    });
    $(document).bind('keydown', 'f10', function assets() {
        save();
        return false;
    });
    $(document).bind('keydown', 'enter', function assets() {
        return false;
    });

    function save() {

        if ($('[id$="btn_save"]').length === 1)
            $('[id$="btn_save"]').click();

        if ($('[id$="btn_update"]').length === 1)
            $('[id$="btn_update"]').click();

    }
});