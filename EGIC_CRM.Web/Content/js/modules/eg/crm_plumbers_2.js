﻿var dataAdapter;
$(document).ready(function () {
    //collapseToggleBox('insert-form', false);
    hideShowActivationReasonDiv();

    $(document).on('change', '[id$="ddl_isActive"]', function () {
        hideShowActivationReasonDiv();
    });

    // prepare the data
    var source =
    {
        datatype: "json", 
        pagesize: 10, 
        datafields: [
            { name: "CODE" },
            { name: "CUST_TYPE" },
            { name: "NAME" },
            { name: "WORK_ADDRESS" },
            { name: "HOME_ADDRESS" },
            { name: "TEL" },
            { name: "TEL2" },
            { name: "MOBILE" },
            { name: "ID_NO" },
            { name: "USER_NAME" },
            { name: "REC_DATE" },
            { name: "GOV_CODE" },
            { name: "CITY_CODE" },
            { name: "DIST_CODE" },
            { name: "TITLE" },
            { name: "LAST_DATE" },
            { name: "OLD_CD" },
            { name: "CD" },
            { name: "BIRTH_DATE" },
            { name: "RELIGION" },
            { name: "BRANCH" },
            { name: "PRINT_STATUS" },
            { name: "SHOP_CODE" },
            { name: "FIRST_DATE" },
            { name: "BIRTH_GOV" },
            { name: "RFM_RES" },
            { name: "RFM_NEXT_DATE" },
            { name: "RFM_ORD_NO" },
            { name: "RFM_USER" },
            { name: "FOLD_NO" },
            { name: "PAGE_NO" },
            { name: "BIRTH_CITY" },
            { name: "BIRTH_LOV" },
            { name: "STATUS" },
            { name: "REASON" },
            { name: "FLAG" },
            { name: "CATEG" },
            { name: "PHOTO_NAME" },
            { name: "LAST_UPDT" },
            { name: "FIRSTS" },
            { name: "GROUP_CODE" },
            { name: "REP_NO" },
            { name: "MANG_NO" },
            { name: "CUST_CLASS" },
            { name: "CLOSE_DATA" },
            { name: "FEAST_GOV" },
            { name: "CC_NOTES" },
            { name: "PHOTO" },
            { name: "NATIONALITY" },
            { name: "GENDER" },
            { name: "PHOTO_UPDATE_BY" },
            { name: "PHOTO_UPDATE_DATE" },
            { name: "SERV_NO" },
            { name: "TEL_WHATS" },
            { name: "TEL2_WHATS" },
            { name: "MOBILE_WHATS" },
            { name: "SMART_PHONE" }
        ],
        id: 'code',
        url:'../../API.ashx?page=getAllplumbers'
    };
    dataAdapter = new $.jqx.dataAdapter(source);

    // initialize jqxGrid
    $("#grid").jqxGrid(
        {
            rtl: true,
            width:"100%",
            source: dataAdapter,
             
            groupable: true, 
            showtoolbar: true,
            keyboardnavigation: true,
            pageable: true,
            pagermode: 'simple',
            autoheight: true,
            rendertoolbar: function (statusbar) {
                // appends buttons to the status bar.
                var container = $("<div class='btn pull-left' style='overflow: hidden; position: relative; margin: 5px;'></div>");
                var addButton = $("<div class='btn pull-left' style='float: left; margin-left: 5px;'><span style='margin-right: 4px;'>إضافة</span><i class='fa  fa-plus-circle'></i></div>");
                var reloadButton = $("<div class='btn pull-left' style='float: left; margin-left: 5px;'><span style='margin-right: 4px;'>تحديث</span><i class='fa fa-refresh'></i></div>");
                var searchButton = $("<div class='btn pull-left' style='float: left; margin-left: 5px;'><span style='margin-right: 4px;'>بحث</span><i class='fa fa-search'></i></div>");

                container.append(addButton);
                container.append(reloadButton);
                container.append(searchButton);

                statusbar.append(container);

                addButton.jqxButton({ width: 60, height: 20});
                reloadButton.jqxButton({ width: 65, height: 20});
                searchButton.jqxButton({ width: 50, height: 20 });

                // add new row.
                addButton.click(function (event) {
                    resetForm();
                    collapseToggleBox('insert-form', true);
                    //$('[href="#main_tab_2"]').tab('show');
                });

                // reload grid data.
                reloadButton.click(function (event) {
                    $("#grid").jqxGrid({ source: dataAdapter });
                });

                // search for a record.
                searchButton.click(function (event) {
                    var offset = $("#grid").offset();
                    $("#jqxwindow").jqxWindow('open');
                    $("#jqxwindow").jqxWindow('move', offset.right + 30, offset.top + 30);
                });


                var leftContainer = $("<div class='btn pull-right' style='overflow: hidden; position: relative; margin: 5px;'></div>");
                var pdfButton = $("<div class='btn pull-right' style='float: right; margin-right: 5px;'><span style='margin-right: 4px;'>PDF</span><i class='fa fa-file-pdf-o'></i></div>");
                var xlsButton = $("<div class='btn pull-right' style='float: right; margin-right: 5px;'><span style='margin-right: 4px;'>XLS</span><i class='fa fa-file-excel-o'></i></div>");
                var csvButton = $("<div class='btn pull-right' style='float: right; margin-right: 5px;'><span style='margin-right: 4px;'>CSV</span><i class='fa fa-file-text-o'></i></div>");

                leftContainer.append(pdfButton);
                leftContainer.append(xlsButton);
                leftContainer.append(csvButton);

                //statusbar.append(leftContainer);

                pdfButton.jqxButton({ width: 60, height: 20 });
                xlsButton.jqxButton({ width: 65, height: 20 });
                csvButton.jqxButton({ width: 50, height: 20 });

                xlsButton.click(function () {
                    $("#grid").jqxGrid('exportdata', 'xls', 'jqxGrid');
                });
                csvButton.click(function () {
                    $("#grid").jqxGrid('exportdata', 'csv', 'jqxGrid');
                });
                pdfButton.click(function () {
                    $("#grid").jqxGrid('exportdata', 'pdf', 'jqxGrid');
                });

            },
            columns: [
                {
                    text: 'حذف', width: 75, cellsalign: 'center', align: 'center',
                    cellsrenderer: function (row, column, value) {
                        return "<button type='button' name='btn_delete_row' data-row-index='" + row + "' class='btn btn-sm btn-block '> حذف <i class='fa fa-trash-o danger text-bold'></i></button>";
                        }
                },
                {
                    text: 'تعديل', width: 75, cellsalign: 'center', align: 'center',
                    cellsrenderer: function (row, column, value) {
                        return "<button type='button' name='btn_edit_row' data-row-index='" + row + "' class='btn btn-sm btn-block '> تعديل <i class='fa fa-edit warning text-bold'></i></button>";
                    }
                },
                { text: 'كود السباك', datafield: 'CODE', width: 100, cellsalign: 'center', align: 'center' },
                { text: 'اسم السباك', datafield: 'NAME',  cellsalign: 'center', align: 'center' },
                { text: 'موبايل السباك', datafield: 'MOBILE', width: 150, cellsalign: 'center', align: 'center' },
                { text: 'رقم بطاقة السباك', datafield: 'ID_NO', width: 200, align: 'center', cellsalign: 'center' }
            ]
        });
    $('.show-after-page-load').show();
    // create jqxWindow.
    $('#jqxwindow').jqxWindow({
        width: 240,
        height: 180,
        autoOpen: false,
        resizable: false,
        cancelButton: $('#clearButton'),
        initContent: function () {
            $('#findButton').jqxButton({ width: '80px' });
            $('#clearButton').jqxButton({ width: '80px' });
        }
    });

    // create dropdownlist.
    $("#dropdownlist").jqxDropDownList({
        autoDropDownHeight: true, selectedIndex: 0, width: 200, height: 23, 
        source: [
            'كود السباك', 'اسم السباك', 'موبايل السباك', 'رقم بطاقة السباك'
        ]
    });
    // create find and clear buttons.
    $("#findButton").jqxButton({ width: 70 });
    $("#clearButton").jqxButton({ width: 70 });
    // clear filters.
    $("#clearButton").click(function () {
        $("#grid").jqxGrid('clearfilters');
    });
    // find records that match a criteria.
    $("#findButton").click(function () {
        $("#grid").jqxGrid('clearfilters');
        var searchColumnIndex = $("#dropdownlist").jqxDropDownList('selectedIndex');
        var datafield = "";
        switch (searchColumnIndex) {
            case 0:
                datafield = "CODE";
                break;
            case 1:
                datafield = "NAME";
                break;
            case 2:
                datafield = "MOBILE";
                break;
            case 3:
                datafield = "ID_NO";
                break;
        }
        var searchText = $("#inputField").val();
        var filtergroup = new $.jqx.filter();
        var filter_or_operator = 1;
        var filtervalue = searchText;
        var filtercondition = 'contains';
        var filter = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);
        filtergroup.addfilter(filter_or_operator, filter);
        $("#grid").jqxGrid('addfilter', datafield, filtergroup);
        // apply the filters.
        $("#grid").jqxGrid('applyfilters');
    });


    // create buttons, listbox and the columns chooser dropdownlist.
    $("#applyfilter").jqxButton({ width: 98, height: 25});
    $("#clearfilter").jqxButton({ width: 98, height: 25});
    $("#txtSearch").jqxInput({ rtl: true,  placeHolder: "ادخل كلمة البحث", width: 200, height: 25, minLength: 1 });
    $("#columnchooser").jqxDropDownList({
        autoDropDownHeight: true, selectedIndex: 0, width: 200, height: 25, 
        source: [
        { label: 'كود السباك', value: 'CODE' },
        { label: 'اسم السباك', value: 'NAME' },
        { label: 'موبايل السباك', value: 'MOBILE' },
        { label: 'رقم بطاقة السباك', value: 'ID_NO' }
        ]
    });
    // handle columns selection.
    $("#columnchooser").on('select', function (event) {
        updateSearchText(event.args.item.value);
    });
    // clears the filter.
    $("#clearfilter").click(function () {
        $('#txtSearch').jqxInput('val', '');
        $("#grid").jqxGrid('clearfilters');
    });
    // applies the filter.
    $("#applyfilter").click(function () {
        var dataField = $("#columnchooser").jqxDropDownList('getSelectedItem').value;
        applyFilter(dataField);
    });


    // builds and applies the filter.
    var applyFilter = function (datafield) {
        $("#grid").jqxGrid('clearfilters');
        var filtertype = 'stringfilter';
        var filtergroup = new $.jqx.filter();
        var searchText = $.trim($("#txtSearch").jqxInput('val'));
        var filter_or_operator,
            filtervalue,
            filtercondition,
            filter;
        if (searchText === "") {
            $("#grid").jqxGrid('clearfilters');
        }
        else {
            filter_or_operator = 1;
            filtervalue = searchText;
            filtercondition = 'contains';
            filter = filtergroup.createfilter(filtertype, filtervalue, filtercondition);
            filtergroup.addfilter(filter_or_operator, filter);
        }
        // add the filters.
        $("#grid").jqxGrid('addfilter', datafield, filtergroup);
        // apply the filters.
        $("#grid").jqxGrid('applyfilters');
    };
    // updates the listbox with unique records depending on the selected column.
    var updateSearchText = function (datafield) {

        var searchTextAdapter = new $.jqx.dataAdapter(source,
            {
                uniqueDataFields: [datafield],
                autoBind: true
            });
        var uniqueRecords = searchTextAdapter.records;
        $('#txtSearch').jqxInput('val', '');
        $("#txtSearch").jqxInput({ rtl: true, placeHolder: "ادخل كلمة البحث", width: 200, height: 25, minLength: 1, source: uniqueRecords });
    };
    updateSearchText('CODE');

    $(document).on('click', '[name="btn_delete_row"]', function () {
        var $this = $(this),
            rowIndex = $this.attr('data-row-index'),
            rowData = $('#grid').jqxGrid('getrowdata', rowIndex);

        $('#ok').attr({ 'data-row-index': rowIndex, 'data-item-code': rowData.CODE });
        $('#confirmation-window').jqxWindow('open');
    });
    $('#confirmation-window').jqxWindow({
        rtl: true,
        draggable: false,
        autoOpen: false,
        maxHeight: 120, maxWidth: 280, minHeight: 30, minWidth: 250, height: 100, width: 270,
        resizable: false, isModal: true, modalOpacity: 0.3,
        okButton: $('#ok'), cancelButton: $('#cancel'),
        initContent: function () {
            $('#ok').jqxButton({ rtl: true });
            $('#cancel').jqxButton({ rtl: true });
            $('#ok').focus();
            $("#ok").on('click', function () {
                var selectedrowindex = $("#grid").jqxGrid('getselectedrowindex');
                var rowData = $('#grid').jqxGrid('getrowdata', selectedrowindex);
                $.ajax({
                    url: '../../API.ashx?page=deletePlumber&plumberCode=' + rowData.CODE,
                    success: function (p) {
                        var id = $("#grid").jqxGrid('getrowid', selectedrowindex);
                        $("#grid").jqxGrid('deleterow', id);
                    },
                    error: function (xhr) {

                    }
                });
                
                
            });
        }
    });

    $(document).on('click', '[name="btn_edit_row"]', function () {
        var $this = $(this),
            rowIndex = $this.attr('data-row-index'),
            rowData = $('#grid').jqxGrid('getrowdata', rowIndex);
        $.ajax({
            url: '../../API.ashx?page=getPlumber&plumberCode=' + rowData.CODE,
            success: function (p) {
                if (p.length > 0) {
                    p = p[0]; 
                    $('[id$="hidden_code"]').val(p.CODE);
                    $('[id$="txt_plumber_name"]').val(p.NAME);
                    $('[id$="txt_plumber_mobile"]').val(p.MOBILE);
                    $('[id$="txt_plumber_ID"]').val(p.ID_NO);
                    $('[id$="txt_home_address"]').val(p.HOME_ADDRESS);

                    $('[id$="txt_plumber_phone"]').val(p.TEL);
                    $('[id$="txt_plumber_phone_2"]').val(p.TEL2);
                    $('[id$="txt_plumber_birthdate"]').val(p.BIRTH_DATE);
                    $('[id$="txt_sales_rep"]').val(p.REP_NO);

                    $('[id$="txt_creator_name"]').val(p.USER_NAME);
                    $('[id$="txt_rec_date"]').val(p.REC_DATE);
                    $('[id$="txt_deactivation_resion"]').val(p.REASON);

                    $('[id$="ddl_isActive"]').val(p.STATUS);
                    $('[id$="ddl_religion"]').val(p.RELIGION);
                    $('[id$="ddl_branch"]').val(p.BRANCH);
                    $('[id$="ddl_work_gov"]').val(p.GOV_CODE);
                    $('[id$="ddl_work_city"]').val(p.CITY_CODE);
                    $('[id$="ddl_work_dist"]').val(p.DIST_CODE);
                    $('[id$="ddl_group_name"]').val(p.GROUP_CODE).trigger('change.select2');

                    if (p.PHOTO !== null && p.PHOTO !== "") {
                        $('[id$="imgPlumberID"]').attr('src', 'data:image/png;base64,' + p.PHOTO);
                    }
                    else {
                        $('[id$="imgPlumberID"]').attr('src', '');
                    }
                    for (var i = 0; i < dataAdapter.length; i++) {
                        if (dataAdapter[i].CODE === p.CODE) {
                            dataAdapter[i] = p;
                            break;
                        }
                    }
                }
            },
            error: function (xhr) {
                console.log(xhr);
            }
        });

        //for (var i = 0; i < dataAdapter.records.length; i++) {
        //    var p = dataAdapter.records[i];
        //    if (p.CODE === rowData.CODE) {
        //        $('[id$="hidden_code"]').val(p.CODE);
        //        $('[id$="txt_plumber_name"]').val(p.NAME);
        //        $('[id$="txt_plumber_mobile"]').val(p.MOBILE);
        //        $('[id$="txt_plumber_ID"]').val(p.ID_NO);
        //        $('[id$="txt_home_address"]').val(p.HOME_ADDRESS);

        //        $('[id$="txt_plumber_phone"]').val(p.TEL);
        //        $('[id$="txt_plumber_phone_2"]').val(p.TEL2);
        //        $('[id$="txt_plumber_birthdate"]').val(p.BIRTH_DATE);
        //        $('[id$="txt_sales_rep"]').val(p.REP_NO);

        //        $('[id$="txt_creator_name"]').val(p.USER_NAME);
        //        $('[id$="txt_rec_date"]').val(p.REC_DATE);
        //        $('[id$="txt_deactivation_resion"]').val(p.REASON);

        //        $('[id$="ddl_isActive"]').val(p.STATUS);
        //        $('[id$="ddl_religion"]').val(p.RELIGION);
        //        $('[id$="ddl_branch"]').val(p.BRANCH);
        //        $('[id$="ddl_work_gov"]').val(p.GOV_CODE);
        //        $('[id$="ddl_work_city"]').val(p.CITY_CODE);
        //        $('[id$="ddl_work_dist"]').val(p.DIST_CODE);
        //        $('[id$="ddl_group_name"]').val(p.GROUP_CODE).trigger('change.select2');

        //        if (p.PHOTO !== null && p.PHOTO !== "") {
        //            $('[id$="imgPlumberID"]').attr('src', 'data:image/png;base64,' + p.PHOTO);
        //        }
        //        else {
        //            $('[id$="imgPlumberID"]').attr('src', '');
        //        }

        //        //$('[id$="ddl_work_shop"]').val(p.SHOP_CODE).trigger('change.select2');

        //        break;
        //    }
        //}

        collapseToggleBox('insert-form', true);
        $('[id$="txt_plumber_name"]').focus();
        //$('[href="#main_tab_2"]').tab('show');
    });
    $('#btnCancelSave').click(function () {
        resetForm();
        collapseToggleBox('insert-form', false);
        $('#grid').focus();
        //$('[href="#main_tab_1"]').tab('show');
    });

    //$(document).on('', '',function () {
    //    resetForm();
    //    collapseToggleBox('insert-form', false);
    //    $('#grid').focus();
    //});

    $("#print").jqxButton();
    $("#print").click(function () {
        var gridContent = $("#grid").jqxGrid('exportdata', 'html');
        var newWindow = window.open('', '', 'width=800, height=500'),
            document = newWindow.document.open(),
            pageContent =
                '<!DOCTYPE html>\n' +
                '<html>\n' +
                '<head>\n' +
                '<meta charset="utf-8" />\n' +
                '<title>طباعة</title>\n' +
                '</head>\n' +
                '<body>\n' + gridContent + '\n</body>\n</html>';
        document.write(pageContent);
        document.close();
        newWindow.print();
    });

    // First register any plugins
    $.fn.filepond.registerPlugin(FilePondPluginImagePreview);


    // Turn input element into a pond
    $('.my-pond').filepond();

    // Set allowMultiple property to true
    $('.my-pond').filepond('allowMultiple', true);

    // Listen for addfile event
    $('.my-pond').on('FilePond:addfile', function (e) {
        console.log('file added event', e);
    });

    // Manually add a file using the addfile method
    $('.my-pond').first().filepond('addFile', '../../API.ashx?page=uploadPlumberID').then(function (file) {
        console.log('file added', file);
    });
});

function hideShowActivationReasonDiv() {
    var ddl_isActive = $('[id$="ddl_isActive"]'),
        div_deactivation_resion = $("#div_deactivation_resion");
    if (ddl_isActive.val() === "موقوف") {
        div_deactivation_resion.show();
    }
    else {
        div_deactivation_resion.hide();
    }
}
function resetForm() {
    $('[id$="hidden_code"]').val('');
    $('[id$="txt_plumber_name"]').val('');
    $('[id$="txt_plumber_mobile"]').val('');
    $('[id$="txt_plumber_ID"]').val('');
    $('[id$="txt_home_address"]').val('');

    $('[id$="txt_plumber_phone"]').val('');
    $('[id$="txt_plumber_phone_2"]').val('');
    $('[id$="txt_plumber_birthdate"]').val('');
    $('[id$="txt_sales_rep"]').val('');

    $('[id$="txt_creator_name"]').val('');
    $('[id$="txt_rec_date"]').val('');
    $('[id$="txt_deactivation_resion"]').val('');

    $('[id$="ddl_isActive"]').val('');
    $('[id$="ddl_religion"]').val('');
    $('[id$="ddl_branch"]').val('');
    $('[id$="ddl_work_gov"]').val('');
    $('[id$="ddl_work_city"]').val('');
    $('[id$="ddl_work_dist"]').val('');
    $('[id$="ddl_group_name"]').val('').trigger('change.select2');
    $('[id$="imgPlumberID"]').attr('src', '');
}