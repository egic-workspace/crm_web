﻿$(document).ready(function () {

    hideShowActivationReasonDiv();

    function reloadGrid() {
        $('[id$="RadGrid1"]').focus();
        $('[id$="RefreshButton"]').click().focus();
    }

    $(document).on('change', '[id$="ddl_isActive"]', function () {
        hideShowActivationReasonDiv();
    });

    $(document).on('click', '#btnCancelSave', function () {
        resetForm();
        reloadGrid();
    });

    $(document).on('keypress', '[type="text"]', function (e) {
        if (e.keyCode === 13) {
            reloadGrid();
            return false;
        }
    });

    $(document).on('keypress', '[type="tel"]', function (e) {
        if (e.keyCode === 13) {
            reloadGrid();
            return false;
        }
    });

    $(document).on('keypress', '[type="number"]', function (e) {
        if (e.keyCode === 13) {
            reloadGrid();
            return false;
        }
    });
    $(document).bind('keydown', "f4", function assets() {

        //collapseToggleBox('insert-form', true);
        $('[id$="txt_plumber_name"]').focus();
        return false;
    });
    $(document).bind('keydown', "f6", function assets() {
        //collapseToggleBox('insert-form', true);
        $('[id$="txt_plumber_name"]').focus();
        return false;
    });
    $(document).bind('keydown', "f7", function assets() {
        //collapseToggleBox('insert-form', true);
        $('[id$="txt_plumber_name"]').focus();
        return false;
    });
    $(document).bind('keydown', "f8", function assets() {
        reloadGrid();
        return false;
    });
    $(document).bind('keydown', "f10", function assets() {
        $('[id$="btn_save"]').click();
        return false;
    });

    doFullWidthScreen();
});

function hideShowActivationReasonDiv() {
    var ddl_isActive = $('[id$="ddl_isActive"]'),
        div_deactivation_resion = $("#div_deactivation_resion textarea");
    if (ddl_isActive.val() === "موقوف") {
        div_deactivation_resion.prop('disabled', false);
    }
    else {
        div_deactivation_resion.prop('disabled', true);
    }
}
function resetForm() {
    $('[id$="hidden_code"]').val('');
    $('[id$="txt_plumber_name"]').val('');
    $('[id$="txt_plumber_mobile"]').val('');
    $('[id$="txt_plumber_ID"]').val('');
    $('[id$="txt_home_address"]').val('');

    $('[id$="txt_plumber_phone"]').val('');
    $('[id$="txt_plumber_phone_2"]').val('');
    $('[id$="txt_plumber_birthdate"]').val('');
    $('[id$="txt_sales_rep"]').val('');

    $('[id$="txt_creator_name"]').val('');
    $('[id$="txt_rec_date"]').val('');
    $('[id$="txt_deactivation_reason"]').val('');
    $('[id$="txt_plumber_place"]').val('');

    $('[id$="ddl_isActive"]').val('');
    $('[id$="ddl_religion"]').val('');
    $('[id$="ddl_branch"]').val('');
    $('[id$="ddl_work_gov"]').val('');
    $('[id$="ddl_work_city"]').val('');
    $('[id$="ddl_work_dist"]').val('');
    $('[id$="ddl_group_manager"]').val('');
    $('[id$="ddl_inspection_rep"]').val('');
    $('[id$="ddl_group_name"]').val('').trigger('change.select2');
    $('[id$="ddl_work_shop"]').val('').trigger('change.select2');
    $('[id$="imgPlumberID"]').attr('src', '');
}