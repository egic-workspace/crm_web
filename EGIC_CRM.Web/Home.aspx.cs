﻿using System;
using System.Web.UI.WebControls;

public partial class Home : System.Web.UI.Page
{
    private readonly UserService _userService;

    public Home()
    { 
        _userService = new UserService();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            Session[AppConsts.MAIN_MENU_ITEMS] = null;
            // essam update
            ddlModuls.Items.Add(new ListItem("إختر النظام", ""));
            var userModules = _userService.GetUserModules(Session[AppConsts.CURRENT_USERNAME].ToString());
            foreach (var item in userModules)
                ddlModuls.Items.Add(new ListItem(item.Text, item.URL));
        }
    }

    protected void btnOpenUrl_Click(object sender, EventArgs e)
    {
        string selectedUrl = ddlModuls.SelectedValue;
        if (string.IsNullOrWhiteSpace(selectedUrl))
            return;
        Response.Redirect(string.Format("~/Modules/{0}", selectedUrl));
    }

    protected void btn_LogOut_Click(object sender, EventArgs e)
    {
        Session[AppConsts.CURRENT_USERNAME] = string.Empty;
        Session[AppConsts.MAIN_MENU_ITEMS] = null;
        Response.Redirect("~/Default");
    }
}