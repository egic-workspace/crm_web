﻿<%@ Page Title="أنواع الإستفسارات" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="CRM_QUERY_TYPES.aspx.cs" Inherits="Modules_EG_CRM_QUERY_TYPES" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <script type='text/javascript'>
        function scrollToElement(id) {
            //Get target
            var target = document.getElementById(id).offsetTop;

            //Scrolls to that target location
            window.scrollTo(0, target);
        }
    </script>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                    <telerik:AjaxUpdatedControl ControlID="RadFilter1" />
                    <telerik:AjaxUpdatedControl ControlID="RadNotification1" />
                    <telerik:AjaxUpdatedControl ControlID="txt_query" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_query_class" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_active" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_dept" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_type" />
                    <telerik:AjaxUpdatedControl ControlID="chk_send_mail" />
                    <telerik:AjaxUpdatedControl ControlID="btn_save" />
                    <telerik:AjaxUpdatedControl ControlID="btn_update" />
                    <telerik:AjaxUpdatedControl ControlID="btn_clear" />
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="RadFilter1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="btn_save">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                    <telerik:AjaxUpdatedControl ControlID="RadNotification1" />
                    <telerik:AjaxUpdatedControl ControlID="txt_query" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_query_class" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_active" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_dept" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_type" />
                    <telerik:AjaxUpdatedControl ControlID="chk_send_mail" />
                    <telerik:AjaxUpdatedControl ControlID="btn_save" />
                    <telerik:AjaxUpdatedControl ControlID="btn_update" />
                    <telerik:AjaxUpdatedControl ControlID="btn_clear" />
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="btn_update">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                    <telerik:AjaxUpdatedControl ControlID="RadNotification1" />
                    <telerik:AjaxUpdatedControl ControlID="txt_query" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_query_class" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_active" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_dept" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_type" />
                    <telerik:AjaxUpdatedControl ControlID="chk_send_mail" />
                    <telerik:AjaxUpdatedControl ControlID="btn_save" />
                    <telerik:AjaxUpdatedControl ControlID="btn_update" />
                    <telerik:AjaxUpdatedControl ControlID="btn_clear" />
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="btn_clear">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                    <telerik:AjaxUpdatedControl ControlID="RadFilter1" />
                    <telerik:AjaxUpdatedControl ControlID="RadNotification1" />
                    <telerik:AjaxUpdatedControl ControlID="txt_query" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_query_class" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_active" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_dept" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_type" />
                    <telerik:AjaxUpdatedControl ControlID="chk_send_mail" />
                    <telerik:AjaxUpdatedControl ControlID="btn_save" />
                    <telerik:AjaxUpdatedControl ControlID="btn_update" />
                    <telerik:AjaxUpdatedControl ControlID="btn_clear" />
                </UpdatedControls>
            </telerik:AjaxSetting>

        </AjaxSettings>
    </telerik:RadAjaxManager>

        <div class="raw clearfix">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h5 class="box-title">
                        بيانات الإستفسارات
                    </h5>
                </div>
                <div class="box-body">
                    <div class="col-md-3">
                        <div class="form-group form-group-sm">
                            <label for="txt_query">الإستفسار</label>
                            <asp:TextBox ID="txt_query" runat="server" CssClass="form-control" placeholder="الإستفسار"></asp:TextBox>
                        </div>

                        <div class="form-group form-group-sm">
                            <label for="ddl_query_class">التصنيف</label>
                            <asp:DropDownList ID="ddl_query_class" runat="server" CssClass="form-control btn-sm">
                                <asp:ListItem Value="0">إختر التصنيف</asp:ListItem>
                                <asp:ListItem Value="Q">استفسار</asp:ListItem>
                                <asp:ListItem Value="C">شكوي</asp:ListItem>
                                <asp:ListItem Value="S">اقتراح</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group form-group-sm">
                            <label for="ddl_active">نشط</label>
                            <asp:DropDownList ID="ddl_active" runat="server" CssClass="form-control btn-sm">
                                <asp:ListItem Value="Y">نعم</asp:ListItem>
                                <asp:ListItem Value="N">لا</asp:ListItem>
                            </asp:DropDownList>
                        </div>


                        <div class="form-group form-group-sm">
                            <label for="ddl_dept">الإدارة</label>
                            <asp:DropDownList ID="ddl_dept" runat="server" CssClass="form-control btn-sm">
                                <asp:ListItem Value="0">إختر الإدارة</asp:ListItem>
                                <asp:ListItem Value="خدمة العملاء ">خدمة العملاء</asp:ListItem>
                                <asp:ListItem Value="المبيعات">المبيعات</asp:ListItem>
                                <asp:ListItem Value="التسويق">التسويق</asp:ListItem>
                                <asp:ListItem Value="المصنع">المصنع</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group form-group-sm">
                            <label for="ddl_type">نوع الشكوي</label>
                            <asp:DropDownList ID="ddl_type" runat="server" CssClass="form-control btn-sm">
                                <asp:ListItem Value="0">إختر نوع الشكوي</asp:ListItem>
                                <asp:ListItem Value="مندوب">مندوب</asp:ListItem>
                                <asp:ListItem Value="مشرف">مشرف</asp:ListItem>
                                <asp:ListItem Value="شركة">شركة</asp:ListItem>
                                <asp:ListItem Value="منتج">منتج</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form-group form-group-sm">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" runat="server" id="chk_send_mail" />
                                    إرسال ميل
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                    </div>
                </div>
                <div class="box-footer">
                    <asp:Button ID="btn_save" runat="server" Text="حفظ" CssClass="btn btn-primary" OnClick="btn_save_Click" />
                    <asp:Button ID="btn_clear" runat="server" Text="جديد" CssClass="btn btn-success" OnClick="btn_clear_Click" />
                    <asp:Button ID="btn_update" runat="server" Text="تعديل" CssClass="btn btn-info" OnClick="btn_query_Click" />
                </div>

            </div>

            <div class="box box-danger">
                <div class="box-header with-border">
                    <h5 class="box-title">
                        تفاصيل الإستفسارات
                    </h5>
                </div>
                <div class="box-body">
                    <div class="raw clearfix" dir="rtl">
                        <telerik:RadFilter ID="RadFilter1" runat="server" FilterContainerID="RadGrid1" Width="100%" UseBetweenValidation="False" ApplyButtonText="بحـث" Skin="Bootstrap" />
                        <telerik:RadMultiPage ID="RadMultiPage1" runat="server">
                            <telerik:RadPageView ID="RadPageView1" Width="100%" Selected="True" Font-Bold="True" Font-Size="12pt" runat="server">
                                <telerik:RadGrid 
                                    runat="server" 
                                    ID="RadGrid1" 
                                    AllowFilteringByColumn="True" 
                                    AllowPaging="True" 
                                    AllowSorting="True" 
                                    ShowGroupPanel="True" 
                                    AutoGenerateColumns="False" 
                                    OnNeedDataSource="RadGrid1_NeedDataSource" 
                                    OnItemDataBound="RadGrid1_ItemDataBound" 
                                    OnDeleteCommand="RadGrid1_DeleteCommand" 
                                    OnUpdateCommand="RadGrid1_UpdateCommand" 
                                    OnItemCreated="RadGrid1_ItemCreated" 
                                    OnItemCommand="RadGrid1_ItemCommand" 
                                    Skin="Bootstrap">

                                    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
                                    </HeaderContextMenu>

                                    <MasterTableView CommandItemDisplay="Top" AllowPaging="true" Dir="RTL">

                                        <CommandItemSettings ShowExportToCsvButton="true" ShowExportToExcelButton="true" ShowExportToWordButton="true" ShowAddNewRecordButton="false" ShowExportToPdfButton="false" />

                                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                                        </RowIndicatorColumn>

                                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                                        </ExpandCollapseColumn>

                                        <Columns>

                                            <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="UPDATE" FilterControlAltText="Filter column column" Text="تعديل" UniqueName="column" ImageUrl="~/Content/images/edit.gif">
                                                <ItemStyle BackColor="Transparent" BorderStyle="Solid" Font-Bold="True" CssClass="btn-updt" Font-Names="Arial" Font-Size="10pt" ForeColor="Black" HorizontalAlign="Center" />
                                            </telerik:GridButtonColumn>

                                            <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="DELETE" FilterControlAltText="Filter column1 column" ImageUrl="~/Content/images/delete.gif" Text="حذف" UniqueName="column1" ConfirmDialogHeight="200px" ConfirmDialogType="RadWindow" ConfirmDialogWidth="400px" ConfirmText="هل تريد حذف هذه البيانات ؟" ConfirmTitle="حذف البيانات">
                                            </telerik:GridButtonColumn>

                                            <telerik:GridTemplateColumn 
                                                DataField="NAME" 
                                                ShowFilterIcon="false" 
                                                AutoPostBackOnFilter="true" 
                                                CurrentFilterFunction="Contains" 
                                                GroupByExpression="NAME Group By NAME" 
                                                HeaderText="الإستفسار / الشكوي" 
                                                SortExpression="NAME" 
                                                UniqueName="NAME">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="IDTextBox" Enabled="false" runat="server" Text='<%# Bind("NAME") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="IDLabel" runat="server" Text='<%# Eval("NAME") %>'></asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>

                                            <telerik:GridTemplateColumn 
                                                DataField="ACTIVE" 
                                                ShowFilterIcon="false" 
                                                AutoPostBackOnFilter="true" 
                                                CurrentFilterFunction="Contains" 
                                                Visible="false" 
                                                GroupByExpression="ACTIVE Group By ACTIVE" 
                                                HeaderText="الحالة" 
                                                SortExpression="ACTIVE" 
                                                UniqueName="ACTIVE">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="ACTIVETextBox" runat="server" Text='<%# Bind("ACTIVE") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="ACTIVELabel" runat="server" Text='<%# Eval("ACTIVE") %>'></asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>

                                            <telerik:GridTemplateColumn 
                                                DataField="active_desc" 
                                                ShowFilterIcon="false" 
                                                AutoPostBackOnFilter="true" 
                                                CurrentFilterFunction="EqualTo" 
                                                GroupByExpression="active_desc Group By active_desc" 
                                                HeaderText="الحالة " 
                                                SortExpression="active_desc" 
                                                UniqueName="active_desc">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="active_descTextBox" Enabled="true" runat="server" Text='<%# Bind("active_desc") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="active_descLabel" runat="server" Text='<%# Eval("active_desc") %>'></asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>

                                            <telerik:GridTemplateColumn 
                                                DataField="qry_type_desc" 
                                                ShowFilterIcon="false" 
                                                AutoPostBackOnFilter="true" 
                                                CurrentFilterFunction="EqualTo" 
                                                GroupByExpression="qry_type_desc Group By qry_type_desc" 
                                                HeaderText="التصنيف" 
                                                SortExpression="qry_type_desc" 
                                                UniqueName="qry_type_desc">

                                                <ItemTemplate>
                                                    <asp:Label ID="MNAMELabel" runat="server" Text='<%# Eval("qry_type_desc") %>'></asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>

                                            <telerik:GridTemplateColumn 
                                                DataField="qry_type" 
                                                ShowFilterIcon="false" 
                                                AutoPostBackOnFilter="true" 
                                                CurrentFilterFunction="EqualTo" 
                                                Visible="false" 
                                                GroupByExpression="qry_type Group By qry_type" 
                                                HeaderText="التصنيف" 
                                                SortExpression="qry_type" 
                                                UniqueName="qry_type">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="qry_typeTextBox" Enabled="false" runat="server" Text='<%# Bind("qry_type") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="qry_typeLabel" runat="server" Text='<%# Eval("qry_type") %>'></asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>

                                            <telerik:GridTemplateColumn 
                                                DataField="serv_dept" 
                                                ShowFilterIcon="false" 
                                                AutoPostBackOnFilter="true" 
                                                CurrentFilterFunction="EqualTo" 
                                                GroupByExpression="serv_dept Group By serv_dept" 
                                                HeaderText="الإدارة المدعومة" 
                                                SortExpression="serv_dept" 
                                                UniqueName="serv_dept">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="serv_deptTextBox" Enabled="false" runat="server" Text='<%# Bind("serv_dept") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="serv_deptLabel" runat="server" Text='<%# Eval("serv_dept") %>'></asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>

                                            <telerik:GridTemplateColumn 
                                                DataField="sendM_desc" 
                                                ShowFilterIcon="false" 
                                                AutoPostBackOnFilter="true" 
                                                CurrentFilterFunction="Contains" 
                                                GroupByExpression="sendM_desc Group By sendM_desc" 
                                                HeaderText="حالة الإرسال" 
                                                SortExpression="sendM_desc" 
                                                UniqueName="sendM_desc">

                                                <ItemTemplate>
                                                    <asp:Label ID="sendM_descLabel" runat="server" Text='<%# Eval("sendM_desc") %>'></asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>

                                            <telerik:GridTemplateColumn 
                                                DataField="send_mail" 
                                                Visible="false" 
                                                ShowFilterIcon="false" 
                                                AutoPostBackOnFilter="true" 
                                                CurrentFilterFunction="Contains" 
                                                GroupByExpression="send_mail GROUP BY send_mail" 
                                                HeaderText="send_mail" 
                                                SortExpression="send_mail" 
                                                UniqueName="send_mail">

                                                <ItemTemplate>
                                                    <asp:Label ID="send_mailLabel" runat="server" Text='<%# Eval("send_mail") %>'></asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>

                                            <telerik:GridTemplateColumn 
                                                DataField="comp_class" 
                                                ShowFilterIcon="false" 
                                                AutoPostBackOnFilter="true" 
                                                CurrentFilterFunction="Contains" 
                                                GroupByExpression="comp_class GROUP BY comp_class" 
                                                HeaderText="نوع الشكوي" 
                                                SortExpression="comp_class" 
                                                UniqueName="comp_class">

                                                <ItemTemplate>
                                                    <asp:Label ID="comp_classLabel" runat="server" Text='<%# Eval("comp_class") %>'></asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>

                                        </Columns>

                                        <EditFormSettings>
                                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                            </EditColumn>
                                        </EditFormSettings>

                                    </MasterTableView>

                                    <GroupingSettings CollapseAllTooltip="Collapse all groups" />

                                    <ValidationSettings EnableModelValidation="false" EnableValidation="false" ValidationGroup="10" />

                                    <ClientSettings AllowColumnsReorder="True" AllowDragToGroup="True" ReorderColumnsOnClient="True">
                                        <Selecting AllowRowSelect="True" />
                                        <Scrolling UseStaticHeaders="True" />
                                    </ClientSettings>

                                    <FilterMenu EnableImageSprites="False">
                                    </FilterMenu>

                                </telerik:RadGrid>

                            </telerik:RadPageView>

                        </telerik:RadMultiPage>

                        <telerik:RadNotification ID="RadNotification1" runat="server" Position="Center" Skin="Bootstrap" Font-Bold="True" Font-Names="Arial" Font-Size="15pt">
                            <NotificationMenu ID="TitleMenu">
                            </NotificationMenu>
                        </telerik:RadNotification>

                    </div>
                </div>
            </div>
        </div>

</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="JsScripts" runat="Server">
    <script src="../../Content/js/modules/eg/crm_query_types.js"></script>
</asp:Content>
