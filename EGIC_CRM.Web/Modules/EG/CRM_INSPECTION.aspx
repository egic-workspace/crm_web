﻿<%@ Page Title="المعاينات" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="CRM_INSPECTION.aspx.cs" Inherits="Modules_EG_CRM_INSPECTION" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">

    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>

            <telerik:AjaxSetting AjaxControlID="ddl_work_gov">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ddl_work_city" />
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="ddl_work_city">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ddl_work_dist" />
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="ddl_birth_gov">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ddl_birth_city" />
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="ddl_birth_city">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ddl_birth_dist" />
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="ddl_group_name">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="txt_sales_rep" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_group_manager" />
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="ddl_group_manager">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ddl_rep" />
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="ddl_inspection_gov">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_city" />
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="ddl_inspection_city">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_dist" />
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="ddl_shop_gov">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ddl_shop_city" />
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="ddl_shop_city">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ddl_shop_dist" />
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="ddl_shop_dist">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ddl_buy_shop" />
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="RadGridInspectionProducts">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridInspectionProducts" />
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="RadGridInspectionCertificates">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridInspectionCertificates" />
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="RadTreeView1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="btnNewCall" />
                    <telerik:AjaxUpdatedControl ControlID="RadLightBox1" />
                    
                    <telerik:AjaxUpdatedControl ControlID="RadGridInspectionProducts" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridInspectionCertificates" />

                    <telerik:AjaxUpdatedControl ControlID="hidden_code" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_religion" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_isActive" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_branch" />

                    <telerik:AjaxUpdatedControl ControlID="ddl_work_gov" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_work_city" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_work_dist" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_work_shop" />

                    <telerik:AjaxUpdatedControl ControlID="ddl_birth_gov" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_birth_city" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_birth_dist" />

                    <telerik:AjaxUpdatedControl ControlID="ddl_group_name" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_group_manager" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_rep" />

                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_name" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_mobile" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_ID" />
                    <telerik:AjaxUpdatedControl ControlID="txt_home_address" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_nickname" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_phone" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_phone_2" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_birthdate" />
                    <telerik:AjaxUpdatedControl ControlID="txt_deactivation_resion" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_place" />
                    <telerik:AjaxUpdatedControl ControlID="txt_sales_rep" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_creator" />
                    <telerik:AjaxUpdatedControl ControlID="txt_rec_date" />


                    <telerik:AjaxUpdatedControl ControlID="hiddenInspectionCode" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_code" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_printed_NO" />
                    <telerik:AjaxUpdatedControl ControlID="txt_owner_name" />
                    <telerik:AjaxUpdatedControl ControlID="txt_owner_mobile" />
                    <telerik:AjaxUpdatedControl ControlID="txt_owner_tel" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_address" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_planed_date" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_year" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_planed_from_time" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_planed_to_time" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_qa_resp" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_notes" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_data_entry_notes" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_total_points" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_rep_points" />
                    <telerik:AjaxUpdatedControl ControlID="txt_grantee_status_reason" />
                    <telerik:AjaxUpdatedControl ControlID="txt_stop_printing_reoprts_reason" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_reject_reason" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_status_note" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_creator" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_rec_date" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_source" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_description" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_gov" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_city" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_dist" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_situation" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_type" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_place_type" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_shop_gov" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_shop_city" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_shop_dist" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_buy_shop" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_rep" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_branch" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_grantee_status" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_stop_printing_reoprts" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_order_status" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_status" />

                    <telerik:AjaxUpdatedControl ControlID="lblErrorMessage" />
                    <telerik:AjaxUpdatedControl ControlID="lblSuccessMessage" />
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="RadLightBox1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="txtCallerNumber" />
                    <telerik:AjaxUpdatedControl ControlID="btnNewCall" />
                    <telerik:AjaxUpdatedControl ControlID="btnSearch" />
                    <telerik:AjaxUpdatedControl ControlID="RadLightBox1" />
                    
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="btnSearch">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="txtCallerNumber" />
                    <telerik:AjaxUpdatedControl ControlID="btnNewCall" />
                    <telerik:AjaxUpdatedControl ControlID="lblCallerType" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridInspectionProducts" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridInspectionCertificates" />
                    <telerik:AjaxUpdatedControl ControlID="RadLightBox1" />
                    <telerik:AjaxUpdatedControl ControlID="RadTreeView1" />

                    <telerik:AjaxUpdatedControl ControlID="hidden_code" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_religion" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_isActive" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_branch" />

                    <telerik:AjaxUpdatedControl ControlID="ddl_work_shop" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_work_gov" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_work_city" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_work_dist" />

                    <telerik:AjaxUpdatedControl ControlID="ddl_birth_gov" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_birth_city" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_birth_dist" />

                    <telerik:AjaxUpdatedControl ControlID="ddl_group_name" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_group_manager" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_rep" />

                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_name" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_mobile" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_ID" />
                    <telerik:AjaxUpdatedControl ControlID="txt_home_address" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_nickname" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_phone" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_phone_2" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_birthdate" />
                    <telerik:AjaxUpdatedControl ControlID="txt_deactivation_resion" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_place" />
                    <telerik:AjaxUpdatedControl ControlID="txt_sales_rep" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_creator" />
                    <telerik:AjaxUpdatedControl ControlID="txt_rec_date" />

                    <telerik:AjaxUpdatedControl ControlID="hiddenInspectionCode" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_code" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_printed_NO" />
                    <telerik:AjaxUpdatedControl ControlID="txt_owner_name" />
                    <telerik:AjaxUpdatedControl ControlID="txt_owner_mobile" />
                    <telerik:AjaxUpdatedControl ControlID="txt_owner_tel" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_address" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_planed_date" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_year" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_planed_from_time" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_planed_to_time" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_qa_resp" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_notes" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_data_entry_notes" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_total_points" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_rep_points" />
                    <telerik:AjaxUpdatedControl ControlID="txt_grantee_status_reason" />
                    <telerik:AjaxUpdatedControl ControlID="txt_stop_printing_reoprts_reason" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_reject_reason" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_status_note" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_creator" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_rec_date" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_source" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_description" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_gov" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_city" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_dist" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_situation" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_type" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_place_type" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_shop_gov" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_shop_city" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_shop_dist" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_buy_shop" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_rep" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_branch" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_grantee_status" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_stop_printing_reoprts" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_order_status" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_status" />

                    <telerik:AjaxUpdatedControl ControlID="lblErrorMessage" />
                    <telerik:AjaxUpdatedControl ControlID="lblSuccessMessage" />

                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="btnReset">
                <UpdatedControls>

                    <telerik:AjaxUpdatedControl ControlID="RadGridInspectionProducts" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridInspectionCertificates" />

                    <telerik:AjaxUpdatedControl ControlID="hidden_code" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_religion" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_isActive" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_branch" />

                    <telerik:AjaxUpdatedControl ControlID="ddl_work_shop" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_work_gov" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_work_city" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_work_dist" />

                    <telerik:AjaxUpdatedControl ControlID="ddl_birth_gov" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_birth_city" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_birth_dist" />

                    <telerik:AjaxUpdatedControl ControlID="ddl_group_name" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_group_manager" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_rep" />

                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_name" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_mobile" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_ID" />
                    <telerik:AjaxUpdatedControl ControlID="txt_home_address" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_nickname" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_phone" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_phone_2" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_birthdate" />
                    <telerik:AjaxUpdatedControl ControlID="txt_deactivation_resion" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_place" />
                    <telerik:AjaxUpdatedControl ControlID="txt_sales_rep" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_creator" />
                    <telerik:AjaxUpdatedControl ControlID="txt_rec_date" />

                    <telerik:AjaxUpdatedControl ControlID="hiddenInspectionCode" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_code" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_printed_NO" />
                    <telerik:AjaxUpdatedControl ControlID="txt_owner_name" />
                    <telerik:AjaxUpdatedControl ControlID="txt_owner_mobile" />
                    <telerik:AjaxUpdatedControl ControlID="txt_owner_tel" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_address" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_planed_date" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_year" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_planed_from_time" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_planed_to_time" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_qa_resp" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_notes" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_data_entry_notes" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_total_points" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_rep_points" />
                    <telerik:AjaxUpdatedControl ControlID="txt_grantee_status_reason" />
                    <telerik:AjaxUpdatedControl ControlID="txt_stop_printing_reoprts_reason" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_reject_reason" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_status_note" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_creator" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_rec_date" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_source" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_description" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_gov" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_city" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_dist" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_situation" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_type" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_place_type" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_shop_gov" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_shop_city" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_shop_dist" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_buy_shop" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_rep" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_branch" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_grantee_status" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_stop_printing_reoprts" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_order_status" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_status" />

                    <telerik:AjaxUpdatedControl ControlID="lblErrorMessage" />
                    <telerik:AjaxUpdatedControl ControlID="lblSuccessMessage" />
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="btnSaveAll">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="btnNewCall" />

                    <telerik:AjaxUpdatedControl ControlID="hidden_code" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_religion" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_isActive" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_branch" />

                    <telerik:AjaxUpdatedControl ControlID="ddl_work_shop" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_work_gov" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_work_city" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_work_dist" />

                    <telerik:AjaxUpdatedControl ControlID="ddl_birth_gov" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_birth_city" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_birth_dist" />

                    <telerik:AjaxUpdatedControl ControlID="ddl_group_name" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_group_manager" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_rep" />

                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_name" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_mobile" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_ID" />
                    <telerik:AjaxUpdatedControl ControlID="txt_home_address" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_nickname" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_phone" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_phone_2" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_birthdate" />
                    <telerik:AjaxUpdatedControl ControlID="txt_deactivation_resion" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_place" />
                    <telerik:AjaxUpdatedControl ControlID="txt_sales_rep" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_creator" />
                    <telerik:AjaxUpdatedControl ControlID="txt_rec_date" />


                    <telerik:AjaxUpdatedControl ControlID="hiddenInspectionCode" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_code" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_printed_NO" />
                    <telerik:AjaxUpdatedControl ControlID="txt_owner_name" />
                    <telerik:AjaxUpdatedControl ControlID="txt_owner_mobile" />
                    <telerik:AjaxUpdatedControl ControlID="txt_owner_tel" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_address" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_planed_date" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_year" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_planed_from_time" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_planed_to_time" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_qa_resp" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_notes" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_data_entry_notes" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_total_points" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_rep_points" />
                    <telerik:AjaxUpdatedControl ControlID="txt_grantee_status_reason" />
                    <telerik:AjaxUpdatedControl ControlID="txt_stop_printing_reoprts_reason" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_reject_reason" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_status_note" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_creator" />
                    <telerik:AjaxUpdatedControl ControlID="txt_inspection_rec_date" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_source" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_description" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_gov" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_city" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_dist" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_situation" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_type" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_place_type" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_shop_gov" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_shop_city" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_shop_dist" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_buy_shop" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_rep" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_branch" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_grantee_status" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_stop_printing_reoprts" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_order_status" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_status" />

                    <telerik:AjaxUpdatedControl ControlID="lblErrorMessage" />
                    <telerik:AjaxUpdatedControl ControlID="lblSuccessMessage" />
                </UpdatedControls>
            </telerik:AjaxSetting>

        </AjaxSettings>
    </telerik:RadAjaxManager>

    <div class="raw clearfix">
        
       
        <telerik:RadLightBox
            RenderMode="Lightweight"
            runat="server"
            ID="RadLightBox1"
            Height="50px"
            Width="400px"
            Modal="true"
            Visible="true"
            ShowCloseButton="false"
            ShowLoadingPanel="true"
            ZIndex="100000"
            Skin="Bootstrap"
            ShowMaximizeButton="false"
            EnableAriaSupport="false"
            DataTargetControlIDField="ControlID">
            <Items>
                <telerik:RadLightBoxItem TargetControlID="btnNewCall">
                    <ItemTemplate>
                        <div class="form-inline">
                            <div class="checkbox">
                                <label>
                                    <asp:RadioButton runat="server" ID="radioByMobile" GroupName="search-by" Checked="true" />
                                    موبايل المالك او السباك
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <asp:RadioButton runat="server" ID="radioByInspectionCode" GroupName="search-by" />
                                    رقم امر الشغل
                                </label>
                            </div>
                        </div>
                        <br />
                        <div class="input-group ">
                            <asp:TextBox ID="txtSearchBy" runat="server" CssClass="form-control" placeholder="بحث عن..."></asp:TextBox>
                            <span class="input-group-btn">
                                <asp:Button ID="btnSearch" runat="server" Text="بحث" CssClass="btn btn-danger btn-flat" OnClick="btnSearch_Click" CausesValidation="false" />
                            </span>
                        </div>
                    </ItemTemplate>
                </telerik:RadLightBoxItem>
            </Items>
        </telerik:RadLightBox>

        <p class="alert alert-danger" runat="server" id="lblErrorMessage" visible="false"></p>
        <div class="alert alert-success alert-dismissable" runat="server" id="lblSuccessMessage" visible="false">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            تم الحفظ بنجاح
        </div>

        <telerik:RadWindow 
            RenderMode="Lightweight" 
            ID="modalPopup" 
            runat="server" 
            Width="360px" 
            Height="365px" 
            Modal="true" 
            OffsetElementID="main" 
            CenterIfModal="true"
            KeepInScreenBounds="true"
            Visible="true"
            Style="z-index: 100001;">
            <ContentTemplate>
                
            </ContentTemplate>
        </telerik:RadWindow>

        <div class="col-md-2">
            <div class="box  ">
                <div class="box-body">
                    <label for="RadTreeView1" runat="server" id="lblCallerType"></label>

                     <telerik:RadTreeView 
                         ID="RadTreeView1" 
                         Runat="server" 
                         OnNodeClick="RadTreeView1_NodeClick" 
                         CausesValidation="false" 
                         Skin="Bootstrap"></telerik:RadTreeView>
                    <%--
                        <telerik:RadGrid
                        RenderMode="Lightweight"
                        ID="RGridPlumbersInspections"
                        OnPreRender="RGridPlumbersInspections_PreRender"
                        OnDetailTableDataBind="RGridPlumbersInspections_DetailTableDataBind"
                        OnItemCommand="RGridPlumbersInspections_ItemCommand"
                        runat="server"
                        AutoGenerateColumns="False"
                        PageSize="20"
                        
                        GridLines="None">
                        <ClientSettings AllowKeyboardNavigation="true" EnablePostBackOnRowClick="true" EnableRowHoverStyle="true">
                            <Selecting AllowRowSelect="true"></Selecting>
                        </ClientSettings>
                        <MasterTableView
                            Dir="RTL"
                            DataKeyNames="CUST_CODE">
                            <DetailTables>
                                <telerik:GridTableView
                                    DataKeyNames="INSPECTION_CODE"
                                    Width="100%"
                                    Dir="RTL"
                                    runat="server">
                                    <Columns>
                                        <telerik:GridBoundColumn Display="false" HeaderButtonType="TextButton" DataField="INSPECTION_CODE" UniqueName="INSPECTION_CODE">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="" HeaderButtonType="TextButton" DataField="INSPECTION_NAME" UniqueName="INSPECTION_NAME">
                                        </telerik:GridBoundColumn>
                                    </Columns>
                                </telerik:GridTableView>
                            </DetailTables>
                            <Columns>
                                <telerik:GridBoundColumn Display="false" DataField="CUST_CODE" UniqueName="CUST_CODE">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="" DataField="CUST_NAME" UniqueName="CUST_NAME">
                                </telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                        --%>

                </div>
            </div>
        </div>
        <div class="col-md-10">
            <div class="box">
                <div class="box-header ">
                    <div class="box-title">
                         
                    <asp:Button runat="server" ID="btnNewCall" ClientIDMode="Static" Text="اتصال جديد" CausesValidation="false" CssClass="btn btn-info btn-sm" />

                    </div>
                </div>
                <div class="box-body">


                    <%--بيانات السباك--%>
                    <div class="box box-primary ">
                        <div class="box-header with-border">
                            <div class="box-title">
                                بيانات السباك
                            </div>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body">

                            <div class="col-md-12">

                                <div class="col-md-2">
                                    <div class="form-group form-group-sm">
                                        <label for="txt_plumber_name">اسم السباك</label>
                                        <span class="danger text-bold">* </span>
                                        <asp:RequiredFieldValidator runat="server" ErrorMessage="مطلوب" ForeColor="Red" ControlToValidate="txt_plumber_name"></asp:RequiredFieldValidator>
                                        <asp:TextBox ID="txt_plumber_name" runat="server" CssClass="form-control" placeholder="اسم السباك (مطلوب)"></asp:TextBox>
                                        <asp:HiddenField ID="hidden_code" runat="server" />
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="txt_plumber_mobile">الموبايل</label>
                                        <span class="danger text-bold">* </span>
                                        <asp:RegularExpressionValidator runat="server" ErrorMessage="غير صحيح" ForeColor="Red" ValidationExpression="[0-9]{7,12}" ControlToValidate="txt_plumber_mobile"></asp:RegularExpressionValidator>
                                        <asp:TextBox ID="txt_plumber_mobile" runat="server" MaxLength="12" CssClass="form-control" placeholder="الموبايل (مطلوب)"></asp:TextBox>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="txt_plumber_ID">رقم البطاقة</label>
                                        <span class="danger text-bold">* </span>
                                        <asp:RegularExpressionValidator runat="server" ErrorMessage="غير صحيح" ForeColor="Red" ValidationExpression="[0-9]{14}" ControlToValidate="txt_plumber_ID"></asp:RegularExpressionValidator>
                                        <asp:TextBox ID="txt_plumber_ID" runat="server" MaxLength="14" CssClass="form-control" placeholder="رقم البطاقة (مطلوب)"></asp:TextBox>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="txt_home_address">عنوان المنزل</label>
                                        <span class="danger text-bold">* </span>
                                        <asp:RequiredFieldValidator runat="server" ErrorMessage="مطلوب" ForeColor="Red" ControlToValidate="txt_home_address"></asp:RequiredFieldValidator>
                                        <asp:TextBox ID="txt_home_address" runat="server" CssClass="form-control" placeholder="عنوان المنزل (مطلوب)"></asp:TextBox>
                                    </div>

                                </div>

                                <div class="col-md-2">

                                    <div class="form-group form-group-sm">
                                        <label for="txt_plumber_nickname">اسم الشهرة</label>
                                        <asp:TextBox ID="txt_plumber_nickname" runat="server" CssClass="form-control" placeholder="اسم الشهرة"></asp:TextBox>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="txt_plumber_phone">التليفون</label>
                                        <asp:TextBox ID="txt_plumber_phone" runat="server" TextMode="Phone" CssClass="form-control" placeholder="التليفون"></asp:TextBox>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="txt_plumber_phone_2">تليفون إضافي</label>
                                        <asp:TextBox ID="txt_plumber_phone_2" runat="server" TextMode="Phone" CssClass="form-control" placeholder="تليفون إضافي"></asp:TextBox>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="ddl_gender">النوع</label>
                                        <asp:DropDownList ID="ddl_gender" runat="server" CssClass="form-control btn-sm">
                                            <asp:ListItem Value="ذكر">ذكر</asp:ListItem>
                                            <asp:ListItem Value="انثى">انثى</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                </div>

                                <div class="col-md-2">

                                    <div class="form-group form-group-sm">
                                        <label for="ddl_birth_gov">محافظة الميلاد</label>
                                        <asp:DropDownList ID="ddl_birth_gov" runat="server" CssClass="form-control btn-sm" OnSelectedIndexChanged="ddl_birth_gov_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Value="">إختر...</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="ddl_birth_city">حي الميلاد</label>
                                        <asp:DropDownList ID="ddl_birth_city" runat="server" Width="100%" CssClass="form-control btn-sm btn-block " OnSelectedIndexChanged="ddl_birth_city_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Value="">إختر...</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="ddl_birth_dist">منطقة الميلاد</label>
                                        <asp:DropDownList ID="ddl_birth_dist" runat="server" Width="100%" CssClass="form-control btn-sm btn-block ">
                                            <asp:ListItem Value="">إختر...</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="txt_plumber_birthdate">تاريخ الميلاد</label>
                                        <telerik:RadDatePicker runat="server" ID="txt_plumber_birthdate" Width="100%"></telerik:RadDatePicker>
                                    </div>
                                </div>

                                <div class="col-md-2">

                                    <div class="form-group form-group-sm">
                                        <label for="ddl_work_gov">محافظةالعمل</label>
                                        <asp:DropDownList ID="ddl_work_gov" runat="server" CssClass="form-control btn-sm" OnSelectedIndexChanged="ddl_work_gov_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Value="">إختر...</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="ddl_work_city">حي العمل</label>
                                        <asp:DropDownList ID="ddl_work_city" runat="server" Width="100%" CssClass="form-control btn-sm btn-block " OnSelectedIndexChanged="ddl_work_city_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Value="">إختر...</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="ddl_work_dist">منطقة العمل</label>
                                        <asp:DropDownList ID="ddl_work_dist" runat="server" Width="100%" CssClass="form-control btn-sm btn-block " OnSelectedIndexChanged="ddl_work_dist_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Value="">إختر...</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="ddl_work_shop">المحل</label>
                                        <asp:DropDownList ID="ddl_work_shop" runat="server" Width="100%" CssClass="form-control btn-sm btn-block">
                                            <asp:ListItem Value="">إختر...</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                </div>

                                <div class="col-md-2">

                                    <div class="form-group form-group-sm">
                                        <label for="ddl_group_name">اسم المجموعة</label>
                                        <asp:DropDownList ID="ddl_group_name" runat="server" Width="100%" CssClass="form-control btn-sm btn-block select2" OnSelectedIndexChanged="ddl_group_name_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Value="">إختر...</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="txt_sales_rep">مندوب البيع</label>
                                        <asp:TextBox ID="txt_sales_rep" runat="server" CssClass="form-control" placeholder="مندوب البيع" ReadOnly="true" Enabled="false"></asp:TextBox>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="ddl_group_manager">رئيس المجموعة</label>
                                        <asp:DropDownList ID="ddl_group_manager" runat="server" CssClass="form-control btn-sm" OnSelectedIndexChanged="ddl_group_manager_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Value="">إختر...</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="ddl_rep">مندوب المتابعة</label>
                                        <asp:DropDownList ID="ddl_rep" runat="server" CssClass="form-control btn-sm">
                                            <asp:ListItem Value="">إختر...</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                </div>

                                <div class="col-md-2">

                                    <div class="form-group form-group-sm">
                                        <label for="txt_plumber_nationality">الجنسية</label>
                                        <asp:TextBox ID="txt_plumber_nationality" runat="server" CssClass="form-control" placeholder="الجنسية" Text="مصري"></asp:TextBox>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="ddl_religion">الديانة</label>
                                        <asp:DropDownList ID="ddl_religion" runat="server" CssClass="form-control btn-sm">
                                            <asp:ListItem Value="">إختر...</asp:ListItem>
                                            <asp:ListItem Value="1">مسلم</asp:ListItem>
                                            <asp:ListItem Value="0">مسيحي</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="ddl_isActive">حالة العمل</label>
                                        <asp:DropDownList ID="ddl_isActive" runat="server" CssClass="form-control btn-sm">
                                            <asp:ListItem Value="">إختر...</asp:ListItem>
                                            <asp:ListItem Value="يعمل">يعمل</asp:ListItem>
                                            <asp:ListItem Value="موقوف">موقوف</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form-group form-group-sm" id="div_deactivation_resion">
                                        <label for="txt_deactivation_reason">سبب الوقف</label>
                                        <asp:TextBox ID="txt_deactivation_reason" runat="server" TextMode="MultiLine" Rows="2" CssClass="form-control" placeholder="سبب الوقف"></asp:TextBox>
                                    </div>

                                </div>

                            </div>

                            <div class="col-md-12">

                                <div class="col-md-2">

                                    <div class="form-group form-group-sm">
                                        <label for="txt_plumber_place">مكان التواجد</label>
                                        <asp:TextBox ID="txt_plumber_place" runat="server" CssClass="form-control" placeholder="مكان التواجد"></asp:TextBox>
                                    </div>


                                </div>

                                <div class="col-md-2">

                                    <div class="form-group form-group-sm">
                                        <label for="ddl_branch">الفرع</label>
                                        <asp:DropDownList ID="ddl_branch" runat="server" CssClass="form-control btn-sm">
                                            <asp:ListItem Value="">إختر...</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                </div>

                                <div class="col-md-2">

                                    <div class="form-group form-group-sm">
                                        <label for="txt_total_points">إجمالي النقاط</label>
                                        <asp:TextBox ID="txt_total_points" runat="server" CssClass="form-control" placeholder="إجمالي النقاط" ReadOnly="true" Enabled="false"></asp:TextBox>
                                    </div>

                                </div>

                                <div class="col-md-2">

                                    <div class="form-group form-group-sm">
                                        <label for="txt_balance">الرصيد</label>
                                        <asp:TextBox ID="txt_balance" runat="server" CssClass="form-control" placeholder="الرصيد" ReadOnly="true" Enabled="false"></asp:TextBox>
                                    </div>

                                </div>

                                <div class="col-md-2">

                                    <div class="form-group form-group-sm">
                                        <label for="txt_plumber_creator">إسم المستخدم</label>
                                        <asp:TextBox ID="txt_plumber_creator" runat="server" CssClass="form-control" placeholder="إسم المستخدم" ReadOnly="true" Enabled="false"></asp:TextBox>
                                    </div>

                                </div>

                                <div class="col-md-2">

                                    <div class="form-group form-group-sm">
                                        <label for="txt_rec_date">تاريخ التسجيل</label>
                                        <asp:TextBox ID="txt_rec_date" runat="server" CssClass="form-control" placeholder="تاريخ التسجيل" ReadOnly="true" Enabled="false"></asp:TextBox>
                                    </div>

                                    <%--<asp:Button ID="btnSavePlumberData" runat="server" Text="حفظ" CssClass="btn btn-info btn-block" OnClick="btnSavePlumberData_Click" />--%>
                                </div>

                            </div>

                        </div>

                    </div>

                    <%--بيانات امر الشغل--%>
                    <div class="box box-primary ">
                        <div class="box-header with-border">
                            <div class="box-title">
                                بيانات امر الشغل
                            </div>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="col-md-12">

                                <div class="col-md-2">

                                    <div class="form-group form-group-sm">
                                        <label for="txt_inspection_code">كود المعاينة</label>
                                        <asp:TextBox ID="txt_inspection_code" runat="server" CssClass="form-control" placeholder="كود المعاينة" ReadOnly="true" Enabled="false"></asp:TextBox>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="txt_inspection_printed_NO">الرقم المطبوع</label>
                                        <asp:TextBox ID="txt_inspection_printed_NO" runat="server" CssClass="form-control" placeholder="الرقم المطبوع"></asp:TextBox>
                                    </div>
                                    <div class="form-group form-group-sm">
                                        <label for="txt_owner_name">اسم المالك</label>
                                        <asp:TextBox ID="txt_owner_name" runat="server" CssClass="form-control" placeholder="اسم المالك"></asp:TextBox>
                                        <asp:HiddenField ID="hiddenInspectionCode" runat="server" />
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="txt_owner_mobile">موبايل المالك</label>
                                        <asp:TextBox ID="txt_owner_mobile" runat="server" MaxLength="12" CssClass="form-control" placeholder="موبايل المالك"></asp:TextBox>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="txt_owner_tel">تليفون المالك</label>
                                        <asp:TextBox ID="txt_owner_tel" runat="server" MaxLength="12" CssClass="form-control" placeholder="تليفون المالك"></asp:TextBox>
                                    </div>
                                    <div class="form-group form-group-sm">
                                        <label for="ddl_inspection_source">مصدر المعاينة</label>
                                        <asp:DropDownList ID="ddl_inspection_source" runat="server" CssClass="form-control btn-sm">
                                            <asp:ListItem Value="">اختر...</asp:ListItem>
                                            <asp:ListItem Value="تليفوني">تليفوني</asp:ListItem>
                                            <asp:ListItem Value="مندوب">مندوب</asp:ListItem>
                                            <asp:ListItem Value="شخصي">شخصي</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="ddl_inspection_description">وصف المعاينة</label>
                                        <asp:DropDownList ID="ddl_inspection_description" runat="server" CssClass="form-control btn-sm">
                                            <asp:ListItem Value="">إختر...</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                </div>

                                <div class="col-md-2">

                                    <div class="form-group form-group-sm">
                                        <label for="ddl_inspection_gov">محافظة المعاينة</label>
                                        <asp:DropDownList ID="ddl_inspection_gov" runat="server" CssClass="form-control btn-sm" OnSelectedIndexChanged="ddl_inspection_gov_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Value="">إختر...</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="ddl_inspection_city">حي المعاينة</label>
                                        <asp:DropDownList ID="ddl_inspection_city" runat="server" Width="100%" CssClass="form-control btn-sm btn-block" OnSelectedIndexChanged="ddl_inspection_city_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Value="">إختر...</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="ddl_inspection_dist">منطقة المعاينة</label>
                                        <asp:DropDownList ID="ddl_inspection_dist" runat="server" Width="100%" CssClass="form-control btn-sm btn-block">
                                            <asp:ListItem Value="">إختر...</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="txt_inspection_address">عنوان المعاينة</label>
                                        <asp:TextBox ID="txt_inspection_address" runat="server" CssClass="form-control" placeholder="عنوان المعاينة" TextMode="MultiLine" Rows="2"></asp:TextBox>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="ddl_inspection_situation">موقف المعاينة</label>
                                        <asp:DropDownList ID="ddl_inspection_situation" runat="server" CssClass="form-control btn-sm">
                                            <asp:ListItem Value="">اختر...</asp:ListItem>
                                            <asp:ListItem Value="تأسيس">تأسيس</asp:ListItem>
                                            <asp:ListItem Value="تجديد">تجديد</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="ddl_inspection_type">نوع المعاينة</label>
                                        <asp:DropDownList ID="ddl_inspection_type" runat="server" CssClass="form-control btn-sm">
                                            <asp:ListItem Value="">اختر...</asp:ListItem>
                                            <asp:ListItem Value="معاينة">معاينة</asp:ListItem>
                                            <asp:ListItem Value="مشروع">مشروع</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="ddl_inspection_place_type">نوع مكان المعاينة</label>
                                        <asp:DropDownList ID="ddl_inspection_place_type" runat="server" CssClass="form-control btn-sm">
                                            <asp:ListItem Value="">اختر...</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                </div>

                                <div class="col-md-2">

                                    <div class="form-group form-group-sm">
                                        <label for="txt_inspection_planed_date">تاريخ المعاينة</label>
                                        <telerik:RadDatePicker runat="server" ID="txt_inspection_planed_date" Width="100%"></telerik:RadDatePicker>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="txt_inspection_year">سنة العرض</label>
                                        <asp:TextBox ID="txt_inspection_year" runat="server" CssClass="form-control" placeholder="سنة العرض" ReadOnly="true" Enabled="false"></asp:TextBox>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="txt_inspection_planed_from_time">من</label>
                                        <telerik:RadTimePicker runat="server" ID="txt_inspection_planed_from_time" Width="100%"></telerik:RadTimePicker>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="txt_inspection_planed_to_time">إلي</label>
                                        <telerik:RadTimePicker runat="server" ID="txt_inspection_planed_to_time" Width="100%"></telerik:RadTimePicker>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="txt_inspection_qa_resp">مسئول الجودة</label>
                                        <asp:TextBox ID="txt_inspection_qa_resp" runat="server" CssClass="form-control" placeholder="مسئول الجودة"></asp:TextBox>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="txt_inspection_notes">ملاحظات</label>
                                        <asp:TextBox ID="txt_inspection_notes" runat="server" TextMode="MultiLine" Rows="2" CssClass="form-control" placeholder="ملاحظات"></asp:TextBox>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="txt_inspection_data_entry_notes">ملاحظات مدخل البيانات</label>
                                        <asp:TextBox ID="txt_inspection_data_entry_notes" runat="server" TextMode="MultiLine" Rows="2" CssClass="form-control" placeholder="ملاحظات مدخل البيانات"></asp:TextBox>
                                    </div>


                                </div>

                                <div class="col-md-2">

                                    <div class="form-group form-group-sm">
                                        <label for="ddl_shop_gov">محافظة المحل</label>
                                        <asp:DropDownList ID="ddl_shop_gov" runat="server" CssClass="form-control btn-sm" OnSelectedIndexChanged="ddl_shop_gov_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Value="">إختر...</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="ddl_shop_city">حي المحل</label>
                                        <asp:DropDownList ID="ddl_shop_city" runat="server" Width="100%" CssClass="form-control btn-sm btn-block " OnSelectedIndexChanged="ddl_shop_city_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Value="">إختر...</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="ddl_shop_dist">منطقة المحل</label>
                                        <asp:DropDownList ID="ddl_shop_dist" runat="server" Width="100%" CssClass="form-control btn-sm btn-block " OnSelectedIndexChanged="ddl_shop_dist_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Value="">إختر...</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="ddl_buy_shop">محل الشراء</label>
                                        <asp:DropDownList ID="ddl_buy_shop" runat="server" Width="100%" CssClass="form-control btn-sm btn-block">
                                            <asp:ListItem Value="">إختر...</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="txt_inspection_total_points">إجمالي النقاط</label>
                                        <asp:TextBox ID="txt_inspection_total_points" runat="server" CssClass="form-control" placeholder="إجمالي النقاط" ReadOnly="true" Enabled="false"></asp:TextBox>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="txt_inspection_rep_points">النقاط حسب المندوب</label>
                                        <asp:TextBox ID="txt_inspection_rep_points" runat="server" CssClass="form-control" placeholder="النقاط حسب المندوب"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-md-2">

                                    <div class="form-group form-group-sm">
                                        <label for="ddl_inspection_rep">مندوب المعاينة</label>
                                        <asp:DropDownList ID="ddl_inspection_rep" runat="server" CssClass="form-control btn-sm select2" Style="width: 100%">
                                            <asp:ListItem Value="">إختر...</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="ddl_inspection_branch">فرع المعاينة</label>
                                        <asp:DropDownList ID="ddl_inspection_branch" runat="server" CssClass="form-control btn-sm">
                                            <asp:ListItem Value="">إختر...</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="ddl_grantee_status">حالة الضمان</label>
                                        <asp:DropDownList ID="ddl_grantee_status" runat="server" CssClass="form-control btn-sm">
                                            <asp:ListItem Value="">إختر...</asp:ListItem>
                                            <asp:ListItem Value="Y">داخل الضمان</asp:ListItem>
                                            <asp:ListItem Value="N">خارج الضمان</asp:ListItem>
                                            <asp:ListItem Value="C">لم يتم التنفيذ </asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="txt_grantee_status_reason">سبب رفض الضمان</label>
                                        <asp:TextBox ID="txt_grantee_status_reason" runat="server" TextMode="MultiLine" Rows="2" CssClass="form-control" placeholder="سبب الوقف"></asp:TextBox>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="ddl_stop_printing_reoprts">حالة طباعة الشهادات</label>
                                        <asp:DropDownList ID="ddl_stop_printing_reoprts" runat="server" CssClass="form-control btn-sm">
                                            <asp:ListItem Value="">اختر...</asp:ListItem>
                                            <asp:ListItem Value="Y">موقوف</asp:ListItem>
                                            <asp:ListItem Value="N">يعمل</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="txt_stop_printing_reoprts_reason">سبب إيقاف الطباعة</label>
                                        <asp:TextBox ID="txt_stop_printing_reoprts_reason" runat="server" TextMode="MultiLine" Rows="2" CssClass="form-control" placeholder="سبب الوقف"></asp:TextBox>
                                    </div>

                                </div>

                                <div class="col-md-2">

                                    <div class="form-group form-group-sm">
                                        <label for="ddl_inspection_order_status">حالة امر الشغل</label>
                                        <asp:DropDownList ID="ddl_inspection_order_status" runat="server" CssClass="form-control btn-sm">
                                            <asp:ListItem Value="">إختر...</asp:ListItem>
                                            <asp:ListItem Value="تم التنفيذ">تم التنفيذ</asp:ListItem>
                                            <asp:ListItem Value="مرفوضة">مرفوضة</asp:ListItem>
                                            <asp:ListItem Value="تم التأجيل">تم التأجيل</asp:ListItem>
                                            <asp:ListItem Value="ملغي">ملغي</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="txt_inspection_reject_reason">السبب</label>
                                        <asp:TextBox ID="txt_inspection_reject_reason" runat="server" TextMode="MultiLine" Rows="2" CssClass="form-control" placeholder="سبب الوقف"></asp:TextBox>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="ddl_inspection_status">حالة المعاينة</label>
                                        <asp:DropDownList ID="ddl_inspection_status" runat="server" CssClass="form-control btn-sm">
                                            <asp:ListItem Value="">إختر...</asp:ListItem>
                                            <asp:ListItem Value="مفتوحة">مفتوحة</asp:ListItem>
                                            <asp:ListItem Value="مغلقة">مغلقة</asp:ListItem>
                                            <asp:ListItem Value="تم الالغاء">تم الالغاء</asp:ListItem>
                                            <asp:ListItem Value="تم التأجيل">تم التأجيل</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="txt_inspection_status_note">ملاحاظات الحالة</label>
                                        <asp:TextBox ID="txt_inspection_status_note" runat="server" TextMode="MultiLine" Rows="2" CssClass="form-control" placeholder="ملاحاظات الحالة"></asp:TextBox>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="txt_inspection_rec_date">تاريخ التسجيل</label>
                                        <asp:TextBox ID="txt_inspection_rec_date" runat="server" CssClass="form-control" placeholder="تاريخ التسجيل" ReadOnly="true" Enabled="false"></asp:TextBox>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label for="txt_inspection_creator">إسم المستخدم</label>
                                        <asp:TextBox ID="txt_inspection_creator" runat="server" CssClass="form-control" placeholder="إسم المستخدم" ReadOnly="true" Enabled="false"></asp:TextBox>
                                    </div>

                                    <%--<asp:Button ID="btnSaveInspectionData" runat="server" Text="حفظ" CssClass="btn btn-info btn-block" Style="margin-bottom: 10px;" OnClick="btnSaveInspectionData_Click" />--%>
                                </div>

                            </div>

                            
                        </div>
                        <div class="box-footer">
                            <asp:Button ID="btnSaveAll" runat="server" Text="حفظ الكل" CssClass="btn btn-success" OnClick="btnSaveAll_Click" />
                            <asp:Button ID="btnReset" runat="server" Text="إلغاء" CssClass="btn" OnClick="btnReset_Click" CausesValidation="false" />
                        </div>
                    </div>
                    <div class="col-md-12">
                                <div class="nav-tabs-custom">
                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a href="#tab_1" data-toggle="tab">المنتجات
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="#tab_2" data-toggle="tab">الوثائق المطبوعة
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">

                                        <div class="active tab-pane" id="tab_1">

                                            <div class="raw clearfix">
                                                <telerik:RadGrid
                                                    RenderMode="Lightweight"
                                                    runat="server"
                                                    ID="RadGridInspectionProducts"
                                                    AutoGenerateColumns="false" AllowPaging="true"
                                                    OnNeedDataSource="RadGridInspectionProducts_NeedDataSource"
                                                    OnUpdateCommand="RadGridInspectionProducts_UpdateCommand"
                                                    OnDeleteCommand="RadGridInspectionProducts_DeleteCommand"
                                                    OnInsertCommand="RadGridInspectionProducts_InsertCommand">
                                                    <MasterTableView
                                                        DataKeyNames="Code"
                                                        CommandItemDisplay="Top"
                                                        InsertItemPageIndexAction="ShowItemOnCurrentPage">
                                                        <Columns>
                                                            <telerik:GridEditCommandColumn />
                                                            <telerik:GridBoundColumn DataField="CODE" Visible="false" ReadOnly="true"/>
                                                            <telerik:GridBoundColumn DataField="PRODUCT_CODE" HeaderText="كود المنتج" />
                                                            <telerik:GridBoundColumn DataField="ITEM_TYPE" HeaderText="نوع المنتج"  ReadOnly="true" />
                                                            <telerik:GridBoundColumn DataField="PRODUCT_NAME" HeaderText="المنتج" ReadOnly="true" />
                                                            <telerik:GridBoundColumn DataField="QTY" HeaderText="الكمية" />
                                                            <telerik:GridBoundColumn DataField="POINTS" HeaderText="النقاط للوحدة" ReadOnly="true"  />
                                                            <telerik:GridBoundColumn DataField="TOTAL_POINTS" HeaderText="المجموع" ReadOnly="true" />
                                                            <telerik:GridBoundColumn DataField="CARD_NO" HeaderText="رقم الكارت" />
                                                            <telerik:GridBoundColumn DataField="NOTES" HeaderText="ملاحظات" />
                                                            <telerik:GridBoundColumn DataField="QTY_BEF_UPDATE" HeaderText="الكمية قبل التعديل" />
                                                            <telerik:GridBoundColumn DataField="UPDATE_REASON" HeaderText="السبب" />
                                                            <telerik:GridBoundColumn DataField="UPDATE_RESP" HeaderText="المسئول" />
                                                            <telerik:GridBoundColumn DataField="UPDATE_NO" HeaderText="رقم التعديل" />
                                                            <telerik:GridButtonColumn ConfirmText="هل تريد حذف هذا المنتج؟" ConfirmDialogType="RadWindow" ConfirmTitle="حذف منتج" ButtonType="FontIconButton" CommandName="Delete" />
                                                        </Columns>
                                                    </MasterTableView>
                                                    <PagerStyle Mode="NextPrevAndNumeric" />
                                                </telerik:RadGrid>
                                               <%-- <telerik:RadInputManager RenderMode="Lightweight" runat="server" ID="RadInputManager1" Enabled="true">
                                                    <telerik:TextBoxSetting BehaviorID="TextBoxSetting1">
                                                    </telerik:TextBoxSetting>
                                                    <telerik:NumericTextBoxSetting BehaviorID="NumericTextBoxSetting1" Type="Currency"
                                                        AllowRounding="true" DecimalDigits="2">
                                                    </telerik:NumericTextBoxSetting>
                                                    <telerik:NumericTextBoxSetting BehaviorID="NumericTextBoxSetting2" Type="Number"
                                                        AllowRounding="true" DecimalDigits="0" MinValue="0">
                                                    </telerik:NumericTextBoxSetting>
                                                </telerik:RadInputManager>--%>
                                                <telerik:RadWindowManager RenderMode="Lightweight" ID="RadWindowManager1" runat="server" />
                                            </div>

                                        </div>

                                        <div class="tab-pane" id="tab_2">

                                            <div class="raw clearfix">
                                                <telerik:RadGrid
                                                    RenderMode="Lightweight"
                                                    runat="server"
                                                    ID="RadGridInspectionCertificates"
                                                    AutoGenerateColumns="false" AllowPaging="true"
                                                    OnNeedDataSource="RadGridInspectionCertificates_NeedDataSource"
                                                    OnUpdateCommand="RadGridInspectionCertificates_UpdateCommand">
                                                    <MasterTableView
                                                        DataKeyNames="Code">
                                                        <Columns>
                                                            <telerik:GridEditCommandColumn />
                                                            <telerik:GridBoundColumn DataField="PRN_TYPE" HeaderText="نوع الطباعة" ReadOnly="true" ForceExtractValue="Always" ConvertEmptyStringToNull="true" />
                                                            <telerik:GridBoundColumn DataField="PRN_DATE" HeaderText="تاريخ الطباعة" ReadOnly="true" ForceExtractValue="Always" ConvertEmptyStringToNull="true" />
                                                            <telerik:GridBoundColumn DataField="USER_NAME" HeaderText="اسم المستخدم" ReadOnly="true" ForceExtractValue="Always" ConvertEmptyStringToNull="true" />
                                                            <telerik:GridBoundColumn DataField="RECEIVER_NAME" HeaderText="اسم المستلم" />
                                                        </Columns>
                                                    </MasterTableView>
                                                    <PagerStyle Mode="NextPrevAndNumeric" />
                                                </telerik:RadGrid>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                </div>

            </div>

            <div class="box-footer">
                
            </div>

        </div>

    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadSection" runat="Server">
    <style>
        .rltbPager {
            display: none;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="JsScripts" runat="Server">
    <script src="../../Content/js/modules/eg/crm_inspection.js"></script>
</asp:Content>

