﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class Modules_EG_CRM_QUERY_TYPES : System.Web.UI.Page
{
    private readonly EGService _egService;
    DataSet Ds = new DataSet();

    public Modules_EG_CRM_QUERY_TYPES()
    {
        _egService = new EGService();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            Fill_Grid();
            btn_update.Visible = false;
            btn_clear.Visible = false;
            btn_save.Visible = true;
        }
    }
    private void Clear()
    {
        txt_query.Text = "";
        ddl_query_class.SelectedIndex = 0;
        ddl_active.SelectedIndex = 0;
        ddl_dept.SelectedIndex = 0;
        ddl_query_class.SelectedIndex = 0;
        ddl_type.SelectedIndex = 0;
        chk_send_mail.Checked = false;

        btn_update.Visible = false;
        btn_clear.Visible = false;
        btn_save.Visible = true;

    }
    protected void btn_clear_Click(object sender, EventArgs e)
    {
        Clear();
 
    }
    protected void btn_save_Click(object sender, EventArgs e)
    {
        if (txt_query.Text == "" || ddl_query_class.SelectedIndex == 0 || ddl_type.SelectedIndex == 0 )
        {
            RadNotification1.Text = "برجاء إدخال البيانات";
            RadNotification1.Title = "برنامج خدمة العملاء";
            RadNotification1.TitleIcon = "info";
            RadNotification1.ContentIcon = "info";
            RadNotification1.Show();
        }
        else
        {
            int Result = _egService.Save_Query_Types(txt_query.Text, ddl_active.SelectedValue.ToString(), ddl_type.SelectedValue.ToString(), ddl_dept.SelectedValue.ToString(), Ret_Send_mail() , ddl_query_class.SelectedValue.ToString());
            if (Result > 0)
            {
                Fill_Grid();
                RadNotification1.Text = "تم حفظ البيانات بنجاح";
                RadNotification1.Title = "برنامج خدمة العملاء";
                RadNotification1.TitleIcon = "info";
                RadNotification1.ContentIcon = "info";
                RadNotification1.Show();
                Clear();
               
            }
            else
            {
                RadNotification1.Text = "برجاء التحقق من البيانات المدخله";
                RadNotification1.Title = "برنامج خدمة العملاء";
                RadNotification1.TitleIcon = "info";
                RadNotification1.ContentIcon = "info";
                RadNotification1.Show();
                
            }
        }
    }

    private string Ret_Send_mail()
    {
        string ret = "";
        if (chk_send_mail.Checked == true)
        { ret = "Y"; }
        else
        { ret = "N"; }
        return ret;
    }

    private void Set_Send_mail(string val)
    {
        if (val == "Y")
        { chk_send_mail.Checked = true; }
        else
        { chk_send_mail.Checked = false; }
    }

    private void Fill_Grid()
    {
        try
        {
           
            Ds.Tables.Clear();
            Ds = _egService.Bind_Query_Grid();
            RadGrid1.DataSource = Ds;
            RadGrid1.DataBind();
        }
        catch
        {

        }
    }

    protected void btn_query_Click(object sender, EventArgs e)
    {
        // Update_Query_Types
        if (txt_query.Text == "" || ddl_query_class.SelectedIndex == 0 || ddl_type.SelectedIndex == 0)
        {
            RadNotification1.Text = "برجاء اختيار البيانات";
            RadNotification1.Title = "برنامج خدمة العملاء";
            RadNotification1.TitleIcon = "info";
            RadNotification1.ContentIcon = "info";
            RadNotification1.Show();
        }
        else
        {
            int Result = _egService.Update_Query_Types(Session["MYID"].ToString(), txt_query.Text, ddl_active.SelectedValue.ToString(), ddl_type.SelectedValue.ToString(), ddl_dept.SelectedValue.ToString(), Ret_Send_mail(), ddl_query_class.SelectedValue.ToString());
            if (Result > 0)
            {
                Fill_Grid();
                RadNotification1.Text = "تم تعديل البيانات بنجاح";
                RadNotification1.Title = "برنامج خدمة العملاء";
                RadNotification1.TitleIcon = "info";
                RadNotification1.ContentIcon = "info";
                RadNotification1.Show();
                Clear();
               
            }
            else
            {
                RadNotification1.Text = "برجاء التحقق من البيانات المدخله";
                RadNotification1.Title = "برنامج خدمة العملاء";
                RadNotification1.TitleIcon = "info";
                RadNotification1.ContentIcon = "info";
                RadNotification1.Show();

            }
        }
    }

    protected void RadGrid1_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "UPDATE")
        {
            // For Normal mode
          //  Session["MODE"] = "EDIT";
           // btn_save.Text = "Update";
            try
            {
               
                GridDataItem item = e.Item as GridDataItem;
                Label Namelbl = item.FindControl("IDLabel") as Label;
                Label Activelbl = item.FindControl("ACTIVELabel") as Label; 
                 Label qry_typeLabel = item.FindControl("qry_typeLabel") as Label;
                Label serv_deptLabel = item.FindControl("serv_deptLabel") as Label;
                Label send_mailLabel = item.FindControl("send_mailLabel") as Label;
                Label comp_classLabel = item.FindControl("comp_classLabel") as Label;

                Session["MYID"] = Namelbl.Text;
                txt_query.Text = Namelbl.Text;
                txt_query.Focus();
                ddl_active.ClearSelection();
                if (Activelbl.Text == "" || Activelbl.Text == string.Empty)
                { ddl_active.SelectedIndex = 0; }
                else
                { ddl_active.Items.FindByValue(Activelbl.Text).Selected = true;  }

                ddl_query_class.ClearSelection();
                if (qry_typeLabel.Text == "" || qry_typeLabel.Text == string.Empty)
                { ddl_query_class.SelectedIndex = 0; }
                else
                { ddl_query_class.Items.FindByValue(qry_typeLabel.Text).Selected = true; }

                ddl_dept.ClearSelection();
                if (serv_deptLabel.Text == "" || serv_deptLabel.Text == string.Empty)
                { ddl_dept.SelectedIndex = 0; }
                else
                { ddl_dept.Items.FindByValue(serv_deptLabel.Text).Selected = true; }

                ddl_type.ClearSelection();
                if (comp_classLabel.Text == "" || comp_classLabel.Text == string.Empty)
                { ddl_type.SelectedIndex = 0; }
                else
                { ddl_type.Items.FindByValue(comp_classLabel.Text).Selected = true; }

              
                Set_Send_mail(send_mailLabel.Text);
                btn_update.Visible = true;
                btn_save.Visible = false;
                btn_clear.Visible = true;

            }
            catch
            {

            }

        }
        else if (e.CommandName == "DELETE")
        {
            GridDataItem item = e.Item as GridDataItem;
            Label IDlbl = item.FindControl("IDLabel") as Label;
            Label ACTIVELabel = item.FindControl("ACTIVELabel") as Label;
            
                int RESLT = _egService.Delete_Query_Types(IDlbl.Text, ACTIVELabel.Text);
                if (RESLT == 1)
                {
                    Fill_Grid();

                RadNotification1.Text = "تم حذف البيانات بنجاح";
                RadNotification1.Title = "برنامج خدمة العملاء";
                RadNotification1.TitleIcon = "info";
                RadNotification1.ContentIcon = "info";
                RadNotification1.Show();
                Clear();
               // txt_query.Focus();
            }
                else
                {
                    RadNotification1.Text = "يوجد مشكله في عملية الحذف";
                RadNotification1.Title = "برنامج خدمة العملاء";
                RadNotification1.TitleIcon = "info";
                    RadNotification1.ContentIcon = "info";
                    RadNotification1.Show();
                }
          
        }
    }

    protected void RadGrid1_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {

    }

    protected void RadGrid1_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        Fill_Grid();
    }

    protected void RadGrid1_ItemCreated(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {

    }

    protected void RadGrid1_UpdateCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {

    }

    protected void RadGrid1_DeleteCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {

    }

    protected void ImportedFilter_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
    {
        DataRowView rowView = (DataRowView)e.Item.DataItem;
        if (rowView["active_desc"] is DBNull)
        {
            e.Item.Text = "نشط";
            e.Item.Value = "نشط";
        }
        else if ((bool)rowView["active_desc"])
        {
            e.Item.Text = "غير نشط";
            e.Item.Value = "غير نشط";
        }
    }

    protected void RadGrid1_PreRender(object sender, System.EventArgs e)
    {
        if (RadGrid1.MasterTableView.FilterExpression != string.Empty)
        {
            RefreshCombos();
        }
    }

    protected void RefreshCombos()
    {
        RadGrid1.MasterTableView.Rebind();
    }
}