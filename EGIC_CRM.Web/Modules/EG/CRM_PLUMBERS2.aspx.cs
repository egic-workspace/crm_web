﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Modules_EG_CRM_PLUMBERS2 : BasePage
{

    #region fields

    private readonly LookupService _lookupService;
    private readonly EGService _egService;

    #endregion

    #region CTOR

    public Modules_EG_CRM_PLUMBERS2()
    {
        _lookupService = new LookupService();
        _egService = new EGService();
    }

    #endregion

    #region Properties

    public string CODE { get { return hidden_code.Value; } }
    public string PlumberName { get { return txt_plumber_name.Text; } }
    public string PlumberMobile { get { return txt_plumber_mobile.Text; } }
    public string PlumberID { get { return txt_plumber_ID.Text; } }
    public string PlumberAddress { get { return txt_home_address.Text; } }
    public string PlumberPhone { get { return txt_plumber_phone.Text; } }

    public string PlumberNickname { get { return txt_plumber_nickname.Text; } }
    public string PlumberPhoneII { get { return txt_plumber_phone_2.Text; } }
    public string PlumberPlace { get { return txt_plumber_place.Text; } }
    public string PlumberBirthdate { get { return txt_plumber_birthdate.Text; } }

    public string PlumberDeactivationResion { get { return txt_deactivation_resion.Text; } }
    public string PlumberSalesRep { get { return txt_sales_rep.Text; } }

    public string PlumberBranch { get { return ddl_branch.SelectedValue; } }
    public string PlumberShopCode { get { return ddl_work_shop.SelectedValue; } }
    public string PlumberWorkGovCode { get { return ddl_work_gov.SelectedValue; } }
    public string PlumberWorkCityCode { get { return ddl_work_city.SelectedValue; } }
    public string PlumberWorkDistCode { get { return ddl_work_dist.SelectedValue; } }
    public string PlumberReligion { get { return ddl_religion.SelectedValue; } }
    public string PlumberIsActive { get { return ddl_isActive.SelectedValue; } }
    public string PlumberGroupManagerCode { get { return ddl_group_manager.SelectedValue; } }
    public string PlumberGroupCode { get { return ddl_group_name.SelectedValue; } }
    public string PlumberInspectionRepCode { get { return ddl_inspection_rep.SelectedValue; } }

    #endregion

    #region Utilities

    private void ShowErrorMessages(List<string> errors)
    {
        if (errors.Count > 0)
        {
            lblErrorMessage.Visible = true;
            lblErrorMessage.InnerText = string.Join(Environment.NewLine, errors);
            return;
        }
    }

    private void ShowErrorMessages(string error)
    {
        if (!string.IsNullOrWhiteSpace(error))
        {
            lblErrorMessage.Visible = true;
            lblErrorMessage.InnerText = string.Join(Environment.NewLine, error);
            return;
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            // fill ddl_branch
            var dataBarnches = _lookupService.GetAllBranches();
            ddl_branch.FillDataSource(dataBarnches);

            // fill ddl_work_gov
            var dataGovernorates = _lookupService.GetAllGovernorates();
            ddl_work_gov.FillDataSource(dataGovernorates);

            // fill ddl_work_gov
            var dataShops = _lookupService.GetAllShops();
            ddl_work_shop.FillDataSource(dataShops);

            // fill ddl_group_name
            var dataGroups = _lookupService.GetAllGroups();
            ddl_group_name.FillDataSource(dataGroups);
        }
    }

    protected void btnUploadImg_Click(object sender, EventArgs e)
    {
        //if (filePlumberID.HasFile)
        //{
        //    try
        //    {
        //        if (filePlumberID.PostedFile.ContentType == "image/jpeg")
        //        {
        //            if (filePlumberID.PostedFile.ContentLength > 502400)
        //            {
        //                string file_name = filePlumberID.FileName.ToString();
        //                string path = filePlumberID.PostedFile.FileName.ToString();
        //                //*********************
        //                byte[] bytearr = new byte[filePlumberID.PostedFile.InputStream.Length + 1];
        //                filePlumberID.PostedFile.InputStream.Read(bytearr, 0, bytearr.Length);
        //                //************************

        //                com = new SqlCommand(" Insert Into  SendingProblems  ( Emp_Code, ProbType_Code, PCProgName, Problem_Desc, Attached_file , Record_Date, Status ,File_Path ,File_Name ,Start_date ,  Ip_Address, Computer_Name, Loged_User, Host_Name ) Values ( '" + MYIDD + "','" + typeID + "','" + txtprogname.Text + "',@DESC,@file1,Getdate(),'00','" + path + "','" + file_name + "',Getdate(),'" + Session["PC_IP"].ToString() + "','" + Session["PC_Name"].ToString() + "','" + Session["LOGED_User"].ToString() + "','" + Session["Host_Name"].ToString() + "' )  ", con);
        //                com.Parameters.AddWithValue("@file1", bytearr);
        //            }
        //            else
        //                lblErrorMessage.InnerText = "الصورة المرفوعه اكبر من 500 كيلو بايت";
        //        }
        //        else
        //            lblErrorMessage.InnerText = "Upload status: Only JPEG files are accepted!";
        //    }
        //    catch (Exception ex)
        //    {
        //        lblErrorMessage.InnerText = "حدث خطأ اثناء الرفع: " + ex.Message;
        //    }
        //}
    }

    protected void btn_save_Click(object sender, EventArgs e)
    {
        List<string> errorMessages = new List<string>();
        if (string.IsNullOrWhiteSpace(PlumberName))
            errorMessages.Add($"يجب ادخال اسم السباك");
        if (string.IsNullOrWhiteSpace(PlumberMobile))
            errorMessages.Add($"يجب ادخال موبايل السباك");
        if (string.IsNullOrWhiteSpace(PlumberID))
            errorMessages.Add($"يجب ادخال رقم بطاقة السباك");
        if (string.IsNullOrWhiteSpace(PlumberAddress))
            errorMessages.Add($"يجب ادخال عنوان منزل السباك");
        if (!string.IsNullOrWhiteSpace(PlumberIsActive) && PlumberIsActive.Equals("موقوف") && string.IsNullOrWhiteSpace(PlumberDeactivationResion))
            errorMessages.Add($"يجب ادخال سبب وقف السباك");
        ShowErrorMessages(errorMessages);

        bool success =
            _egService.SavePlumberData(out string errorMessage,
            code: CODE,
            name: PlumberName,
            mobile: PlumberMobile,
            id_no: PlumberID,
            home_address: PlumberAddress,
            work_address: PlumberPlace,
            tel: PlumberPhone,
            tel2: PlumberPhoneII,
            gov_code: PlumberWorkGovCode,
            city_code: PlumberWorkCityCode,
            dist_code: PlumberWorkDistCode,
            shop_code: PlumberShopCode,
            branch: PlumberBranch,
            birth_date: PlumberBirthdate,
            religion: PlumberReligion,
            status: PlumberIsActive,
            reason: PlumberDeactivationResion,
            group_code: PlumberGroupCode,
            rep_no: PlumberInspectionRepCode,
            mang_no: PlumberGroupManagerCode,
            nickname: PlumberNickname,

            birth_gov: string.Empty,
            birth_city: string.Empty,
            birth_lov: string.Empty,
            feast_gov: string.Empty,
            gender:string.Empty,
            nationality:string.Empty,
            photo: null,

            currentUsername: CurrentUsername
            );
        if (!success)
            ShowErrorMessages($"فشل في الحفظ: {Environment.NewLine}({errorMessage})");
        else
            lblSuccessMessage.Visible = true;
    }

    protected void ddl_work_gov_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (int.TryParse(ddl_work_gov.SelectedValue, out int governorateCode) && governorateCode > 0)
        {
            var items = _lookupService.GetAllCities(governorateCode);
            ddl_work_city.FillDataSource(items);
        }
    }

    protected void ddl_work_city_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (int.TryParse(ddl_work_city.SelectedValue, out int cityCode) && cityCode > 0)
        {
            var items = _lookupService.GetAllDistricts(cityCode);
            ddl_work_dist.FillDataSource(items);
        }
    }

    protected void ddl_group_name_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (int.TryParse(ddl_group_name.SelectedValue, out int groupCode) && groupCode > 0)
        {
            var items = _lookupService.GetAllGroupsManagers(groupCode);
            ddl_group_manager.FillDataSource(items);
            txt_sales_rep.Text = _lookupService.GetSalesRepName(groupCode);
        }
    }

    protected void ddl_group_manager_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (int.TryParse(ddl_group_manager.SelectedValue, out int managerCode) && managerCode > 0)
        {
            var items = _lookupService.GetAllInspectionReps(managerCode);
            ddl_inspection_rep.FillDataSource(items);
        }
    }
}