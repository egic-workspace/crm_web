﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.Data.Extensions;
using Telerik.Web.UI;

public partial class Modules_EG_CRM_INSPECTION : BasePage
{
    #region fields

    private readonly LookupService _lookupService;
    private readonly EGService _egService;

    #endregion

    #region CTOR

    public Modules_EG_CRM_INSPECTION()
    {
        _lookupService = new LookupService();
        _egService = new EGService();
    }

    #endregion

    #region Utilities

    private void ShowErrorMessages(List<string> errors)
    {
        if (errors.Count > 0)
        {
            lblErrorMessage.Visible = true;
            lblErrorMessage.InnerText = string.Join(Environment.NewLine, errors);
            return;
        }
    }

    private void ShowErrorMessages(string error)
    {
        if (!string.IsNullOrWhiteSpace(error))
        {
            lblErrorMessage.Visible = true;
            lblErrorMessage.InnerText = string.Join(Environment.NewLine, error);
            return;
        }
    }

    #endregion

    #region private

    private void SetupInputManager(GridEditableItem editableItem)
    {
        //var textBox =
        //    ((GridTextBoxColumnEditor)editableItem.EditManager.GetColumnEditor("PRODUCT_NAME")).TextBoxControl;


        //InputSetting inputSetting = RadInputManager1.GetSettingByBehaviorID("TextBoxSetting1");
        //inputSetting.TargetControls.Add(new TargetInput(textBox.UniqueID, true));
        //inputSetting.InitializeOnClient = true;
        //inputSetting.Validation.IsRequired = true;

        //// style UnitPrice column's textbox 
        //textBox =
        //    ((GridTextBoxColumnEditor)editableItem.EditManager.GetColumnEditor("QTY")).TextBoxControl;

        //inputSetting = RadInputManager1.GetSettingByBehaviorID("NumericTextBoxSetting1");
        //inputSetting.InitializeOnClient = true;
        //inputSetting.TargetControls.Add(new TargetInput(textBox.UniqueID, true));

        //// style UnitsInStock column's textbox 
        //textBox =
        //    ((GridTextBoxColumnEditor)editableItem.EditManager.GetColumnEditor("QTY_BEF_UPDATE")).TextBoxControl;

        //inputSetting = RadInputManager1.GetSettingByBehaviorID("NumericTextBoxSetting2");
        //(inputSetting as NumericTextBoxSetting).MaxValue = short.MaxValue;
        //inputSetting.InitializeOnClient = true;
        //inputSetting.TargetControls.Add(new TargetInput(textBox.UniqueID, true));
    }

    private void ResetPlumberForm()
    {
        
        //hidden_code.Value =
        //    txt_plumber_name.Text =
        //    txt_plumber_mobile.Text =
        //    txt_plumber_ID.Text =
        //    txt_home_address.Text =
        //    txt_plumber_place.Text =
        //    txt_plumber_nickname.Text =
        //    txt_plumber_phone.Text =
        //    txt_plumber_phone_2.Text =
        //    txt_sales_rep.Text =
        //    txt_deactivation_reason.Text =
        //    txt_balance.Text =
        //    txt_total_points.Text = string.Empty;

        //txt_plumber_creator.Text = CurrentUsername;
        //txt_rec_date.Text = DateTime.Now.ToShortDateString();
        //txt_plumber_birthdate.SelectedDate = null;
        //txt_plumber_nationality.Text = "مصري";

        //ddl_gender.SetSelectedValue("ذكر");
        //ddl_branch.SetSelectedValue(string.Empty);
        //ddl_religion.SetSelectedValue(string.Empty);
        //ddl_isActive.SetSelectedValue(string.Empty);
        //ddl_work_gov.SetSelectedValue(string.Empty);
        //ddl_birth_gov.SetSelectedValue(string.Empty);
        //ddl_group_name.SetSelectedValue(string.Empty);

        //ddl_group_manager.FillDataSource(null);
        //ddl_rep.FillDataSource(null);
        //ddl_birth_city.FillDataSource(null);
        //ddl_birth_dist.FillDataSource(null);
        //ddl_work_city.FillDataSource(null);
        //ddl_work_dist.FillDataSource(null);
        //ddl_work_shop.FillDataSource(null);

    }

    private void ResetInspectionForm()
    {
        hiddenInspectionCode.Value =
                    txt_inspection_address.Text =
                    txt_inspection_code.Text =
                    txt_inspection_data_entry_notes.Text =
                    txt_inspection_notes.Text =
                    txt_stop_printing_reoprts_reason.Text =
                    txt_inspection_printed_NO.Text =
                    txt_inspection_qa_resp.Text =
                    txt_inspection_reject_reason.Text =
                    txt_inspection_status_note.Text =
                    txt_inspection_year.Text =
                    txt_owner_mobile.Text =
                    txt_owner_tel.Text =
                    txt_owner_name.Text = string.Empty;

        txt_inspection_creator.Text = CurrentUsername;
        txt_inspection_rec_date.Text = DateTime.Now.ToShortDateString();
        txt_inspection_planed_to_time.SelectedDate =
                    txt_inspection_planed_from_time.SelectedDate =
                    txt_inspection_planed_date.SelectedDate = null;

        ddl_inspection_gov.SetSelectedValue(string.Empty);
        ddl_shop_gov.SetSelectedValue(string.Empty);
        ddl_stop_printing_reoprts.SetSelectedValue(string.Empty);
        ddl_grantee_status.SetSelectedValue(string.Empty);
        ddl_inspection_branch.SetSelectedValue(string.Empty);
        ddl_inspection_description.SetSelectedValue(string.Empty);
        ddl_inspection_order_status.SetSelectedValue(string.Empty);
        ddl_inspection_place_type.SetSelectedValue(string.Empty);
        ddl_inspection_rep.SetSelectedValue(string.Empty);
        ddl_inspection_situation.SetSelectedValue(string.Empty);
        ddl_inspection_source.SetSelectedValue(string.Empty);
        ddl_inspection_status.SetSelectedValue(string.Empty);
        ddl_inspection_type.SetSelectedValue(string.Empty);
        ddl_inspection_rep.SetSelectedValue(string.Empty);

        ddl_inspection_city.FillDataSource(null);
        ddl_inspection_dist.FillDataSource(null);
        ddl_shop_city.FillDataSource(null);
        ddl_shop_dist.FillDataSource(null);
        ddl_buy_shop.FillDataSource(null);

        RadGridInspectionProducts.DataSource = null;
        RadGridInspectionProducts.DataBind();
        RadGridInspectionCertificates.DataSource = null;
        RadGridInspectionCertificates.DataBind();
    }

    private void LoadInspectionData(int inspectionCode = 0, string ownerMobile = null)
    {
        DataTable inspectionData = null;
        if (inspectionCode > 0)
            inspectionData = _egService.GetAllInspectionData(inspectionCode);//3933936
        else if (!string.IsNullOrWhiteSpace(ownerMobile))
            inspectionData = _egService.GetAllInspectionData(owner_mob: ownerMobile);
        if (inspectionData != null && inspectionData.Rows.Count > 0)
        {
            var inspection = inspectionData.Rows[0];

            if (inspection["inspection_code"] != null)
            {

                hiddenInspectionCode.Value = inspection["inspection_code"].ToString();

                txt_inspection_code.Text = inspection["inspection_code"].ToString();
                txt_inspection_printed_NO.Text = inspection["inspection_printed_NO"].ToString();
                txt_inspection_address.Text = inspection["inspection_address"].ToString();
                txt_inspection_reject_reason.Text = inspection["inspection_reject_reason"].ToString();
                txt_inspection_status_note.Text = inspection["inspection_status_note"].ToString();
                txt_owner_name.Text = inspection["owner_name"].ToString();
                txt_owner_mobile.Text = inspection["owner_mobile"].ToString();
                txt_owner_tel.Text = inspection["owner_tel"].ToString();
                txt_inspection_qa_resp.Text = inspection["inspection_qa_resp_name"].ToString();
                txt_inspection_creator.Text = inspection["inspection_creator"].ToString();
                txt_inspection_rec_date.Text = inspection["inspection_rec_date"].ToString();
                txt_inspection_year.Text = inspection["inspection_year"].ToString();
                txt_stop_printing_reoprts_reason.Text = inspection["stop_printing_reoprts_reason"].ToString();
                txt_grantee_status_reason.Text = inspection["grantee_status_reason"].ToString();
                txt_inspection_notes.Text = inspection["feedback"].ToString();
                txt_inspection_data_entry_notes.Text = inspection["data_entry_notes"].ToString();
                txt_inspection_rep_points.Text = inspection["rep_points"].ToString();

                ddl_inspection_description.SetSelectedValue(inspection["inspection_description"]);
                ddl_inspection_branch.SetSelectedValue(inspection["inspection_branch"]);
                ddl_grantee_status.SetSelectedValue(inspection["gur_status"]);
                ddl_inspection_status.SetSelectedValue(inspection["view_status"]);
                ddl_inspection_order_status.SetSelectedValue(inspection["order_status"]);
                ddl_inspection_type.SetSelectedValue(inspection["view_type"]);
                ddl_inspection_place_type.SetSelectedValue(inspection["place_type"]);
                ddl_inspection_source.SetSelectedValue(inspection["inspection_source"]);
                ddl_inspection_situation.SetSelectedValue(inspection["inspection_situation"]);
                ddl_stop_printing_reoprts.SetSelectedValue(inspection["stop_printing_reoprts"]);

                var allReps = _lookupService.GetAllInspectionReps();
                ddl_inspection_rep.FillDataSource(allReps);
                ddl_inspection_rep.SetSelectedValue(inspection["inspection_rep_code"]);

                string shop_gov_code = inspection["shop_gov_code"].ToString();
                if (!string.IsNullOrWhiteSpace(shop_gov_code) && int.TryParse(shop_gov_code, out int _shop_gov_code) && _shop_gov_code > 0)
                {
                    ddl_shop_gov.SelectedValue = shop_gov_code;
                    var cities = _lookupService.GetAllCities(_shop_gov_code);
                    ddl_shop_city.FillDataSource(cities);

                    string shop_city_code = inspection["shop_city_code"].ToString();
                    if (!string.IsNullOrWhiteSpace(shop_city_code) && int.TryParse(shop_city_code, out int _shop_city_code) && _shop_city_code > 0)
                    {
                        ddl_shop_city.SelectedValue = shop_city_code;
                        var districts = _lookupService.GetAllDistricts(_shop_city_code);
                        ddl_shop_dist.FillDataSource(districts);


                        string shop_dist_code = inspection["shop_dist_code"].ToString();
                        if (!string.IsNullOrWhiteSpace(shop_dist_code) && int.TryParse(shop_dist_code, out int _shop_dist_code) && _shop_dist_code > 0)
                        {
                            ddl_shop_dist.SelectedValue = shop_dist_code;
                            var shops = _lookupService.GetAllShops(districtCode: _shop_dist_code);
                            ddl_buy_shop.FillDataSource(shops);
                            ddl_buy_shop.SelectedValue = inspection["buy_shop_code"].ToString();
                        }
                    }
                }

                string inspection_gov_code = inspection["inspection_gov_code"].ToString();
                if (!string.IsNullOrWhiteSpace(inspection_gov_code) && int.TryParse(inspection_gov_code, out int _inspection_gov_code) && _inspection_gov_code > 0)
                {
                    ddl_inspection_gov.SelectedValue = inspection_gov_code;
                    var cities = _lookupService.GetAllCities(_inspection_gov_code);
                    ddl_inspection_city.FillDataSource(cities);

                    string inspection_city_code = inspection["inspection_city_code"].ToString();
                    if (!string.IsNullOrWhiteSpace(inspection_city_code) && int.TryParse(inspection_city_code, out int _inspection_city_code) && _inspection_city_code > 0)
                    {
                        ddl_inspection_city.SelectedValue = inspection_city_code;
                        var districts = _lookupService.GetAllDistricts(_inspection_city_code);
                        ddl_inspection_dist.FillDataSource(districts);
                        ddl_inspection_dist.SetSelectedValue(inspection["inspection_dist_code"]);
                    }
                }

                if (DateTime.TryParse(inspection["inspection_planed_date"].ToString(), out DateTime _planedDate))
                    txt_inspection_planed_date.SelectedDate = _planedDate;

                if (inspection["FROM_TIME"] != null && DateTime.TryParse(inspection["FROM_TIME"].ToString(), out DateTime _planedTimeFrom))
                    txt_inspection_planed_from_time.SelectedTime = _planedTimeFrom.TimeOfDay;
                if (inspection["TO_TIME"] != null && DateTime.TryParse(inspection["TO_TIME"].ToString(), out DateTime _planedTimeTo))
                    txt_inspection_planed_to_time.SelectedTime = _planedTimeTo.TimeOfDay;

                RadGridInspectionProducts.DataSource = _egService.GetAllInspectionProducts(inspectionCode);
                RadGridInspectionProducts.DataBind();

                RadGridInspectionCertificates.DataSource = _egService.GetAllInspectionPrints(inspectionCode);
                RadGridInspectionCertificates.DataBind();
            }

            LoadPlumberData(inspection);
        }
    }

    private void LoadPlumberData(DataRow plumberRow)
    {

        //plumber
        string birthDate = plumberRow["birth_date"].ToString();
        if (!string.IsNullOrWhiteSpace(birthDate) && DateTime.TryParse(birthDate, out DateTime _birthDate))
            txt_plumber_birthdate.SelectedDate = _birthDate;

        hidden_code.Value = plumberRow["cust_code"].ToString();
        txt_plumber_name.Text = plumberRow["name"].ToString();
        txt_plumber_mobile.Text = plumberRow["mobile"].ToString();
        txt_plumber_ID.Text = plumberRow["id_no"].ToString();
        txt_home_address.Text = plumberRow["home_address"].ToString();
        txt_plumber_place.Text = plumberRow["work_address"].ToString();
        txt_plumber_nickname.Text = plumberRow["title"].ToString();
        txt_plumber_phone.Text = plumberRow["tel"].ToString();
        txt_plumber_phone_2.Text = plumberRow["tel2"].ToString();
        txt_plumber_creator.Text = plumberRow["plumber_creator"].ToString();
        txt_rec_date.Text = plumberRow["plumber_rec_date"].ToString();
        txt_sales_rep.Text = plumberRow["sales_rep_name"].ToString();
        txt_deactivation_reason.Text = plumberRow["reason"].ToString();
        txt_balance.Text = plumberRow["balance"].ToString();
        txt_total_points.Text = plumberRow["total_points"].ToString();
        txt_plumber_nationality.Text = plumberRow["nationality"].ToString();

        ddl_branch.SetSelectedValue(plumberRow["BRANCH"]);
        ddl_religion.SetSelectedValue(plumberRow["religion"]);
        ddl_isActive.SetSelectedValue(plumberRow["status"]);
        ddl_gender.SetSelectedValue(plumberRow["gender"]);

        if (int.TryParse(plumberRow["GROUP_CODE"].ToString(), out int groupCode) && groupCode > 0)
        {
            ddl_group_name.SetSelectedValue(groupCode);
            var dataGroupManagers = _lookupService.GetAllGroupsManagers(groupCode);
            ddl_group_manager.FillDataSource(dataGroupManagers);

            if (int.TryParse(plumberRow["MANG_NO"].ToString(), out int managerCode) && managerCode > 0)
            {
                ddl_group_manager.SetSelectedValue(managerCode);
                var dataReps = _lookupService.GetAllInspectionReps(managerCode);
                ddl_rep.FillDataSource(dataReps);
                ddl_rep.SetSelectedValue(plumberRow["REP_NO"]);
            }
        }

        if (int.TryParse(plumberRow["GOV_CODE"].ToString(), out int govCode) && govCode > 0)
        {
            ddl_work_gov.SetSelectedValue(govCode);
            var dataCities = _lookupService.GetAllCities(govCode);
            ddl_work_city.FillDataSource(dataCities);
            if (int.TryParse(plumberRow["CITY_CODE"].ToString(), out int cityCode))
            {
                ddl_work_city.SetSelectedValue(cityCode);
                var dataDistricts = _lookupService.GetAllDistricts(cityCode);
                ddl_work_dist.FillDataSource(dataDistricts);
                int.TryParse(plumberRow["DIST_CODE"].ToString(), out int _distCode);
                ddl_work_dist.SetSelectedValue(_distCode);
                if (int.TryParse(plumberRow["SHOP_CODE"].ToString(), out int _shopCode) && _shopCode > 0)
                {
                    var dataShops = _lookupService.GetAllShops(shopCode: _shopCode, governorateCode: govCode, cityCode: cityCode, districtCode: _distCode);
                    ddl_work_shop.FillDataSource(dataShops);
                    ddl_work_shop.SetSelectedValue(_shopCode);
                }
            }
        }

        string birth_gov_code = plumberRow["birth_gov_code"].ToString();
        if (!string.IsNullOrWhiteSpace(birth_gov_code) && int.TryParse(birth_gov_code, out int _birth_gov_code) && _birth_gov_code > 0)
        {
            ddl_birth_gov.SelectedValue = birth_gov_code;
            var cities = _lookupService.GetAllCities(_birth_gov_code);
            ddl_birth_city.FillDataSource(cities);

            string birth_city_code = plumberRow["birth_city_code"].ToString();
            if (!string.IsNullOrWhiteSpace(birth_city_code) && int.TryParse(birth_city_code, out int _birth_city_code) && _birth_city_code > 0)
            {
                ddl_birth_city.SelectedValue = birth_city_code;
                var districts = _lookupService.GetAllDistricts(_birth_city_code);
                ddl_birth_dist.FillDataSource(districts);
                ddl_birth_dist.SetSelectedValue(plumberRow["birth_dist_code"].ToString());
            }
        }
    }

    private void SavePlumberData()
    {
        string birthDate = txt_plumber_birthdate.SelectedDate.HasValue ? txt_plumber_birthdate.SelectedDate.Value.ToShortDateString() : "";
        bool success =
            _egService.SavePlumberData(out string errorMessage,
            code: hidden_code.Value,
            name: txt_plumber_name.Text,
            mobile: txt_plumber_mobile.Text,
            id_no: txt_plumber_ID.Text,
            home_address: txt_home_address.Text,
            work_address: txt_plumber_place.Text,
            tel: txt_plumber_phone.Text,
            tel2: txt_plumber_phone_2.Text,
            gov_code: ddl_work_gov.SelectedValue,
            city_code: ddl_work_city.SelectedValue,
            dist_code: ddl_work_dist.SelectedValue,
            shop_code: ddl_work_shop.SelectedValue,
            branch: ddl_branch.SelectedValue,
            birth_date: birthDate,
            religion: ddl_religion.SelectedValue,
            status: ddl_isActive.SelectedValue,
            reason: txt_deactivation_reason.Text,
            group_code: ddl_group_name.SelectedValue,
            rep_no: ddl_rep.SelectedValue,
            mang_no: ddl_group_manager.SelectedValue,
            nickname: txt_plumber_nickname.Text,
            birth_gov: ddl_birth_gov.SelectedValue,
            birth_city: ddl_birth_city.SelectedValue,
            birth_lov: ddl_birth_dist.SelectedValue,
            feast_gov: string.Empty,
            gender: ddl_gender.SelectedValue,
            nationality: txt_plumber_nationality.Text,
            photo: null,
            currentUsername: CurrentUsername
            );
        if (!success)
            ShowErrorMessages($"فشل في الحفظ: {Environment.NewLine}({errorMessage})");
        else
        {
            lblSuccessMessage.InnerText = "تم الحفظ بنجاح";
            lblSuccessMessage.Visible = true;
        }
    }

    private void SaveInspectionData()
    {
        string planedDate = txt_inspection_planed_date.SelectedDate.HasValue ? txt_inspection_planed_date.SelectedDate.Value.ToShortDateString() : "";

        bool success =
            _egService.SaveInspectionData(out string errorMessage,
            code: hiddenInspectionCode.Value,
            owner_name: txt_owner_name.Text,
            inspection_address: txt_inspection_address.Text,
            inspection_gov_code: ddl_inspection_gov.SelectedValue,
            inspection_city_code: ddl_inspection_city.SelectedValue,
            inspection_dist_code: ddl_inspection_dist.SelectedValue,
            buy_shop_code: ddl_buy_shop.SelectedValue,
            inspection_planed_date: txt_inspection_planed_date.SelectedDate.Value.ToShortDateString(),
            inspection_branch: ddl_inspection_branch.SelectedValue,
            inspection_rep_code: ddl_inspection_rep.SelectedValue,
            cust_code: hidden_code.Value,
            from_time: txt_inspection_planed_from_time.SelectedTime.ToString(),
            to_time: txt_inspection_planed_to_time.SelectedTime.ToString(),
            feedback: txt_inspection_notes.Text,
            inspection_creator: CurrentUsername,
            gur_status: ddl_grantee_status.SelectedValue,
            grantee_status_reason: txt_grantee_status_reason.Text,
            inspection_source: ddl_inspection_source.SelectedValue,
            owner_tel: txt_owner_tel.Text,
            owner_mobile: txt_owner_mobile.Text,
            inspection_description: ddl_inspection_description.SelectedValue,
            inspection_printed_no: txt_inspection_printed_NO.Text,
            caller_name: string.Empty,// txt_caller_name.Text,
            caller_tel: string.Empty,// txt_caller_tel.Text,
            caller_type: string.Empty,// ddl_caller_type.SelectedValue,
            view_status: ddl_inspection_status.SelectedValue,
            rep_points: txt_inspection_rep_points.Text,
            inspection_status_note: txt_inspection_status_note.Text,
            data_entry_notes: txt_inspection_data_entry_notes.Text,
            order_status: ddl_inspection_order_status.SelectedValue,
            inspection_reject_reason: txt_inspection_reject_reason.Text,
            stop_printing_reoprts: ddl_stop_printing_reoprts.SelectedValue,
            stop_printing_reoprts_reason: txt_stop_printing_reoprts_reason.Text,
            view_type: ddl_inspection_type.SelectedValue,
            inspection_qa_resp_name: txt_inspection_qa_resp.Text,
            inspection_situation: ddl_inspection_situation.SelectedValue,
            place_type: ddl_inspection_place_type.SelectedValue,
            currentUsername: CurrentUsername
            );
        if (!success)
            ShowErrorMessages($"فشل في الحفظ: {Environment.NewLine}({errorMessage})");
        else
        {
            lblSuccessMessage.InnerText = "تم الحفظ بنجاح";
            lblSuccessMessage.Visible = true;
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            // fill ddl_branch
            var dataBarnches = _lookupService.GetAllBranches();
            ddl_branch.FillDataSource(dataBarnches);
            ddl_inspection_branch.FillDataSource(dataBarnches);

            // fill ddl_gov
            var dataGovernorates = _lookupService.GetAllGovernorates();
            ddl_work_gov.FillDataSource(dataGovernorates);
            ddl_birth_gov.FillDataSource(dataGovernorates);
            ddl_shop_gov.FillDataSource(dataGovernorates);
            ddl_inspection_gov.FillDataSource(dataGovernorates);

            // fill ddl_group_name
            var dataGroups = _lookupService.GetAllGroups();
            ddl_group_name.FillDataSource(dataGroups);

            var dataInspectionsDescriptions = _lookupService.GetAllInspectionsDescriptions();
            ddl_inspection_description.FillDataSource(dataInspectionsDescriptions);

            //var dataInspectionsTypes = _lookupService.GetAllInspectionsTypes();
            //ddl_inspection_type.FillDataSource(dataInspectionsTypes);

            var dataInspectionsPlacesTypes = _lookupService.GetAllInspectionsPlacesTypes();
            ddl_inspection_place_type.FillDataSource(dataInspectionsPlacesTypes);

            txt_rec_date.Text = DateTime.Now.ToShortDateString();
            txt_plumber_birthdate.MinDate = DateTime.Now.AddYears(-100);
        }
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        ResetPlumberForm();
        ResetInspectionForm();

        lblErrorMessage.Visible = false;
        lblSuccessMessage.Visible = false;
    }

    protected void btnSaveAll_Click(object sender, EventArgs e)
    {
        List<string> errorMessages = new List<string>();

        if (string.IsNullOrWhiteSpace(txt_owner_name.Text))
            errorMessages.Add($"يجب ادخال اسم المالك");

        if (string.IsNullOrWhiteSpace(ddl_inspection_gov.SelectedValue))
            errorMessages.Add($"يجب ادخال محافظة المعاينة");
        if (string.IsNullOrWhiteSpace(ddl_inspection_city.SelectedValue))
            errorMessages.Add($"يجب ادخال منطقة المعاينة");
        if (string.IsNullOrWhiteSpace(ddl_inspection_dist.SelectedValue))
            errorMessages.Add($"يجب ادخال حي المعاينة");

        if (string.IsNullOrWhiteSpace(txt_inspection_address.Text))
            errorMessages.Add($"يجب ادخال عنوان المعاينة");

        if (string.IsNullOrWhiteSpace(ddl_buy_shop.SelectedValue))
            errorMessages.Add($"يجب اختيار محل الشراء");

        if (txt_inspection_planed_date.SelectedDate == null)
            errorMessages.Add($"يجب تحديد تاريخ المعاينة");

        if (string.IsNullOrWhiteSpace(ddl_grantee_status.SelectedValue))
            errorMessages.Add($"يجب تحديد حالة الضمان");

        if (string.IsNullOrWhiteSpace(txt_plumber_name.Text))
            errorMessages.Add($"يجب ادخال اسم السباك");
        if (string.IsNullOrWhiteSpace(txt_plumber_mobile.Text))
            errorMessages.Add($"يجب ادخال موبايل السباك");
        if (string.IsNullOrWhiteSpace(txt_plumber_ID.Text))
            errorMessages.Add($"يجب ادخال رقم بطاقة السباك");
        if (string.IsNullOrWhiteSpace(txt_home_address.Text))
            errorMessages.Add($"يجب ادخال عنوان منزل السباك");
        if (!string.IsNullOrWhiteSpace(ddl_isActive.SelectedValue) && ddl_isActive.SelectedValue.Equals("موقوف") && string.IsNullOrWhiteSpace(txt_deactivation_reason.Text))
            errorMessages.Add($"يجب ادخال سبب وقف السباك");

        ShowErrorMessages(errorMessages);

        SavePlumberData();
        SaveInspectionData();
    }

    protected void btnSaveInspectionData_Click(object sender, EventArgs e)
    {
        SaveInspectionData();
    }

    protected void btnSavePlumberData_Click(object sender, EventArgs e)
    {
        SavePlumberData();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        ResetInspectionForm();
        ResetPlumberForm();

        var btnSearch = (Button)RadLightBox1.FindControl("btnSearch");
        var txtSearchBy = (TextBox)RadLightBox1.FindControl("txtSearchBy");
        if (string.IsNullOrWhiteSpace(txtSearchBy.Text))
            return;

        var plumberData = _egService.BindViewingTree(txtSearchBy.Text);
        RadTreeView1.DataSource = plumberData;
        RadTreeView1.DataFieldID = "CUST_CODE";
        RadTreeView1.DataFieldParentID = "PARENT";
        RadTreeView1.DataTextField = "CUST_NAME";
        RadTreeView1.DataValueField = "CUST_CODE";
        RadTreeView1.DataBind();
        RadTreeView1.ExpandAllNodes();

        if (plumberData.Rows.Count > 0 && int.TryParse(plumberData.Rows[0]["CUST_CODE"].ToString(), out int code))
        {
            var plumber = _egService.GetPlumberDataByCode(code);
            if (plumber.Rows.Count > 0)
                LoadPlumberData(plumber.Rows[0]);

        }
        else
        {
            var plumber = _egService.GetPlumberDataByCode(ConfigurationManager.AppSettings["UnknownPlumberCode"]);
            LoadPlumberData(plumber.Rows[0]);
        }
        return;

        var radioByMobile = (RadioButton)RadLightBox1.FindControl("radioByMobile");
        var radioByInspectionCode = (RadioButton)RadLightBox1.FindControl("radioByInspectionCode");

        string callerType = "";
        DataTable callerData = null;
        if (radioByMobile.Checked)
            callerData = _egService.SearchForPlumber(txtSearchBy.Text, out callerType);
        else if (radioByInspectionCode.Checked)
        {
            callerData = _egService.GetAllInspectionData(txtSearchBy.Text);
            if (callerData.Rows.Count == 0)
                return;

            DataTable plumberDT = new DataTable();
            plumberDT.Clear();
            plumberDT.Columns.Add("CUST_CODE");
            plumberDT.Columns.Add("CUST_NAME");
            DataRow row = plumberDT.NewRow();
            row["CUST_CODE"] = callerData.Rows[0]["CUST_CODE"].ToString();
            row["CUST_NAME"] = callerData.Rows[0]["CUST_NAME"].ToString();
            plumberDT.Rows.Add(row);

            //RGridPlumbersInspections.DataSource = plumberDT;
            //RGridPlumbersInspections.DataBind();

            DataTable inspectionDT = new DataTable();
            inspectionDT.Clear();
            inspectionDT.Columns.Add("INSPECTION_CODE");
            inspectionDT.Columns.Add("INSPECTION_NAME");
            DataRow row2 = inspectionDT.NewRow();
            row2["INSPECTION_CODE"] = callerData.Rows[0]["INSPECTION_CODE"].ToString();
            row2["INSPECTION_NAME"] = callerData.Rows[0]["INSPECTION_NAME"].ToString();
            inspectionDT.Rows.Add(row2);

            //RGridPlumbersInspections.MasterTableView.DetailTables[0].DataSource = inspectionDT;
            //RGridPlumbersInspections.MasterTableView.DetailTables[0].DataBind();
            //RGridPlumbersInspections.MasterTableView.Items[0].Expanded = true;

            if (int.TryParse(callerData.Rows[0]["INSPECTION_CODE"].ToString(), out int inspectionCode))
                LoadInspectionData(inspectionCode);

            return;
        }

        //RGridPlumbersInspections.DataSource = callerData;
        //RGridPlumbersInspections.DataBind();
        lblCallerType.InnerText = callerType;
    }

    protected void ddl_birth_gov_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (int.TryParse(ddl_birth_gov.SelectedValue, out int governorateCode) && governorateCode > 0)
        {
            var items = _lookupService.GetAllCities(governorateCode);
            ddl_birth_city.FillDataSource(items);
        }
    }
    protected void ddl_birth_city_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (int.TryParse(ddl_birth_city.SelectedValue, out int cityCode) && cityCode > 0)
        {
            var items = _lookupService.GetAllDistricts(cityCode);
            ddl_birth_dist.FillDataSource(items);
        }
    }

    protected void ddl_work_gov_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (int.TryParse(ddl_work_gov.SelectedValue, out int governorateCode) && governorateCode > 0)
        {
            var items = _lookupService.GetAllCities(governorateCode);
            ddl_work_city.FillDataSource(items);
        }
    }
    protected void ddl_work_city_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (int.TryParse(ddl_work_city.SelectedValue, out int cityCode) && cityCode > 0)
        {
            var items = _lookupService.GetAllDistricts(cityCode);
            ddl_work_dist.FillDataSource(items);
        }
    }
    protected void ddl_work_dist_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (int.TryParse(ddl_work_dist.SelectedValue, out int distCode) && distCode > 0)
        {
            var items = _lookupService.GetAllShops(districtCode: distCode);
            ddl_work_shop.FillDataSource(items);
        }
    }

    protected void ddl_group_name_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (int.TryParse(ddl_group_name.SelectedValue, out int groupCode) && groupCode > 0)
        {
            var items = _lookupService.GetAllGroupsManagers(groupCode);
            ddl_group_manager.FillDataSource(items);
            txt_sales_rep.Text = _lookupService.GetSalesRepName(groupCode);
        }
    }
    protected void ddl_group_manager_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (int.TryParse(ddl_group_manager.SelectedValue, out int managerCode) && managerCode > 0)
        {
            var items = _lookupService.GetAllInspectionReps(managerCode);
            ddl_inspection_rep.FillDataSource(items);
        }
    }

    protected void ddl_inspection_gov_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (int.TryParse(ddl_inspection_gov.SelectedValue, out int governorateCode) && governorateCode > 0)
        {
            var items = _lookupService.GetAllCities(governorateCode);
            ddl_inspection_city.FillDataSource(items);
        }
    }
    protected void ddl_inspection_city_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (int.TryParse(ddl_inspection_city.SelectedValue, out int cityCode) && cityCode > 0)
        {
            var items = _lookupService.GetAllDistricts(cityCode);
            ddl_inspection_dist.FillDataSource(items);
        }
    }

    protected void ddl_shop_gov_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (int.TryParse(ddl_shop_gov.SelectedValue, out int governorateCode) && governorateCode > 0)
        {
            var items = _lookupService.GetAllCities(governorateCode);
            ddl_shop_city.FillDataSource(items);
        }
    }
    protected void ddl_shop_city_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (int.TryParse(ddl_shop_city.SelectedValue, out int cityCode) && cityCode > 0)
        {
            var items = _lookupService.GetAllDistricts(cityCode);
            ddl_shop_dist.FillDataSource(items);
        }
    }
    protected void ddl_shop_dist_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (int.TryParse(ddl_shop_dist.SelectedValue, out int distCode) && distCode > 0)
        {
            var items = _lookupService.GetAllShops(districtCode: distCode);
            ddl_buy_shop.FillDataSource(items);
        }
    }

    protected void RadTreeList1_NeedDataSource(object sender, Telerik.Web.UI.TreeListNeedDataSourceEventArgs e)
    {
        //RadTreeList1.DataSource = _egService.GetAllPlumbersData(txtCallerNumber.Text);
    }

    protected void RadGridInspectionProducts_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(hiddenInspectionCode.Value) && int.TryParse(hiddenInspectionCode.Value, out int inspectionCode) && inspectionCode > 0)
        {
            RadGridInspectionProducts.DataSource = _egService.GetAllInspectionProducts(inspectionCode);
        }
    }

    //protected void RadGridInspectionProducts_ItemCreated(object sender, Telerik.Web.UI.GridItemEventArgs e)
    //{
    //    if (e.Item is GridEditableItem && (e.Item.IsInEditMode))
    //    {
    //        GridEditableItem editableItem = (GridEditableItem)e.Item;
    //        SetupInputManager(editableItem);
    //    }
    //}

    protected void RadGridInspectionProducts_InsertCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        var editableItem = ((GridEditableItem)e.Item);

        Hashtable values = new Hashtable();
        editableItem.ExtractValues(values);
        try
        {
            if (values["Qty"] != null && int.TryParse(values["Qty"].ToString(), out int qty))
                _egService.InsertInspectionProduct(
                    hiddenInspectionCode.Value,
                    values["Product_Code"].ToString(),
                    values["Item_Code"].ToString(),
                    qty,
                    values["Notes"].ToString(),
                    values["Card_No"].ToString(),
                    CurrentUsername,
                    values["Update_Reason"].ToString(),
                    values["Update_Resp"].ToString(),
                    values["Update_No"].ToString(),
                    values["Qty_Bef_Update"].ToString());
        }
        catch (System.Exception ex)
        {
            ShowErrorMessages(ex.Message);
        }
    }
    protected void RadGridInspectionProducts_UpdateCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        var editableItem = ((GridEditableItem)e.Item);
        Hashtable values = new Hashtable();
        editableItem.ExtractValues(values);

        try
        {
            if (values["Qty"] != null && int.TryParse(values["Qty"].ToString(), out int qty))
                _egService.UpdateInspectionProduct(
                    values["Product_Code"].ToString(),
                    values["Item_Code"].ToString(),
                    qty,
                    values["Notes"].ToString(),
                    values["Card_No"].ToString(),
                    CurrentUsername,
                    values["Update_Reason"].ToString(),
                    values["Update_Resp"].ToString(),
                    values["Update_No"].ToString(),
                    values["Qty_Bef_Update"].ToString());
        }
        catch (Exception ex)
        {
            ShowErrorMessages(ex.Message);
        }
    }
    protected void RadGridInspectionProducts_DeleteCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        var editableItem = ((GridEditableItem)e.Item);
        var productCodeStr = editableItem.GetDataKeyValue("Code");

        if (productCodeStr == null || !int.TryParse(productCodeStr.ToString(), out int productCode))
            return;

        if (productCode > 0)
        {
            try
            {
                Hashtable values = new Hashtable();
                editableItem.ExtractValues(values);


                _egService.DeleteInspectionProduct(productCode);
            }
            catch (System.Exception ex)
            {
                ShowErrorMessages(ex.Message);
            }
        }
    }

    protected void RadGridInspectionCertificates_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(hiddenInspectionCode.Value) && int.TryParse(hiddenInspectionCode.Value, out int inspectionCode) && inspectionCode > 0)
        {
            RadGridInspectionCertificates.DataSource = _egService.GetAllInspectionPrints(inspectionCode);
        }
    }
    protected void RadGridInspectionCertificates_UpdateCommand(object sender, GridCommandEventArgs e)
    {
        var editableItem = ((GridEditableItem)e.Item);
        var printCode = (int)editableItem.GetDataKeyValue("CODE");
        var printReceiverName = (string)editableItem.GetDataKeyValue("RECEIVER_NAME");
        try
        {
            _egService.UpdateInspectionPrint(printCode, printReceiverName);
        }
        catch (System.Exception ex)
        {
            ShowErrorMessages(ex.Message);
        }
    }

    protected void RadTreeView1_NodeClick(object sender, RadTreeNodeEventArgs e)
    {
        ResetInspectionForm();
        ResetPlumberForm();
        var codeObj = e.Node.Value;
        if (codeObj != null && int.TryParse(codeObj.ToString(), out int code))
        {
            if (e.Node.Level == 1)
                LoadInspectionData(code);
            else if (e.Node.Level == 0)
            {
                var plumber = _egService.GetPlumberDataByCode(code);
                if (plumber != null && plumber.Rows.Count > 0)
                    LoadPlumberData(plumber.Rows[0]);
            }
        }
    }

    //protected void RGridPlumbersInspections_PreRender(object sender, EventArgs e)
    //{

    //}
    //protected void RGridPlumbersInspections_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    //{
    //    //if (!e.IsFromDetailTable)
    //    //{
    //    //    RGridPlumbersInspections.DataSource = null;
    //    //}
    //}
    //protected void RGridPlumbersInspections_DetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e)
    //{
    //    GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;
    //    string cust_code = dataItem.GetDataKeyValue("CUST_CODE").ToString();
    //    e.DetailTableView.DataSource = _egService.GetPlumberLatestInspections(cust_code);
    //}
    //protected void RGridPlumbersInspections_ItemCommand(object sender, GridCommandEventArgs e)
    //{
    //    if (e.CommandName == "RowClick" || e.CommandName == "ExpandCollapse")
    //    {
    //        ResetInspectionForm();
    //        ResetPlumberForm();
    //        var editableItem = ((GridEditableItem)e.Item);
    //        var inspectionCodeObj = editableItem.GetDataKeyValue("INSPECTION_CODE");
    //        if (inspectionCodeObj != null && int.TryParse(inspectionCodeObj.ToString(), out int inspectionCode))
    //        {
    //            LoadInspectionData(inspectionCode);
    //            return;
    //        }
    //        else if (editableItem.GetDataKeyValue("CUST_CODE") != null && int.TryParse(editableItem.GetDataKeyValue("CUST_CODE").ToString(), out int custCode) && custCode > 0)
    //        {
    //            var plumber = _egService.GetPlumberDataByCode(custCode);
    //            if (plumber != null && plumber.Rows.Count > 0)
    //                LoadPlumberData(plumber.Rows[0]);
    //        }
    //    }
    //}

}

public class TreeViewItem
{
    public string ID { get; set; }
    public string Name { get; set; }
    public string Parent { get; set; }
}