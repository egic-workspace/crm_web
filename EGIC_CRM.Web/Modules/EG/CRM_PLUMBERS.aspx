﻿<%@ Page Title="تعريف السباكين" Language="C#" AutoEventWireup="true" CodeFile="CRM_PLUMBERS.aspx.cs" Inherits="Modules_EG_CRM_PLUMBERS" MasterPageFile="~/Main.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">

    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"></asp:ScriptManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>

            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="filePlumberID" />
                    <telerik:AjaxUpdatedControl ControlID="imgPlumberID" />

                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="ddl_work_gov">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ddl_work_city" />
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="ddl_work_city">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ddl_work_dist" />
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="ddl_work_dist">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ddl_work_shop" />
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="ddl_group_name">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="txt_sales_rep" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_group_manager" />
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="ddl_group_manager">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_rep" />
                </UpdatedControls>
            </telerik:AjaxSetting>

            <%--            <telerik:AjaxSetting AjaxControlID="filePlumberID">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="imgPlumberID" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>

            <telerik:AjaxSetting AjaxControlID="btnReset">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                    <telerik:AjaxUpdatedControl ControlID="hidden_code" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_religion" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_isActive" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_branch" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_work_shop" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_work_gov" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_work_city" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_work_dist" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_group_name" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_group_manager" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_rep" />

                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_name" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_mobile" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_ID" />
                    <telerik:AjaxUpdatedControl ControlID="txt_home_address" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_nickname" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_phone" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_phone_2" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_birthdate" />
                    <telerik:AjaxUpdatedControl ControlID="txt_deactivation_resion" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_place" />
                    <telerik:AjaxUpdatedControl ControlID="txt_sales_rep" />

                    <telerik:AjaxUpdatedControl ControlID="lblErrorMessage" />
                    <telerik:AjaxUpdatedControl ControlID="lblSuccessMessage" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btn_save">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                    <telerik:AjaxUpdatedControl ControlID="hidden_code" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_religion" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_isActive" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_branch" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_work_shop" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_work_gov" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_work_city" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_work_dist" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_group_name" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_group_manager" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_rep" />

                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_name" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_mobile" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_ID" />
                    <telerik:AjaxUpdatedControl ControlID="txt_home_address" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_nickname" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_phone" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_phone_2" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_birthdate" />
                    <telerik:AjaxUpdatedControl ControlID="txt_deactivation_resion" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_place" />
                    <telerik:AjaxUpdatedControl ControlID="txt_sales_rep" />

                    <telerik:AjaxUpdatedControl ControlID="lblErrorMessage" />
                    <telerik:AjaxUpdatedControl ControlID="lblSuccessMessage" />
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>

                    <telerik:AjaxUpdatedControl ControlID="hidden_code" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_name" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_mobile" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_ID" />
                    <telerik:AjaxUpdatedControl ControlID="txt_home_address" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_nickname" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_phone" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_phone_2" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_birthdate" />
                    <telerik:AjaxUpdatedControl ControlID="txt_deactivation_resion" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_place" />
                    <telerik:AjaxUpdatedControl ControlID="txt_sales_rep" />

                    <telerik:AjaxUpdatedControl ControlID="ddl_religion" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_isActive" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_branch" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_work_shop" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_work_gov" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_work_city" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_work_dist" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_group_name" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_group_manager" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_rep" />

                    <telerik:AjaxUpdatedControl ControlID="lblErrorMessage" />
                    <telerik:AjaxUpdatedControl ControlID="lblSuccessMessage" />

                </UpdatedControls>
            </telerik:AjaxSetting>

        </AjaxSettings>
    </telerik:RadAjaxManager>
    <div class="raw clearfix">
        <div class="box box-danger ">
            <div class="box-header with-border">
                <div class="box-title">
                    إدخال بيانات السباك
                </div>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">

                <div class="col-md-12">

                    <div class="col-md-3">
                        <div class="form-group form-group-sm">
                            <label for="txt_plumber_name">اسم السباك</label>
                            <span class="danger text-bold">* </span>
                            <asp:RequiredFieldValidator runat="server" ErrorMessage=" اسم السباك مطلوب" ForeColor="Red" ControlToValidate="txt_plumber_name"></asp:RequiredFieldValidator>
                            <asp:TextBox ID="txt_plumber_name" runat="server" CssClass="form-control" placeholder="اسم السباك (مطلوب)"></asp:TextBox>
                            <asp:HiddenField ID="hidden_code" runat="server" />
                        </div>

                        <div class="form-group form-group-sm">
                            <label for="txt_plumber_mobile">الموبايل</label>
                            <span class="danger text-bold">* </span>
                            <asp:RegularExpressionValidator runat="server" ErrorMessage="يجب ادخال رقم موبايل صحيح" ForeColor="Red" ValidationExpression="[0-9]{7,12}" ControlToValidate="txt_plumber_mobile"></asp:RegularExpressionValidator>
                            <asp:TextBox ID="txt_plumber_mobile" runat="server" MaxLength="12" CssClass="form-control" placeholder="الموبايل (مطلوب)"></asp:TextBox>
                        </div>

                        <div class="form-group form-group-sm">
                            <label for="txt_plumber_ID">رقم البطاقة</label>
                            <span class="danger text-bold">* </span>
                            <asp:RegularExpressionValidator runat="server" ErrorMessage="يجب ادخال رقم بطاقة صحيح" ForeColor="Red" ValidationExpression="[0-9]{14}" ControlToValidate="txt_plumber_ID"></asp:RegularExpressionValidator>
                            <asp:TextBox ID="txt_plumber_ID" runat="server" MaxLength="14" CssClass="form-control" placeholder="رقم البطاقة (مطلوب)"></asp:TextBox>
                        </div>

                        <div class="form-group form-group-sm">
                            <label for="txt_home_address">عنوان المنزل</label>
                            <span class="danger text-bold">* </span>
                            <asp:RequiredFieldValidator runat="server" ErrorMessage=" عنوان منزل السباك مطلوب" ForeColor="Red" ControlToValidate="txt_home_address"></asp:RequiredFieldValidator>
                            <asp:TextBox ID="txt_home_address" runat="server" CssClass="form-control" placeholder="عنوان المنزل (مطلوب)"></asp:TextBox>
                        </div>

                    </div>
                    <div class="col-md-3">

                        <div class="form-group form-group-sm">
                            <label for="txt_plumber_nickname">اسم الشهرة</label>
                            <asp:TextBox ID="txt_plumber_nickname" runat="server" CssClass="form-control" placeholder="اسم الشهرة"></asp:TextBox>
                        </div>

                        <div class="form-group form-group-sm">
                            <label for="txt_plumber_phone">التليفون</label>
                            <asp:TextBox ID="txt_plumber_phone" runat="server" TextMode="Phone" CssClass="form-control" placeholder="التليفون"></asp:TextBox>
                        </div>

                        <div class="form-group form-group-sm">
                            <label for="txt_plumber_phone_2">تليفون إضافي</label>
                            <asp:TextBox ID="txt_plumber_phone_2" runat="server" TextMode="Phone" CssClass="form-control" placeholder="تليفون إضافي"></asp:TextBox>
                        </div>


                        <div class="form-group form-group-sm">
                            <label for="ddl_gender">النوع</label>
                            <asp:DropDownList ID="ddl_gender" runat="server" CssClass="form-control btn-sm">
                                <asp:ListItem Value="ذكر">ذكر</asp:ListItem>
                                <asp:ListItem Value="انثى">انثى</asp:ListItem>
                            </asp:DropDownList>
                        </div>

                    </div>
                    <div class="col-md-3">

                        <div class="form-group form-group-sm">
                            <label for="txt_plumber_nationality">الجنسية</label>
                            <asp:TextBox ID="txt_plumber_nationality" runat="server" CssClass="form-control" placeholder="الجنسية" Text="مصري"></asp:TextBox>
                        </div>


                        <div class="form-group form-group-sm">
                            <label for="ddl_religion">الديانة</label>
                            <asp:DropDownList ID="ddl_religion" runat="server" CssClass="form-control btn-sm">
                                <asp:ListItem Value="">إختر...</asp:ListItem>
                                <asp:ListItem Value="1">مسلم</asp:ListItem>
                                <asp:ListItem Value="0">مسيحي</asp:ListItem>
                            </asp:DropDownList>
                        </div>

                        <div class="form-group form-group-sm">
                            <label for="ddl_isActive">حالة العمل</label>
                            <asp:DropDownList ID="ddl_isActive" runat="server" CssClass="form-control btn-sm">
                                <asp:ListItem Value="">إختر...</asp:ListItem>
                                <asp:ListItem Value="يعمل">يعمل</asp:ListItem>
                                <asp:ListItem Value="موقوف">موقوف</asp:ListItem>
                            </asp:DropDownList>
                        </div>

                        <div class="form-group form-group-sm" id="div_deactivation_resion">
                            <label for="txt_deactivation_reason">سبب الوقف</label>
                            <asp:TextBox ID="txt_deactivation_reason" runat="server" TextMode="MultiLine" Rows="2" CssClass="form-control" placeholder="سبب الوقف"></asp:TextBox>
                        </div>

                    </div>
                    <div class="col-md-3">

                        <div class="form-group form-group-sm">
                            <label for="ddl_birth_gov">محافظة الميلاد</label>
                            <asp:DropDownList ID="ddl_birth_gov" runat="server" CssClass="form-control btn-sm" OnSelectedIndexChanged="ddl_birth_gov_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="">إختر...</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form-group form-group-sm">
                            <label for="ddl_birth_city">حي الميلاد</label>
                            <asp:DropDownList ID="ddl_birth_city" runat="server" Width="100%" CssClass="form-control btn-sm btn-block select2" OnSelectedIndexChanged="ddl_birth_city_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="">إختر...</asp:ListItem>
                            </asp:DropDownList>
                        </div>

                        <div class="form-group form-group-sm">
                            <label for="ddl_birth_dist">منطقة الميلاد</label>
                            <asp:DropDownList ID="ddl_birth_dist" runat="server" Width="100%" CssClass="form-control btn-sm btn-block select2">
                                <asp:ListItem Value="">إختر...</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        
                        <div class="form-group form-group-sm">
                            <label for="txt_plumber_birthdate">تاريخ الميلاد</label>
                            <telerik:RadDatePicker runat="server" ID="txt_plumber_birthdate" Width="100%"></telerik:RadDatePicker>
                        </div>

                        <%-- 
                            <div class="form-group form-group-sm">
                                <label for="filePlumberID">صورة البطاقة</label>
                                <telerik:RadAsyncUpload RenderMode="Lightweight" ID="filePlumberID" Skin="Bootstrap" runat="server" 
                                    OnClientFilesUploaded="OnClientFilesUploaded" OnFileUploaded="filePlumberID_FileUploaded" CssClass="form-control"
                                    MaxFileSize="2097152" AllowedFileExtensions="jpg"
                                    AutoAddFileInputs="false" Localization-Select="اختر صورة" />

                                <telerik:RadBinaryImage runat="server" ID="imgPlumberID" AutoAdjustImageControlSize="false" Width="200px" Height="200px" />

                            </div>
                            --%>

                    </div>

                </div>


                <div class="col-md-12">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_1" data-toggle="tab">
                                    بيانات العمل ومكتب خدمة العملاء التابع له العميل
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="active tab-pane" id="tab_1">

                                <div class="raw clearfix">

                                    <div class="col-md-3">

                                        <div class="form-group form-group-sm">
                                            <label for="ddl_work_gov">المحافظة</label>
                                            <asp:DropDownList ID="ddl_work_gov" runat="server" CssClass="form-control btn-sm" OnSelectedIndexChanged="ddl_work_gov_SelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem Value="">إختر...</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <div class="form-group form-group-sm">
                                            <label for="ddl_work_city">الحي</label>
                                            <asp:DropDownList ID="ddl_work_city" runat="server" Width="100%" CssClass="form-control btn-sm btn-block select2" OnSelectedIndexChanged="ddl_work_city_SelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem Value="">إختر...</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <div class="form-group form-group-sm">
                                            <label for="ddl_work_dist">المنطقة</label>
                                            <asp:DropDownList ID="ddl_work_dist" runat="server" Width="100%" CssClass="form-control btn-sm btn-block select2" OnSelectedIndexChanged="ddl_work_dist_SelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem Value="">إختر...</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <div class="form-group form-group-sm">
                                            <label for="ddl_work_shop">المحل</label>
                                            <asp:DropDownList ID="ddl_work_shop" runat="server" Width="100%" CssClass="form-control btn-sm btn-block">
                                                <asp:ListItem Value="">إختر...</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                    </div>

                                    <div class="col-md-3">

                                        <div class="form-group form-group-sm">
                                            <label for="ddl_group_name">اسم المجموعة</label>
                                            <asp:DropDownList ID="ddl_group_name" runat="server" Width="100%" CssClass="form-control btn-sm btn-block select2" OnSelectedIndexChanged="ddl_group_name_SelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem Value="">إختر...</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <div class="form-group form-group-sm">
                                            <label for="txt_sales_rep">مندوب البيع</label>
                                            <asp:TextBox ID="txt_sales_rep" runat="server" CssClass="form-control" placeholder="مندوب البيع" ReadOnly="true" Enabled="false"></asp:TextBox>
                                        </div>

                                        <div class="form-group form-group-sm">
                                            <label for="ddl_group_manager">رئيس المجموعة</label>
                                            <asp:DropDownList ID="ddl_group_manager" runat="server" CssClass="form-control btn-sm" OnSelectedIndexChanged="ddl_group_manager_SelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem Value="">إختر...</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <div class="form-group form-group-sm">
                                            <label for="ddl_inspection_rep">مندوب المتابعة</label>
                                            <asp:DropDownList ID="ddl_inspection_rep" runat="server" CssClass="form-control btn-sm">
                                                <asp:ListItem Value="">إختر...</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                    </div>

                                    <div class="col-md-3">

                                        <div class="form-group form-group-sm">
                                            <label for="txt_plumber_place">مكان التواجد</label>
                                            <asp:TextBox ID="txt_plumber_place" runat="server" CssClass="form-control" placeholder="مكان التواجد"></asp:TextBox>
                                        </div>

                                        <div class="form-group form-group-sm">
                                            <label for="ddl_branch">الفرع</label>
                                            <asp:DropDownList ID="ddl_branch" runat="server" CssClass="form-control btn-sm">
                                                <asp:ListItem Value="">إختر...</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <div class="form-group form-group-sm">
                                            <label for="txt_creator_name">إسم المستخدم</label>
                                            <asp:TextBox ID="txt_creator_name" runat="server" CssClass="form-control" placeholder="إسم المستخدم" ReadOnly="true" Enabled="false"></asp:TextBox>
                                        </div>

                                        <div class="form-group form-group-sm">
                                            <label for="txt_rec_date">تاريخ التسجيل</label>
                                            <asp:TextBox ID="txt_rec_date" runat="server" CssClass="form-control" placeholder="تاريخ التسجيل" ReadOnly="true" Enabled="false"></asp:TextBox>
                                        </div>

                                    </div>
                                    <div class="col-md-3">

                                        <div class="form-group form-group-sm">
                                            <label for="txt_total_points">إجمالي النقاط</label>
                                            <asp:TextBox ID="txt_total_points" runat="server" CssClass="form-control" placeholder="إجمالي النقاط" ReadOnly="true" Enabled="false"></asp:TextBox>
                                        </div>

                                        <div class="form-group form-group-sm">
                                            <label for="txt_balance">الرصيد</label>
                                            <asp:TextBox ID="txt_balance" runat="server" CssClass="form-control" placeholder="الرصيد" ReadOnly="true" Enabled="false"></asp:TextBox>
                                        </div>

                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <asp:Button ID="btn_save" runat="server" Text="حفظ" CssClass="btn btn-primary" OnClick="btn_save_Click" />
                <asp:Button ID="btnReset" runat="server" Text="إلغاء" CssClass="btn" OnClick="btnReset_Click" CausesValidation="false" />
            </div>
        </div>
        <p class="alert alert-danger" runat="server" id="lblErrorMessage" visible="false"></p>
        <div class="alert alert-success alert-dismissable" runat="server" id="lblSuccessMessage" visible="false">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            تم الحفظ بنجاح
        </div>


        <div class="box box-danger">
            <div class="box-header with-border">
                <h5>بيانات السباكين
                </h5>
            </div>
            <div class="box-body">

                <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Bootstrap"></telerik:RadAjaxLoadingPanel>
                <telerik:RadAjaxPanel runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
                    <telerik:RadGrid
                        runat="server"
                        ID="RadGrid1"
                        AllowFilteringByColumn="false"
                        AllowPaging="True"
                        AllowSorting="True"
                        ShowGroupPanel="True"
                        AutoGenerateColumns="False"
                        OnNeedDataSource="RadGrid1_NeedDataSource"
                        OnItemCommand="RadGrid1_ItemCommand"
                        Skin="Bootstrap">

                        <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
                        </HeaderContextMenu>

                        <MasterTableView CommandItemDisplay="Top" AllowPaging="true" Dir="RTL">

                            <CommandItemSettings
                                ShowExportToCsvButton="true"
                                ShowExportToExcelButton="true"
                                ShowExportToWordButton="true"
                                ShowExportToPdfButton="true"
                                ShowAddNewRecordButton="false" />

                            <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                            </RowIndicatorColumn>

                            <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                            </ExpandCollapseColumn>

                            <Columns>

                                <telerik:GridButtonColumn
                                    ButtonType="ImageButton"
                                    CommandName="UPDATE"
                                    Text="تعديل"
                                    UniqueName="column"
                                    ImageUrl="~/Content/images/edit.gif">
                                    <ItemStyle BackColor="Transparent" BorderStyle="Solid" Font-Bold="True" CssClass="btn-updt" Font-Names="Arial" Font-Size="10pt" ForeColor="Black" HorizontalAlign="Center" />
                                </telerik:GridButtonColumn>

                                <telerik:GridButtonColumn
                                    ButtonType="ImageButton"
                                    CommandName="DELETE"
                                    ImageUrl="~/Content/images/delete.gif"
                                    Text="حذف"
                                    UniqueName="column1"
                                    ConfirmDialogHeight="200px"
                                    ConfirmDialogType="RadWindow"
                                    ConfirmDialogWidth="400px"
                                    ConfirmText="هل تريد حذف هذا السباك ؟"
                                    ConfirmTitle="حذف سباك">
                                </telerik:GridButtonColumn>

                                <telerik:GridTemplateColumn
                                    DataField="code"
                                    UniqueName="code"
                                    SortExpression="code"
                                    GroupByExpression="code Group By code"
                                    HeaderText="كود السباك">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txt_code" Enabled="false" runat="server" Text='<%# Bind("code") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_code" runat="server" Text='<%# Eval("code") %>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>

                                <telerik:GridTemplateColumn
                                    DataField="name"
                                    UniqueName="name"
                                    SortExpression="name"
                                    GroupByExpression="name Group By name"
                                    HeaderText="اسم السباك">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txt_name" runat="server" Text='<%# Bind("name") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_name" runat="server" Text='<%# Eval("name") %>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>

                                <telerik:GridTemplateColumn
                                    DataField="mobile"
                                    UniqueName="mobile"
                                    SortExpression="mobile"
                                    GroupByExpression="mobile Group By mobile"
                                    HeaderText="موبايل">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txt_mobile" Enabled="true" runat="server" Text='<%# Bind("mobile") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_mobile" runat="server" Text='<%# Eval("mobile") %>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>

                                <telerik:GridTemplateColumn
                                    DataField="id_no"
                                    UniqueName="id_no"
                                    SortExpression="id_no"
                                    GroupByExpression="id_no Group By id_no"
                                    HeaderText="رقم البطاقة">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txt_ID" Enabled="true" runat="server" Text='<%# Bind("id_no") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_ID" runat="server" Text='<%# Eval("id_no") %>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>

                                <telerik:GridTemplateColumn
                                    DataField="home_address"
                                    UniqueName="home_address"
                                    SortExpression="home_address"
                                    GroupByExpression="home_address Group By home_address"
                                    HeaderText="عنوان المنزل">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txt_h_address" runat="server" Text='<%# Bind("home_address") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_h_address" runat="server" Text='<%# Eval("home_address") %>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>

                                <telerik:GridTemplateColumn
                                    DataField="title"
                                    UniqueName="title"
                                    SortExpression="title"
                                    Visible="false"
                                    GroupByExpression="title Group By title"
                                    HeaderText="اسم الشهرة">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txt_nickname" runat="server" Text='<%# Bind("title") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_nickname" runat="server" Text='<%# Eval("title") %>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>

                                <telerik:GridTemplateColumn
                                    DataField="tel"
                                    UniqueName="tel"
                                    SortExpression="tel"
                                    Visible="false"
                                    GroupByExpression="tel Group By tel"
                                    HeaderText="التليفون">

                                    <EditItemTemplate>
                                        <asp:TextBox ID="txt_phone" runat="server" Text='<%# Bind("tel") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_phone" runat="server" Text='<%# Eval("tel") %>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>

                                <telerik:GridTemplateColumn
                                    DataField="tel2"
                                    UniqueName="tel2"
                                    SortExpression="tel2"
                                    Visible="false"
                                    GroupByExpression="tel2 GROUP BY tel2"
                                    HeaderText="تليفون إضافي">

                                    <EditItemTemplate>
                                        <asp:TextBox ID="txt_phone_2" runat="server" Text='<%# Bind("tel2") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_phone_2" runat="server" Text='<%# Eval("tel2") %>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>

                                <telerik:GridTemplateColumn
                                    DataField="religion"
                                    UniqueName="religion"
                                    SortExpression="religion"
                                    Visible="false"
                                    GroupByExpression="religion GROUP BY religion"
                                    HeaderText="الديانة">

                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlReligion" runat="server" CssClass="form-control btn-sm">
                                            <asp:ListItem Value="">إختر...</asp:ListItem>
                                            <asp:ListItem Value="1">مسلم</asp:ListItem>
                                            <asp:ListItem Value="0">مسيحي</asp:ListItem>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblReligion" runat="server" Text='<%# Eval("religion") %>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>

                                <telerik:GridTemplateColumn
                                    DataField="status"
                                    UniqueName="status"
                                    SortExpression="status"
                                    GroupByExpression="status GROUP BY status"
                                    HeaderText="حالة العمل">

                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlIsActive" runat="server" CssClass="form-control btn-sm">
                                            <asp:ListItem Value="">إختر...</asp:ListItem>
                                            <asp:ListItem Value="يعمل">يعمل</asp:ListItem>
                                            <asp:ListItem Value="موقوف">موقوف</asp:ListItem>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblIsActive" runat="server" Text='<%# Eval("status") %>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>

                            </Columns>

                        </MasterTableView>

                        <GroupingSettings CollapseAllTooltip="Collapse all groups" />

                        <ValidationSettings EnableModelValidation="false" EnableValidation="false" ValidationGroup="10" />

                        <ClientSettings AllowColumnsReorder="True" AllowDragToGroup="True" ReorderColumnsOnClient="True">
                            <Selecting AllowRowSelect="True" />
                            <Scrolling UseStaticHeaders="True" />
                        </ClientSettings>

                        <FilterMenu EnableImageSprites="False">
                        </FilterMenu>

                    </telerik:RadGrid>
                </telerik:RadAjaxPanel>

                <telerik:RadNotification ID="RadNotification1" runat="server" Position="Center" Skin="Bootstrap" Font-Bold="True" Font-Names="Arial" Font-Size="15pt">
                    <NotificationMenu ID="TitleMenu">
                    </NotificationMenu>
                </telerik:RadNotification>

            </div>
        </div>

    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadSection" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="JsScripts" runat="Server">
    <script src="../../Content/js/modules/eg/crm_plumbers.js"></script>
    <script type="text/javascript">
        //<![CDATA[
        serverID("ajaxManagerID", "<%= RadAjaxManager1.ClientID %>");
        //]]>
    </script>
</asp:Content>
