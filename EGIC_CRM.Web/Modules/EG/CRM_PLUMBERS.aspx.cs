﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class Modules_EG_CRM_PLUMBERS : BasePage
{

    #region fields

    private readonly LookupService _lookupService;
    private readonly EGService _egService;
    DataSet Ds = new DataSet();

    #endregion

    #region CTOR

    public Modules_EG_CRM_PLUMBERS()
    {
        _lookupService = new LookupService();
        _egService = new EGService();
    }

    #endregion

    #region Properties

    public string CODE { get { return hidden_code.Value; } }
    public string PlumberName { get { return txt_plumber_name.Text; } }
    public string PlumberMobile { get { return txt_plumber_mobile.Text; } }
    public string PlumberID { get { return txt_plumber_ID.Text; } }
    public string PlumberAddress { get { return txt_home_address.Text; } }
    public string PlumberPhone { get { return txt_plumber_phone.Text; } }

    public string PlumberNickname { get { return txt_plumber_nickname.Text; } }
    public string PlumberPhoneII { get { return txt_plumber_phone_2.Text; } }
    public string PlumberPlace { get { return txt_plumber_place.Text; } }
    public string PlumberBirthdate { get { return txt_plumber_birthdate.SelectedDate!=null? txt_plumber_birthdate.SelectedDate.Value.ToShortDateString():""; } }
    public string PlumberBirthGovCode { get { return ddl_birth_gov.SelectedValue; } }
    public string PlumberBirthCityCode { get { return ddl_birth_gov.SelectedValue; } }
    public string PlumberBirthDistCode { get { return ddl_birth_gov.SelectedValue; } }
    public string PlumberNationality { get { return txt_plumber_nationality.Text; } }
    public string PlumberGender { get { return ddl_gender.SelectedValue; } }

    public string PlumberDeactivationResion { get { return txt_deactivation_reason.Text; } }
    public string PlumberSalesRep { get { return txt_sales_rep.Text; } }

    public string PlumberBranch { get { return ddl_branch.SelectedValue; } }
    public string PlumberShopCode { get { return ddl_work_shop.SelectedValue; } }
    public string PlumberWorkGovCode { get { return ddl_work_gov.SelectedValue; } }
    public string PlumberWorkCityCode { get { return ddl_work_city.SelectedValue; } }
    public string PlumberWorkDistCode { get { return ddl_work_dist.SelectedValue; } }
    public string PlumberReligion { get { return ddl_religion.SelectedValue; } }
    public string PlumberIsActive { get { return ddl_isActive.SelectedValue; } }
    public string PlumberGroupManagerCode { get { return ddl_group_manager.SelectedValue; } }
    public string PlumberGroupCode { get { return ddl_group_name.SelectedValue; } }
    public string PlumberInspectionRepCode { get { return ddl_inspection_rep.SelectedValue; } }

    #endregion

    #region Utilities

    private void ShowErrorMessages(List<string> errors)
    {
        if (errors.Count > 0)
        {
            lblErrorMessage.Visible = true;
            lblErrorMessage.InnerText = string.Join(Environment.NewLine, errors);
            return;
        }
    }

    private void ShowErrorMessages(string error)
    {
        if (!string.IsNullOrWhiteSpace(error))
        {
            lblErrorMessage.Visible = true;
            lblErrorMessage.InnerText = string.Join(Environment.NewLine, error);
            return;
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            // fill ddl_branch
            var dataBarnches = _lookupService.GetAllBranches();
            ddl_branch.FillDataSource(dataBarnches);

            // fill ddl_work_gov
            var dataGovernorates = _lookupService.GetAllGovernorates();
            ddl_work_gov.FillDataSource(dataGovernorates);
            ddl_birth_gov.FillDataSource(dataGovernorates);

            // fill ddl_work_gov
            //var dataShops = _lookupService.GetAllShops();
            //ddl_work_shop.FillDataSource(dataShops);

            // fill ddl_group_name
            var dataGroups = _lookupService.GetAllGroups();
            ddl_group_name.FillDataSource(dataGroups);

            FillGrid();

            txt_rec_date.Text = DateTime.Now.ToShortDateString();
            //txt_creator_name.Text = Session[AppConsts.CURRENT_USERNAME].ToString();
            txt_plumber_birthdate.MinDate = DateTime.Now.AddYears(-100);


        }
    }

    private void FillGrid(bool rebind = true)
    {
        if (!int.TryParse(CODE, out int _code))
            _code = 0;

        RadGrid1.DataSource = _egService.GetAllPlumbersData(
            pageSize: RadGrid1.PageSize,
            code: _code,
            plumberName: PlumberName,
            plumberMobile: PlumberMobile,
            plumberID: PlumberID,
            plumberAddress: PlumberAddress,
            plumberPhone: PlumberPhone,
            plumberPhoneII: PlumberPhoneII,
            plumberNickname: PlumberNickname,
            plumberPlace: PlumberPlace,
            plumberBirthdate: PlumberBirthdate,
            plumberDeactivationResion: PlumberDeactivationResion,
            plumberSalesRep: PlumberSalesRep,
            plumberBranch: PlumberBranch,
            plumberShopCode: PlumberShopCode,
            plumberWorkGovCode: PlumberWorkGovCode,
            plumberWorkCityCode: PlumberWorkCityCode,
            plumberWorkDistCode: PlumberWorkDistCode,
            plumberReligion: PlumberReligion,
            plumberIsActive: PlumberIsActive,
            plumberGroupManagerCode: PlumberGroupManagerCode,
            plumberGroupCode: PlumberGroupCode,
            plumberInspectionRepCode: PlumberInspectionRepCode
            );

        if (rebind)
            RadGrid1.DataBind();

        RadGrid1.Focus();
    }

    protected void RadGrid1_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        lblErrorMessage.Visible = false;
        if (e.CommandName == "UPDATE")
        {
            try
            {
                GridDataItem item = (GridDataItem)e.Item;
                Label lbl_code = (Label)item.FindControl("lbl_code");

                if (!string.IsNullOrWhiteSpace(lbl_code.Text) && int.TryParse(lbl_code.Text, out int _code) && _code > 0)
                {
                    var plumberData = _egService.GetPlumberDataByCode(_code);
                    if (plumberData != null && plumberData.Rows.Count > 0)
                    {
                        var plumber = plumberData.Rows[0];
                        hidden_code.Value = lbl_code.Text;
                        txt_plumber_name.Text = plumber["name"].ToString();
                        txt_plumber_mobile.Text = plumber["mobile"].ToString();
                        txt_plumber_ID.Text = plumber["id_no"].ToString();
                        txt_home_address.Text = plumber["home_address"].ToString();
                        txt_plumber_place.Text = plumber["work_address"].ToString();

                        string birthDate = plumber["birth_date"].ToString();
                        if(!string.IsNullOrWhiteSpace(birthDate)&& DateTime.TryParse(birthDate, out DateTime _birthDate))
                            txt_plumber_birthdate.SelectedDate = _birthDate;

                        txt_plumber_nickname.Text = plumber["title"].ToString();
                        txt_plumber_phone.Text = plumber["tel"].ToString();
                        txt_plumber_phone_2.Text = plumber["tel2"].ToString();
                        txt_creator_name.Text = plumber["user_name"].ToString();
                        txt_rec_date.Text = plumber["rec_date"].ToString();
                        txt_sales_rep.Text = plumber["sales_rep_name"].ToString();
                        txt_deactivation_reason.Text = plumber["reason"].ToString();
                        txt_balance.Text = plumber["balance"].ToString();
                        txt_total_points.Text = plumber["total_points"].ToString();

                        //try
                        //{
                        //    string jsonPhoto = JsonConvert.SerializeObject(plumber["photo"], Formatting.Indented);
                            
                        //    if (!string.IsNullOrWhiteSpace(jsonPhoto))
                        //    {
                        //        var photo = Convert.FromBase64String(jsonPhoto);
                        //        //imgPlumberID.DataValue = photo;

                        //        //Stream image = (Stream)photo;
                        //        //using (Stream stream = image)
                        //        //{
                        //        //    byte[] imgData = new byte[stream.Length];
                        //        //    imgPlumberID.DataValue = imgData;
                        //        //}
                        //    }
                            
                        //}
                        //catch (Exception ex)
                        //{
                        //    string ee = ex.Message;
                        //}

                        ddl_branch.SetSelectedValue(plumber["BRANCH"]);

                        ddl_religion.SetSelectedValue(plumber["religion"]);
                        ddl_isActive.SetSelectedValue(plumber["status"]);

                        if (int.TryParse(plumber["GROUP_CODE"].ToString(), out int groupCode) && groupCode > 0)
                        {
                            ddl_group_name.SetSelectedValue(groupCode);
                            var dataGroupManagers = _lookupService.GetAllGroupsManagers(groupCode);
                            ddl_group_manager.FillDataSource(dataGroupManagers);

                            if (int.TryParse(plumber["MANG_NO"].ToString(), out int managerCode) && managerCode > 0)
                            {
                                ddl_group_manager.SetSelectedValue(managerCode);
                                var dataReps = _lookupService.GetAllInspectionReps(managerCode);
                                ddl_inspection_rep.FillDataSource(dataReps);
                                ddl_inspection_rep.SetSelectedValue(plumber["REP_NO"]);
                            }
                        }

                        if (int.TryParse(plumber["GOV_CODE"].ToString(), out int govCode) && govCode > 0)
                        {
                            ddl_work_gov.SetSelectedValue(govCode);
                            var dataCities = _lookupService.GetAllCities(govCode);
                            ddl_work_city.FillDataSource(dataCities);
                            if (int.TryParse(plumber["CITY_CODE"].ToString(), out int cityCode))
                            {
                                ddl_work_city.SetSelectedValue(cityCode);
                                var dataDistricts = _lookupService.GetAllDistricts(cityCode);
                                ddl_work_dist.FillDataSource(dataDistricts);
                                int.TryParse(plumber["DIST_CODE"].ToString(), out int _distCode);
                                ddl_work_dist.SetSelectedValue(_distCode);
                                if (int.TryParse(plumber["SHOP_CODE"].ToString(), out int _shopCode) && _shopCode > 0)
                                {
                                    var dataShops = _lookupService.GetAllShops(shopCode: _shopCode, governorateCode: govCode, cityCode: cityCode, districtCode: _distCode);
                                    ddl_work_shop.FillDataSource(dataShops);
                                    ddl_work_shop.SetSelectedValue(_shopCode);
                                }
                            }
                        }
                        txt_plumber_name.Focus();

                    }
                }
            }
            catch (Exception ex)
            {
                lblErrorMessage.InnerText = ex.Message;
                lblErrorMessage.Visible = true;
            }

        }
        else if (e.CommandName == "DELETE")
        {
            GridDataItem item = e.Item as GridDataItem;
            Label lblCode = (Label)item.FindControl("lbl_code");

            bool deleted = _egService.DeletePlumber(lblCode.Text);
            if (deleted)
            {
                FillGrid();
                lblSuccessMessage.InnerText = "تم حذف البيانات بنجاح";
                lblSuccessMessage.Visible = true;
            }
            else
            {
                lblErrorMessage.InnerText = "يوجد مشكله في عملية الحذف";
                lblErrorMessage.Visible = true;
            }
        }
    }

    protected void RadGrid1_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        FillGrid(false);
    }

    protected void RadGrid1_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {

    }

    protected void btnUploadImg_Click(object sender, EventArgs e)
    {
        //if (filePlumberID.HasFile)
        //{
        //    try
        //    {
        //        if (filePlumberID.PostedFile.ContentType == "image/jpeg")
        //        {
        //            if (filePlumberID.PostedFile.ContentLength > 502400)
        //            {
        //                string file_name = filePlumberID.FileName.ToString();
        //                string path = filePlumberID.PostedFile.FileName.ToString();
        //                //*********************
        //                byte[] bytearr = new byte[filePlumberID.PostedFile.InputStream.Length + 1];
        //                filePlumberID.PostedFile.InputStream.Read(bytearr, 0, bytearr.Length);
        //                //************************

        //                com = new SqlCommand(" Insert Into  SendingProblems  ( Emp_Code, ProbType_Code, PCProgName, Problem_Desc, Attached_file , Record_Date, Status ,File_Path ,File_Name ,Start_date ,  Ip_Address, Computer_Name, Loged_User, Host_Name ) Values ( '" + MYIDD + "','" + typeID + "','" + txtprogname.Text + "',@DESC,@file1,Getdate(),'00','" + path + "','" + file_name + "',Getdate(),'" + Session["PC_IP"].ToString() + "','" + Session["PC_Name"].ToString() + "','" + Session["LOGED_User"].ToString() + "','" + Session["Host_Name"].ToString() + "' )  ", con);
        //                com.Parameters.AddWithValue("@file1", bytearr);
        //            }
        //            else
        //                lblErrorMessage.InnerText = "الصورة المرفوعه اكبر من 500 كيلو بايت";
        //        }
        //        else
        //            lblErrorMessage.InnerText = "Upload status: Only JPEG files are accepted!";
        //    }
        //    catch (Exception ex)
        //    {
        //        lblErrorMessage.InnerText = "حدث خطأ اثناء الرفع: " + ex.Message;
        //    }
        //}
    }

    protected void btn_save_Click(object sender, EventArgs e)
    {
        List<string> errorMessages = new List<string>();
        if (string.IsNullOrWhiteSpace(PlumberName))
            errorMessages.Add($"يجب ادخال اسم السباك");
        if (string.IsNullOrWhiteSpace(PlumberMobile))
            errorMessages.Add($"يجب ادخال موبايل السباك");
        if (string.IsNullOrWhiteSpace(PlumberID))
            errorMessages.Add($"يجب ادخال رقم بطاقة السباك");
        if (string.IsNullOrWhiteSpace(PlumberAddress))
            errorMessages.Add($"يجب ادخال عنوان منزل السباك");
        if (!string.IsNullOrWhiteSpace(PlumberIsActive) && PlumberIsActive.Equals("موقوف") && string.IsNullOrWhiteSpace(PlumberDeactivationResion))
            errorMessages.Add($"يجب ادخال سبب وقف السباك");
        ShowErrorMessages(errorMessages);

        //var img = Context.Cache.Get(Session.SessionID + "UploadedFile");
        //if (img != null)
        //{
        //    Stream image = (Stream)img;
        //    using (Stream stream = image)
        //    {
        //        byte[] imgData = new byte[stream.Length];
        //        imgPlumberID.DataValue = imgData;
        //    }
        //}
        bool success =
            _egService.SavePlumberData(out string errorMessage,
            code: CODE,
            name: PlumberName,
            mobile: PlumberMobile,
            id_no: PlumberID,
            home_address: PlumberAddress,
            work_address: PlumberPlace,
            tel: PlumberPhone,
            tel2: PlumberPhoneII,
            gov_code: PlumberWorkGovCode,
            city_code: PlumberWorkCityCode,
            dist_code: PlumberWorkDistCode,
            shop_code: PlumberShopCode,
            branch: PlumberBranch,
            birth_date: PlumberBirthdate,
            religion: PlumberReligion,
            status: PlumberIsActive,
            reason: PlumberDeactivationResion,
            group_code: PlumberGroupCode,
            rep_no: PlumberInspectionRepCode,
            mang_no: PlumberGroupManagerCode,
            nickname: PlumberNickname,
            birth_gov: PlumberBirthGovCode,
            birth_city: PlumberBirthCityCode,
            birth_lov: PlumberBirthDistCode,
            feast_gov: string.Empty,
            gender:PlumberGender,
            nationality:PlumberNationality,
            photo: null,
            currentUsername: CurrentUsername
            );
        if (!success)
            ShowErrorMessages($"فشل في الحفظ: {Environment.NewLine}({errorMessage})");
        else
        {
            lblSuccessMessage.InnerText = "تم الحفظ بنجاح";
            lblSuccessMessage.Visible = true;
            ResetForm();
        }
    }

    protected void ddl_birth_gov_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (int.TryParse(ddl_birth_gov.SelectedValue, out int governorateCode) && governorateCode > 0)
        {
            var items = _lookupService.GetAllCities(governorateCode);
            ddl_birth_city.FillDataSource(items);
        }
    }

    protected void ddl_birth_city_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (int.TryParse(ddl_birth_city.SelectedValue, out int cityCode) && cityCode > 0)
        {
            var items = _lookupService.GetAllDistricts(cityCode);
            ddl_birth_dist.FillDataSource(items);
        }
    }

    protected void ddl_work_gov_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (int.TryParse(ddl_work_gov.SelectedValue, out int governorateCode) && governorateCode > 0)
        {
            var items = _lookupService.GetAllCities(governorateCode);
            ddl_work_city.FillDataSource(items);
        }
    }

    protected void ddl_work_city_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (int.TryParse(ddl_work_city.SelectedValue, out int cityCode) && cityCode > 0)
        {
            var items = _lookupService.GetAllDistricts(cityCode);
            ddl_work_dist.FillDataSource(items);
        }
    }

    protected void ddl_work_dist_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (int.TryParse(ddl_work_dist.SelectedValue, out int distCode) && distCode > 0)
        {
            var items = _lookupService.GetAllShops(districtCode:distCode);
            ddl_work_shop.FillDataSource(items);
        }
    }

    protected void ddl_group_name_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (int.TryParse(ddl_group_name.SelectedValue, out int groupCode) && groupCode > 0)
        {
            var items = _lookupService.GetAllGroupsManagers(groupCode);
            ddl_group_manager.FillDataSource(items);
            txt_sales_rep.Text = _lookupService.GetSalesRepName(groupCode);
        }
    }

    protected void ddl_group_manager_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (int.TryParse(ddl_group_manager.SelectedValue, out int managerCode) && managerCode > 0)
        {
            var items = _lookupService.GetAllInspectionReps(managerCode);
            ddl_inspection_rep.FillDataSource(items);
        }
    }

    protected void filePlumberID_FileUploaded(object sender, FileUploadedEventArgs e)
    {
        //Clear changes and remove uploaded image from Cache
        
        Context.Cache.Remove(Session.SessionID + "UploadedFile");
        using (Stream stream = e.File.InputStream)
        {
            byte[] imgData = new byte[stream.Length];
            stream.Read(imgData, 0, imgData.Length);
            MemoryStream ms = new MemoryStream();
            ms.Write(imgData, 0, imgData.Length);

            //imgPlumberID.DataValue = imgData;
            Context.Cache.Insert(Session.SessionID + "UploadedFile", ms, null, DateTime.Now.AddMinutes(20), TimeSpan.Zero);
        }
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        ResetForm();
        lblErrorMessage.Visible = false;
        lblSuccessMessage.Visible = false;
    }

    private void ResetForm()
    {
        hidden_code.Value =
            txt_plumber_name.Text =
            txt_plumber_mobile.Text =
            txt_plumber_ID.Text =
            txt_home_address.Text =
            txt_plumber_place.Text =
            txt_plumber_nickname.Text =
            txt_plumber_phone.Text =
            txt_plumber_phone_2.Text =
            txt_creator_name.Text =
            txt_rec_date.Text =
            txt_sales_rep.Text =
            txt_deactivation_reason.Text =
            txt_balance.Text =
            txt_total_points.Text = string.Empty;

        txt_plumber_birthdate.SelectedDate = null;
        //imgPlumberID.DataValue = null;
        //imgPlumberID.ImageUrl = null;

        ddl_branch.SetSelectedValue(string.Empty);
        ddl_religion.SetSelectedValue(string.Empty);
        ddl_isActive.SetSelectedValue(string.Empty);
        ddl_work_gov.SetSelectedValue(string.Empty);
        ddl_group_name.SetSelectedValue(string.Empty);

        ddl_group_manager.FillDataSource(null);
        ddl_inspection_rep.FillDataSource(null);
        ddl_work_city.FillDataSource(null);
        ddl_work_dist.FillDataSource(null);
        ddl_work_shop.FillDataSource(null);

       

        FillGrid();
        RadGrid1.Focus();
    }
}