﻿<%@ Page Title="تعريف السباكين" Language="C#" AutoEventWireup="true" CodeFile="CRM_PLUMBERS2.aspx.cs" Inherits="Modules_EG_CRM_PLUMBERS2" MasterPageFile="~/Main.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>

            <telerik:AjaxSetting AjaxControlID="ddl_work_gov">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ddl_work_city" />
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="ddl_work_city">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ddl_work_dist" />
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="ddl_group_name">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="txt_sales_rep" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_group_manager" />
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="ddl_group_manager">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_rep" />
                </UpdatedControls>
            </telerik:AjaxSetting>


            <telerik:AjaxSetting AjaxControlID="btn_save">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ddl_religion" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_isActive" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_branch" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_work_shop" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_work_gov" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_work_city" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_work_dist" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_group_name" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_group_manager" />
                    <telerik:AjaxUpdatedControl ControlID="ddl_inspection_rep" />

                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_name" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_mobile" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_ID" />
                    <telerik:AjaxUpdatedControl ControlID="txt_home_address" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_nickname" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_phone" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_phone_2" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_birthdate" />
                    <telerik:AjaxUpdatedControl ControlID="txt_deactivation_resion" />
                    <telerik:AjaxUpdatedControl ControlID="txt_plumber_place" />
                    <telerik:AjaxUpdatedControl ControlID="txt_sales_rep" />

                    <telerik:AjaxUpdatedControl ControlID="lblErrorMessage" />
                    <telerik:AjaxUpdatedControl ControlID="lblSuccessMessage" />
                </UpdatedControls>
            </telerik:AjaxSetting>

        </AjaxSettings>
    </telerik:RadAjaxManager>
    <div class="raw clearfix">
            <div class="box box-danger collapsed-box" id="insert-form">
                <div class="box-header with-border">
                    <div class="box-title">
                        إدخال بيانات السباك
                    </div>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body" style="display:none;">

                    <div class="col-md-12">

                        <div class="col-md-3">
                            <div class="form-group form-group-sm">
                                <label for="txt_plumber_name">اسم السباك</label>
                                <span class="danger text-bold">* </span>
                                <asp:RequiredFieldValidator runat="server" ErrorMessage=" اسم السباك مطلوب" ForeColor="Red" ControlToValidate="txt_plumber_name"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txt_plumber_name" runat="server" CssClass="form-control" placeholder="اسم السباك (مطلوب)"></asp:TextBox>
                                <asp:HiddenField ID="hidden_code" runat="server" />
                            </div>

                            <div class="form-group form-group-sm">
                                <label for="txt_plumber_mobile">الموبايل</label>
                                <span class="danger text-bold">* </span>
                                <asp:RequiredFieldValidator runat="server" ErrorMessage=" موبايل السباك مطلوب" ForeColor="Red" ControlToValidate="txt_plumber_mobile"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txt_plumber_mobile" runat="server" TextMode="Phone" CssClass="form-control" placeholder="الموبايل (مطلوب)"></asp:TextBox>
                            </div>

                            <div class="form-group form-group-sm">
                                <label for="txt_plumber_ID">رقم البطاقة</label>
                                <span class="danger text-bold">* </span>
                                <asp:RequiredFieldValidator runat="server" ErrorMessage=" رقم بطاقة السباك مطلوبه" ForeColor="Red" ControlToValidate="txt_plumber_ID"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txt_plumber_ID" runat="server" TextMode="Number" CssClass="form-control" placeholder="رقم البطاقة (مطلوب)"></asp:TextBox>
                            </div>

                            <div class="form-group form-group-sm">
                                <label for="txt_home_address">عنوان المنزل</label>
                                <span class="danger text-bold">* </span>
                                <asp:RequiredFieldValidator runat="server" ErrorMessage=" عنوان منزل السباك مطلوب" ForeColor="Red" ControlToValidate="txt_home_address"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txt_home_address" runat="server" CssClass="form-control" placeholder="عنوان المنزل (مطلوب)"></asp:TextBox>
                            </div>

                        </div>
                        <div class="col-md-3">

                            <div class="form-group form-group-sm">
                                <label for="txt_plumber_nickname">اسم الشهرة</label>
                                <asp:TextBox ID="txt_plumber_nickname" runat="server" CssClass="form-control" placeholder="اسم الشهرة"></asp:TextBox>
                            </div>

                            <div class="form-group form-group-sm">
                                <label for="txt_plumber_phone">التليفون</label>
                                <asp:TextBox ID="txt_plumber_phone" runat="server" TextMode="Phone" CssClass="form-control" placeholder="التليفون"></asp:TextBox>
                            </div>

                            <div class="form-group form-group-sm">
                                <label for="txt_plumber_phone_2">تليفون إضافي</label>
                                <asp:TextBox ID="txt_plumber_phone_2" runat="server" TextMode="Phone" CssClass="form-control" placeholder="تليفون إضافي"></asp:TextBox>
                            </div>

                            <div class="form-group form-group-sm" style="display:none;">
                                <label for="txt_plumber_birthdate">تاريخ الميلاد</label>
                                <asp:TextBox ID="txt_plumber_birthdate" runat="server" TextMode="Date" CssClass="form-control" placeholder="تاريخ الميلاد"></asp:TextBox>
                            </div>

                        </div>
                        <div class="col-md-3">


                            <div class="form-group form-group-sm">
                                <label for="ddl_religion">الديانة</label>
                                <asp:DropDownList ID="ddl_religion" runat="server" CssClass="form-control btn-sm">
                                    <asp:ListItem Value="">إختر...</asp:ListItem>
                                    <asp:ListItem Value="1">مسلم</asp:ListItem>
                                    <asp:ListItem Value="0">مسيحي</asp:ListItem>
                                </asp:DropDownList>
                            </div>

                            <div class="form-group form-group-sm">
                                <label for="ddl_isActive">حالة العمل</label>
                                <asp:DropDownList ID="ddl_isActive" runat="server" CssClass="form-control btn-sm">
                                    <asp:ListItem Value="">إختر...</asp:ListItem>
                                    <asp:ListItem Value="يعمل">يعمل</asp:ListItem>
                                    <asp:ListItem Value="موقوف">موقوف</asp:ListItem>
                                </asp:DropDownList>
                            </div>

                            <div class="form-group form-group-sm" id="div_deactivation_resion">
                                <label for="txt_deactivation_resion">سبب الوقف</label>
                                <asp:TextBox ID="txt_deactivation_resion" runat="server" TextMode="MultiLine" Rows="5" CssClass="form-control" placeholder="سبب الوقف"></asp:TextBox>
                            </div>

                        </div>
                        <div class="col-md-3">
                            <div class="form-group form-group-sm">
                                <label for="filePlumberID">صورة البطاقة</label>
                                <input 
                                    class="my-pond" 
                                    type="file" 
                                    name="filepond" 
                                    data-max-file-size="500KB"
                                    accept="image/png, image/jpeg, image/gif"
                                    />
                            </div>
                        </div>

                    </div>


                    <div class="col-md-12">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_1" data-toggle="tab">بيانات العمل ومكتب خدمة العملاء التابع له العميل
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab_2" data-toggle="tab">النقاط والرصيد
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab_3" data-toggle="tab">التسجيل
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="active tab-pane" id="tab_1">

                                    <div class="raw clearfix">
                                        <div class="col-md-3">
                                            <div class="form-group form-group-sm">
                                                <label for="ddl_branch">الفرع</label>
                                                <asp:DropDownList ID="ddl_branch" runat="server" CssClass="form-control btn-sm">
                                                    <asp:ListItem Value="">إختر...</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>


                                            <div class="form-group form-group-sm">
                                                <label for="ddl_work_shop">المحل</label>
                                                <asp:DropDownList ID="ddl_work_shop" runat="server" Width="100%" CssClass="form-control btn-sm btn-block select2">
                                                    <asp:ListItem Value="">إختر...</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <div class="form-group form-group-sm">
                                                <label for="txt_plumber_place">مكان التواجد</label>
                                                <asp:TextBox ID="txt_plumber_place" runat="server" CssClass="form-control" placeholder="مكان التواجد"></asp:TextBox>
                                            </div>

                                        </div>
                                        <div class="col-md-3">

                                            <div class="form-group form-group-sm">
                                                <label for="ddl_work_gov">المحافظة</label>
                                                <asp:DropDownList ID="ddl_work_gov" runat="server" CssClass="form-control btn-sm" OnSelectedIndexChanged="ddl_work_gov_SelectedIndexChanged" AutoPostBack="true">
                                                    <asp:ListItem Value="">إختر...</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <div class="form-group form-group-sm">
                                                <label for="ddl_work_city">الحي</label>
                                                <asp:DropDownList ID="ddl_work_city" runat="server" Width="100%" CssClass="form-control btn-sm btn-block select2" OnSelectedIndexChanged="ddl_work_city_SelectedIndexChanged" AutoPostBack="true">
                                                    <asp:ListItem Value="">إختر...</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <div class="form-group form-group-sm">
                                                <label for="ddl_work_dist">المنطقة</label>
                                                <asp:DropDownList ID="ddl_work_dist" runat="server" Width="100%" CssClass="form-control btn-sm btn-block select2">
                                                    <asp:ListItem Value="">إختر...</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group form-group-sm">
                                                <label for="ddl_group_name">اسم المجموعة</label>
                                                <asp:DropDownList ID="ddl_group_name" runat="server" Width="100%" CssClass="form-control btn-sm btn-block select2" OnSelectedIndexChanged="ddl_group_name_SelectedIndexChanged" AutoPostBack="true">
                                                    <asp:ListItem Value="">إختر...</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <div class="form-group form-group-sm">
                                                <label for="txt_sales_rep">مندوب البيع</label>
                                                <asp:TextBox ID="txt_sales_rep" runat="server" CssClass="form-control" placeholder="مندوب البيع" ReadOnly="true" Enabled="false"></asp:TextBox>
                                            </div>

                                        </div>
                                        <div class="col-md-3">

                                            <div class="form-group form-group-sm">
                                                <label for="ddl_group_manager">رئيس المجموعة</label>
                                                <asp:DropDownList ID="ddl_group_manager" runat="server" CssClass="form-control btn-sm" OnSelectedIndexChanged="ddl_group_manager_SelectedIndexChanged" AutoPostBack="true">
                                                    <asp:ListItem Value="">إختر...</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>


                                            <div class="form-group form-group-sm">
                                                <label for="ddl_inspection_rep">مندوب المتابعة</label>
                                                <asp:DropDownList ID="ddl_inspection_rep" runat="server" CssClass="form-control btn-sm">
                                                    <asp:ListItem Value="">إختر...</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <div class="tab-pane" id="tab_2">
                                    <div class="raw clearfix">
                                        <div class="col-md-3">
                                            <div class="form-group form-group-sm">
                                                <label for="txt_total_points">إجمالي النقاط</label>
                                                <asp:TextBox ID="txt_total_points" runat="server" CssClass="form-control" placeholder="إجمالي النقاط" ReadOnly="true" Enabled="false"></asp:TextBox>
                                            </div>

                                            <div class="form-group form-group-sm">
                                                <label for="txt_balance">الرصيد</label>
                                                <asp:TextBox ID="txt_balance" runat="server" CssClass="form-control" placeholder="الرصيد" ReadOnly="true" Enabled="false"></asp:TextBox>
                                            </div>

                                            <div class="form-group form-group-sm">
                                                <label for="txt_inspection_days_count">عدد أيام عرض المعايانات</label>
                                                <asp:TextBox ID="txt_inspection_days_count" runat="server" CssClass="form-control" placeholder="عدد أيام عرض المعايانات" ReadOnly="true" Enabled="false"></asp:TextBox>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab_3">
                                    <div class="raw clearfix">
                                        <div class="col-md-3">
                                            <div class="form-group form-group-sm">
                                                <label for="txt_creator_name">إسم المستخدم</label>
                                                <asp:TextBox ID="txt_creator_name" runat="server" CssClass="form-control" placeholder="إسم المستخدم" ReadOnly="true" Enabled="false"></asp:TextBox>
                                            </div>
                                            <div class="form-group form-group-sm">
                                                <label for="txt_rec_date">تاريخ التسجيل</label>
                                                <asp:TextBox ID="txt_rec_date" runat="server" CssClass="form-control" placeholder="تاريخ التسجيل" ReadOnly="true" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer" style="display: none;">
                    <asp:Button ID="btn_save" runat="server" Text="حفظ" CssClass="btn btn-primary" OnClick="btn_save_Click" />
                    <button type="button" class="btn" id="btnCancelSave">إلغاء</button>
                </div>
            </div>
        <p class="alert alert-danger" runat="server" id="lblErrorMessage" visible="false"></p>
        <div class="alert alert-success alert-dismissable" runat="server" id="lblSuccessMessage" visible="false">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            تم الحفظ بنجاح
        </div>
        

        <div class="box box-danger">
            <div class="box-header with-border">
                <h5>
                    بيانات السباكين
                </h5>
            </div>
            <div class="box-body">
                <div class="col-md-4 show-after-page-load" style="padding-bottom: 10px; display: none;">
                    <div class="form-group form-group-sm ">
                        <label for="columnchooser">بحث بواسطة</label>
                        <div id='columnchooser'>
                        </div>
                    </div>
                    <div class="form-group form-group-sm ">
                        <input type="text" id="txtSearch" class="form-control">
                    </div>
                    <button type="button" class="btn-flat" id="applyfilter">تصفية</button>
                    <button type="button" class="btn-flat" id="clearfilter">إلغاء</button>
                </div>
                <div id='jqxWidget'>
                    <div id="grid">
                    </div>
                    <div class="show-after-page-load" style='margin-top: 20px; display: none;'>
                        <div style='margin-left: 10px; float: left;'>
                            <input type="button" value="طباعة" id='print' />
                        </div>
                    </div>
                    <div id="jqxwindow" class="" style="display: none;">
                        <div id="customWindowHeader">
                            <span id="captureContainer" style="float: right">اجراء بحث
                            </span>
                        </div>
                        <div id="customWindowContent" style="overflow: hidden">
                            <div style="margin: 10px; text-align: right;">
                                بحث بواسطة
                                <div id='dropdownlist' style="width: 175px; height: 20px; border: 1px solid #aaa" class="jqx-input form-control input-sm">
                                </div>
                                <br />
                                <input id='inputField' type="text" placeholder="كلمة البحث" class="jqx-input form-control input-sm" style="width: 200px; height: 23px;" />
                                <br />
                                <div style="float: left">
                                    <input value="إلغاء" id="clearButton" style="margin-bottom: 5px;" />
                                    <input value="تصفية" id="findButton" />
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>

    <div id="confirmation-window" style="display: none;">
        <div>
            <i class="fa fa-question-circle"></i>
            رسالة تأكيد
        </div>
        <div>
            <div style="text-align: right">
                هل انت متأكد من حذف هذا العنصر؟
            </div>
            <div>
                <div style="float: left; margin-top: 15px;">
                    <input type="button" id="cancel" value="إلغاء" />
                    <input type="button" id="ok" value="نعم" style="margin-right: 10px" />
                </div>
            </div>
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadSection" runat="Server">
    <link href="../../Content/css/file-upload/filepond.css" rel="stylesheet" />
    <link href="../../Content/css/file-upload/filepond-plugin-image-preview.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="JsScripts" runat="Server">

    <script src="../../Content/js/upload/filepond.min.js"></script>
    <script src="../../Content/js/upload/filepond-plugin-image-preview.min.js"></script>
    <script src="../../Content/js/upload/filepond.jquery.js"></script>
    <script src="../../Content/js/modules/eg/crm_plumbers_2.js"></script>

</asp:Content>
