﻿<%@ WebHandler Language="C#" Class="API" %>

using System;
using System.Web;
using System.Collections;
using Newtonsoft.Json;
using System.Collections.Generic;

public class API : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/json";
        string page = context.Request.QueryString["page"] ?? "";
        string plumberCode = context.Request.QueryString["plumberCode"] ?? "";
        string shopName = context.Request.QueryString["shopname"] ?? "";
        if (page.Equals("getAllPlumbers", StringComparison.InvariantCultureIgnoreCase))
        {
            EGService _egService = new EGService();
            var allPlumbers = _egService.GetAllPlumbersData();
            string json = JsonConvert.SerializeObject(allPlumbers, Formatting.Indented);
            context.Response.Write(json);
        }
        else if (page.Equals("getPlumber", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrWhiteSpace(plumberCode))
        {
            EGService _egService = new EGService();
            var plumber = _egService.GetPlumberDataByCode(plumberCode);
            string json = JsonConvert.SerializeObject(plumber, Formatting.Indented);
            context.Response.Write(json);
        }
        else if (page.Equals("deletePlumber", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrWhiteSpace(plumberCode))
        {
            EGService _egService = new EGService();
            _egService.DeletePlumber(plumberCode);
            context.Response.Write(true);
        }
        else if (page.Equals("GetShops", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrWhiteSpace(shopName))
        {
            LookupService _eService = new LookupService();
            var data = _eService.GetAllShops(shopName:shopName);
            string json = JsonConvert.SerializeObject(new { items = data }, Formatting.Indented);
            context.Response.Write(json);
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}