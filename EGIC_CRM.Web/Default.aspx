﻿<%@ Page Title="تسجيل دخول" Language="C#" MasterPageFile="~/Login.Master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    
<div class="login-box" style="margin: 0 auto 0 auto!important;">
  <div class="login-logo">
      <img src="Content/images/EGIC.jpg" style="width:100%"/>
    <div style="font-family:'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif!important;">
        <strong style="color: #725f58!important;">EGIC</strong> 
        <small style="color: #f28b2f!important;">CRM</small>
    </div>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg text-center">
        <strong>نظام إدارة خدمة العملاء</strong>
        <br />
        <br />
        تسجيل الدخول
    </p>

      <form runat="server">
          <p class="alert alert-danger" runat="server" ID="lblErrorMessage" visible="false"></p>
          <div class="form-group has-feedback">
              <asp:TextBox runat="server" ID="txtUsername" type="text" class="form-control" placeholder="اسم المستخدم"></asp:TextBox>
              <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
              <asp:TextBox runat="server" ID="txtPassword" type="Password" class="form-control" placeholder="كلمة المرور"></asp:TextBox>
              <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
              <div class="col-xs-4">
                  <asp:Button type="submit" runat="server" class="btn btn-success btn-block btn-flat" Text="دخول" ID="btnLogin" OnClick="btnLogin_Click" />
              </div>
              <!-- /.col -->
              <div class="col-xs-8">
              </div>
              <!-- /.col -->
          </div>
      </form>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

</asp:Content>
