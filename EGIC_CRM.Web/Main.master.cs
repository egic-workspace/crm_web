﻿using System;
using System.Collections.Generic;
using System.Linq;

public partial class Main : System.Web.UI.MasterPage
{
    private readonly UserService _userService;

    public Main()
    {
        _userService = new UserService();
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session[AppConsts.CURRENT_USERNAME] == null || string.IsNullOrWhiteSpace(Session[AppConsts.CURRENT_USERNAME].ToString()))
            Response.Redirect("~/Default");

        if (!this.IsPostBack)
        {
            lblUsername.Text = Session[AppConsts.CURRENT_USERNAME].ToString();
            var currentUrlParts = Request.Url.AbsolutePath.Split('/');
            string currentPageUrl = currentUrlParts[currentUrlParts.Length - 1].Replace(".aspx", "");
            Session[AppConsts.CURRENT_PAGE_URL] = currentPageUrl;
            var menuItems = new List<MainMenuItem>();
            if (Session[AppConsts.MAIN_MENU_ITEMS] == null)
            {
                string moduleName = "";
                var urlPathParts = Context.Request.Path.Split('/');
                for (int i = 0; i < urlPathParts.Length; i++)
                    if (urlPathParts[i].ToUpper() == "MODULES")
                    {
                        moduleName = urlPathParts[i + 1];
                        break;
                    }
                menuItems = _userService.GetUserMainMenuItems(Session[AppConsts.CURRENT_USERNAME].ToString(), moduleName);
                Session[AppConsts.MAIN_MENU_ITEMS] = menuItems;
            }
            else
                menuItems = (List<MainMenuItem>)Session[AppConsts.MAIN_MENU_ITEMS];

            //if (!menuItems.SelectMany(x => x.SubItems.Select(s => s.URL)).Contains(currentPageUrl))
            //    Response.Redirect("~/Home");
        }
    }

    protected void BtnLogout_Click(object sender, EventArgs e)
    {
        Session[AppConsts.CURRENT_USERNAME] = string.Empty;
        Session[AppConsts.MAIN_MENU_ITEMS] = null;
        Response.Redirect("~/Default");
    }
}