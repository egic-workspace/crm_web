﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Default2 : System.Web.UI.Page
{
    private readonly EGService _egService;
    DataSet Ds = new DataSet();

    public Default2()
    {
        _egService = new EGService();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Fill_Grid();
    }
    private void Fill_Grid()
    {
        try
        {

            Ds.Tables.Clear();
            Ds = _egService.Bind_Query_Grid();
            RadGrid1.DataSource = Ds;
            RadGrid1.DataBind();
        }
        catch
        {

        }
    }
}