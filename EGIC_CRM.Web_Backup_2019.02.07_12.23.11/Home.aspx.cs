﻿using System;
using System.Web.UI.WebControls;

public partial class Home : System.Web.UI.Page
{
    private readonly UserService _userService;

    public Home()
    { 
        _userService = new UserService();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            CurrentContext.MainMenuItems = null;
            // essam update
            ddlModuls.Items.Add(new ListItem("إختر النظام", ""));
            var userModules = _userService.GetUserModules(CurrentContext.CurrentUsername);
            foreach (var item in userModules)
                ddlModuls.Items.Add(new ListItem(item.Text, item.URL));
        }
    }

    protected void btnOpenUrl_Click(object sender, EventArgs e)
    {
        string selectedUrl = ddlModuls.SelectedValue;
        if (string.IsNullOrWhiteSpace(selectedUrl))
            return;
        Response.Redirect(string.Format("~/Modules/{0}", selectedUrl));
    }
}