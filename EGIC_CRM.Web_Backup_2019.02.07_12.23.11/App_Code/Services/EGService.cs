﻿using System;
using System.Data;

/// <summary>
/// Summary description for EGService
/// </summary>
public class EGService :BaseService
{
    #region CTOR

    public EGService():base()
    {
    }

    #endregion

    #region Methods

    public DataSet Bind_Query_Grid()
    {
        try
        {
            SQL = "select name , active , decode (active ,'Y' ,'نشط', 'N', 'غير نشط') as active_desc , decode (qry_type ,'C' ,'شكوي', 'Q', 'استفسار' , 'S' , 'اقتراح') as qry_type_desc , qry_type , serv_dept , decode (send_mail ,'Y' ,'تم الإرسال', 'N', 'لم يتم الإرسال') as sendM_desc , send_mail , comp_class from eg_query_types     ";
            return GetDataAsDataSet(SQL);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_Query_Grid >> Fill qury types to GridView");
        }
        return null;
    }

    #endregion
}