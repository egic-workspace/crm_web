﻿using System;
using System.Configuration;
using System.Data;
using System.IO;

/// <summary>
/// Summary description for BaseService
/// </summary>
public class BaseService
{

    #region Fields

    public readonly OracleAcess _oracleAcess;
    public string SQL = "";
    private DataSet Ds = new DataSet();
    private DataTable dt = new DataTable();

    #endregion

    #region CTOR

    public BaseService()
    {
        string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        _oracleAcess = new OracleAcess(connectionString);
    }

    #endregion

    #region Internal Functions

    public DataSet GetDataAsDataSet(string sqlQuery)
    {
        Ds.Clear();
        dt = _oracleAcess.ExecuteDataTable(sqlQuery);
        Ds.Tables.Add(dt);
        return Ds;
    }

    public DataTable GetDataAsDataTable(string sqlQuery)
    {
        return _oracleAcess.ExecuteDataTable(sqlQuery);
    }

    public void LogError(string msg, string functionName)
    {
        // StreamWriter sr = new StreamWriter(@"C:\Inetpub\wwwroot\EGIC_CRM.Web\LogError.txt", true);
        StreamWriter sr = new StreamWriter(@"E:\EGIC Sources\EGIC_CRM.Web\LogError.txt", true);
        string txt = "Time: " + DateTime.Now.ToString() + "\r\n";
        txt += "Function Name: " + functionName + "\r\n";
        txt += "Error Message: " + msg + "\r\n";
        txt += "================================= *** =================================\r\n";
        sr.WriteLine(txt);
        sr.Close();
    }

    #endregion
}