﻿using System.Collections.Generic;

public static class CurrentContext
{
    public static string CurrentUsername { get; set; }
    public static string CurrentPageUrl { get; set; }
    public static List<MainMenuItem> MainMenuItems { get; set; }
}