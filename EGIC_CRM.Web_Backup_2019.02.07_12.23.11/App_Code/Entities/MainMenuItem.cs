﻿using System.Collections.Generic;
using System.Data;

public class MainMenuItem
{
    public MainMenuItem()
    {
        SubItems = new List<MainMenuItem>();
    }

    public string Text { get; set; }
    public string IconClass { get; set; }
    public string URL { get; set; }
    public bool Active { get; set; }

    public List<MainMenuItem> SubItems { get; set; }
}

public class ModuleRow
{
    public ModuleRow()
    {
    }

    public ModuleRow(DataRow row)
    {
        ModuleText = row["MODULE_NAME"].ToString();
        MainMenuText = row["MAIN_MENU_TEXT"].ToString();
        MenuItemText = row["MENU_ITEM_TEXT"].ToString();
        MenuItemUrl = row["MENU_ITEM_URL"].ToString();
    }

    public string ModuleText { get; set; }
    public string MainMenuText { get; set; }
    public string MenuItemText { get; set; }
    public string MenuItemUrl { get; set; }
}