﻿<%@ Page Title="أنواع الإستفسارات"  Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="CRM_QUERY_TYPES.aspx.cs" Inherits="Modules_EG_CRM_QUERY_TYPES" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
         <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active">
                  <a href="#tab_1" data-toggle="tab">
                      ادخال الاستفسارات
                  </a>
              </li>
              <li>
                  <a href="#tab_2" data-toggle="tab">
                      بيانات الاستفسارات
                  </a>
              </li>
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="tab_1">
                    <!-- general form elements -->
                <div class="form-group">
                    <label for="txt_query">الإستفسار</label>
                    <asp:TextBox ID="txt_query" runat="server" CssClass="form-control" placeholder="الإستفسار"></asp:TextBox>
                </div>

                <div class="form-group">
                    <label for="ddl_query_class">التصنيف</label>
                    <asp:DropDownList ID="ddl_query_class" runat="server" CssClass="form-control btn-sm">
                        <asp:ListItem Value="0">إختر التصنيف</asp:ListItem>
                        <asp:ListItem Value="Q">استفسار</asp:ListItem>
                        <asp:ListItem Value="C">شكوي</asp:ListItem>
                        <asp:ListItem Value="S">اقتراح</asp:ListItem>
                    </asp:DropDownList>
                </div>

                <div class="form-group">
                    <label for="ddl_active">نشط</label>
                    <asp:DropDownList ID="ddl_active" runat="server" CssClass="form-control btn-sm">
                        <asp:ListItem Value="Y">نعم</asp:ListItem>
                        <asp:ListItem Value="N">لا</asp:ListItem>
                    </asp:DropDownList>
                </div>
                  
                <div class="form-group">
                    <label for="ddl_dept">الإدارة</label>
                    <asp:DropDownList ID="ddl_dept" runat="server" CssClass="form-control btn-sm">
                        <asp:ListItem Value="0">إختر الإدارة</asp:ListItem>
                        <asp:ListItem Value="1">خدمة العملاء</asp:ListItem>
                        <asp:ListItem Value="2">المبيعات</asp:ListItem>
                        <asp:ListItem Value="3">التسويق</asp:ListItem>
                        <asp:ListItem Value="4">المصنع</asp:ListItem>
                    </asp:DropDownList>
                </div>

                <div class="form-group">
                    <label for="ddl_type">نوع الشكوي</label>
                    <asp:DropDownList ID="ddl_type" runat="server" CssClass="form-control btn-sm">
                        <asp:ListItem Value="0">إختر نوع الشكوي</asp:ListItem>
                        <asp:ListItem Value="1">مندوب</asp:ListItem>
                        <asp:ListItem Value="2">مشرف</asp:ListItem>
                        <asp:ListItem Value="3">شركة</asp:ListItem>
                        <asp:ListItem Value="4">منتج</asp:ListItem>
                    </asp:DropDownList>
                </div>

                  <div class="checkbox">
                      <label>
                          <input type="checkbox" runat="server" ID="chk_send_mail" /> 
                          إرسال ميل
                      </label>
                  </div>
                  <div class="form-group">
                  <asp:Button ID="btn_save" runat="server" Text="حفظ" CssClass="btn btn-primary" OnClick="btn_save_Click" />
                  <asp:Button ID="btn_query" runat="server" Text="إستعلام" CssClass="btn btn-info" OnClick="btn_query_Click" />
                </div>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                 
                 <%-- <asp:GridView ID="GridView1" runat="server"></asp:GridView>--%>
                  <telerik:RadGrid ID="RadGrid1" runat="server" AllowFilteringByColumn="True" AllowPaging="True" AllowSorting="True" CellSpacing="0" GridLines="None" ShowGroupPanel="True" AutoGenerateColumns="False" OnNeedDataSource="RadGrid1_NeedDataSource" OnItemDataBound="RadGrid1_ItemDataBound" OnDeleteCommand="RadGrid1_DeleteCommand" OnUpdateCommand="RadGrid1_UpdateCommand" OnItemCreated="RadGrid1_ItemCreated"  OnItemCommand="RadGrid1_ItemCommand" Skin="Material">
                            <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
                            </HeaderContextMenu>
                            <MasterTableView CommandItemDisplay="Top" AllowPaging="true">
                                <CommandItemSettings ShowExportToCsvButton="true" ShowExportToExcelButton="true" ShowExportToWordButton="true" ShowAddNewRecordButton="false" ShowExportToPdfButton="false" />
                                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridButtonColumn ButtonType="ImageButton"  CommandName="UPDATE" FilterControlAltText="Filter column column" Text="تعديل" UniqueName="column" ImageUrl="~/Content/images/edit.gif">
                                        <ItemStyle BackColor="Transparent" BorderStyle="Solid" Font-Bold="True" Font-Names="Arial" Font-Size="10pt" ForeColor="Black" HorizontalAlign="Center" />
                                    </telerik:GridButtonColumn>
                                    <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="DELETE" FilterControlAltText="Filter column1 column" ImageUrl="~/Content/images/delete.gif" Text="حذف" UniqueName="column1" ConfirmDialogHeight="200px" ConfirmDialogType="RadWindow" ConfirmDialogWidth="400px" ConfirmText="هل تريد حذف هذه البيانات ؟" ConfirmTitle="حذف البيانات">
                                    </telerik:GridButtonColumn>

                                    <telerik:GridTemplateColumn DataField="NAME" FilterControlAltText="Filter NAME column" GroupByExpression="NAME Group By NAME" HeaderText="الإستفسار / الشكوي" SortExpression="NAME" UniqueName="NAME">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="IDTextBox" Enabled="false" runat="server" Text='<%# Bind("NAME") %>'></asp:TextBox></EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="IDLabel" runat="server" Text='<%# Eval("NAME") %>'></asp:Label></ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn DataField="ACTIVE" FilterControlAltText="Filter ACTIVE column" Visible="false" GroupByExpression="ACTIVE Group By ACTIVE" HeaderText="الحالة" SortExpression="ACTIVE" UniqueName="ACTIVE">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="ACTIVETextBox" runat="server" Text='<%# Bind("ACTIVE") %>'></asp:TextBox></EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ACTIVELabel" runat="server" Text='<%# Eval("ACTIVE") %>'></asp:Label></ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn DataField="active_desc" FilterControlAltText="Filter active_desc column" GroupByExpression="active_desc Group By active_desc" HeaderText="الحالة " SortExpression="active_desc" UniqueName="active_desc">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="active_descTextBox" Enabled="true" runat="server" Text='<%# Bind("active_desc") %>'></asp:TextBox></EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="active_descLabel" runat="server" Text='<%# Eval("active_desc") %>'></asp:Label></ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn DataField="qry_type_desc" FilterControlAltText="Filter qry_type_desc column" GroupByExpression="qry_type_desc Group By qry_type_desc" HeaderText="التصنيف" SortExpression="qry_type_desc" UniqueName="qry_type_desc">
                                      
                                        <ItemTemplate>
                                            <asp:Label ID="MNAMELabel" runat="server" Text='<%# Eval("qry_type_desc") %>'></asp:Label></ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    
                                     <telerik:GridTemplateColumn DataField="qry_type" Visible="false" FilterControlAltText="Filter qry_type column" GroupByExpression="qry_type Group By qry_type" HeaderText="التصنيف" SortExpression="qry_type" UniqueName="qry_type">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="qry_typeTextBox" Enabled="false" runat="server" Text='<%# Bind("qry_type") %>'></asp:TextBox></EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="qry_typeLabel" runat="server" Text='<%# Eval("qry_type") %>'></asp:Label></ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    
                                     <telerik:GridTemplateColumn DataField="serv_dept" FilterControlAltText="Filter serv_dept column" GroupByExpression="serv_dept Group By serv_dept" HeaderText="الإدارة المدعومة" SortExpression="serv_dept" UniqueName="serv_dept">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="serv_deptTextBox" Enabled="false" runat="server" Text='<%# Bind("serv_dept") %>'></asp:TextBox></EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="serv_deptLabel" runat="server" Text='<%# Eval("serv_dept") %>'></asp:Label></ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    
                                    <telerik:GridTemplateColumn DataField="sendM_desc" FilterControlAltText="Filter sendM_desc column" GroupByExpression="sendM_desc Group By sendM_desc" HeaderText="حالة الإرسال" SortExpression="sendM_desc" UniqueName="sendM_desc">
                                       
                                        <ItemTemplate>
                                            <asp:Label ID="sendM_descLabel" runat="server" Text='<%# Eval("sendM_desc") %>'></asp:Label></ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                       <telerik:GridTemplateColumn DataField="send_mail" Visible="false" FilterControlAltText="Filter send_mail column" GroupByExpression="send_mail GROUP BY send_mail" HeaderText="send_mail" SortExpression="send_mail" UniqueName="send_mail">
                                        
                                        <ItemTemplate>
                                            <asp:Label ID="send_mailLabel" runat="server" Text='<%# Eval("send_mail") %>'></asp:Label></ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn DataField="comp_class"  FilterControlAltText="Filter comp_class column" GroupByExpression="comp_class GROUP BY comp_class" HeaderText="نوع الشكوي" SortExpression="comp_class" UniqueName="comp_class">
                                       
                                        <ItemTemplate>
                                            <asp:Label ID="comp_classLabel" runat="server" Text='<%# Eval("comp_class") %>'></asp:Label></ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                            </MasterTableView><ValidationSettings EnableModelValidation="false" EnableValidation="false" ValidationGroup="10" />
                            <ClientSettings AllowColumnsReorder="True" AllowDragToGroup="True" ReorderColumnsOnClient="True">
                                <Selecting AllowRowSelect="True" />
                                <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                            </ClientSettings>
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>


              </div>
              <!-- /.tab-pane -->

            </div>
            <!-- /.tab-content -->
          </div>
        </div>
      </div>

</asp:Content>


