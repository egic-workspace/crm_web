﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Modules_EG_CRM_QUERY_TYPES : System.Web.UI.Page
{
    private readonly EGService _egService;
    DataSet Ds = new DataSet();

    public Modules_EG_CRM_QUERY_TYPES()
    {
        _egService = new EGService();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            Fill_Grid();
        }
    }

    protected void btn_save_Click(object sender, EventArgs e)
    {
        
    }
    private void Fill_Grid()
    {
        try
        {
           
            Ds.Tables.Clear();
            Ds = _egService.Bind_Query_Grid();
            RadGrid1.DataSource = Ds;
            RadGrid1.DataBind();
        }
        catch
        {

        }
    }
    protected void btn_query_Click(object sender, EventArgs e)
    {

    }

    protected void RadGrid1_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {

    }

    protected void RadGrid1_ItemCreated(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {

    }

    protected void RadGrid1_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {

    }

    protected void RadGrid1_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        Fill_Grid();
    }

    protected void RadGrid1_UpdateCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {

    }

    protected void RadGrid1_DeleteCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {

    }
}