﻿<%@ Page Title="السباكين" Language="C#" AutoEventWireup="true" CodeFile="EG_PLUMBERS.aspx.cs" Inherits="Modules_EG_EG_PLUMBERS" MasterPageFile="~/Main.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
         <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active">
                  <a href="#tab_1" data-toggle="tab">
                      ادخال سباك
                  </a>
              </li>
              <li>
                  <a href="#tab_2" data-toggle="tab">
                      بيانات السباكين
                  </a>
              </li>
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="tab_1">

                <div class="form-group">
                    <label for="txt_plumber_name">اسم السباك</label>
                    <asp:TextBox ID="txt_plumber_name" runat="server" CssClass="form-control" placeholder="اسم السباك"></asp:TextBox>
                </div>
                  
                  <div class="form-group">
                    <label for="txt_plumber_nickname">اسم الشهرة</label>
                    <asp:TextBox ID="txt_plumber_nickname" runat="server" CssClass="form-control" placeholder="اسم الشهرة"></asp:TextBox>
                </div>
                  
                  <div class="form-group">
                    <label for="txt_plumber_place">مكان التواجد</label>
                    <asp:TextBox ID="txt_plumber_place" runat="server" CssClass="form-control" placeholder="مكان التواجد"></asp:TextBox>
                </div>
                  
                <div class="form-group">
                    <label for="txt_plumber_phone_2">عنوان المنزل</label>
                    <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" placeholder="عنوان المنزل"></asp:TextBox>
                </div>

                <div class="form-group">
                    <label for="txt_plumber_mobile">الموبايل</label>
                    <asp:TextBox ID="txt_plumber_mobile" runat="server" CssClass="form-control" placeholder="الموبايل"></asp:TextBox>
                </div>
                  
                <div class="form-group">
                    <label for="txt_plumber_phone">التليفون</label>
                    <asp:TextBox ID="txt_plumber_phone" runat="server" CssClass="form-control" placeholder="التليفون"></asp:TextBox>
                </div>

                <div class="form-group">
                    <label for="txt_plumber_phone_2">تليفون إضافي</label>
                    <asp:TextBox ID="txt_plumber_phone_2" runat="server" CssClass="form-control" placeholder="تليفون إضافي"></asp:TextBox>
                </div>
                  
                <div class="form-group">
                    <label for="ddl_query_class">الفرع</label>
                    <asp:DropDownList ID="ddl_query_class" runat="server" CssClass="form-control btn-sm">
                        <asp:ListItem Value="">إختر الفرع</asp:ListItem>
                    </asp:DropDownList>
                </div>

                <div class="form-group">
                    <label for="ddl_query_class">المحافظة</label>
                    <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control btn-sm">
                        <asp:ListItem Value="">إختر المحافظة</asp:ListItem>
                    </asp:DropDownList>
                </div>

                <div class="form-group">
                    <label for="ddl_query_class">الحي</label>
                    <asp:DropDownList ID="DropDownList3" runat="server" CssClass="form-control btn-sm">
                        <asp:ListItem Value="">إختر الحي</asp:ListItem>
                    </asp:DropDownList>
                </div>

                <div class="form-group">
                    <label for="ddl_query_class">المحل</label>
                    <asp:DropDownList ID="DropDownList2" runat="server" CssClass="form-control btn-sm">
                        <asp:ListItem Value="">إختر المحل</asp:ListItem>
                    </asp:DropDownList>
                </div>

                <div class="form-group">
                    <label for="ddl_active">نشط</label>
                    <asp:DropDownList ID="ddl_active" runat="server" CssClass="form-control btn-sm">
                        <asp:ListItem Value="Y">نعم</asp:ListItem>
                        <asp:ListItem Value="N">لا</asp:ListItem>
                    </asp:DropDownList>
                </div>
                  
                <div class="form-group">
                    <label for="ddl_dept">الإدارة</label>
                    <asp:DropDownList ID="ddl_dept" runat="server" CssClass="form-control btn-sm">
                        <asp:ListItem Value="0">إختر الإدارة</asp:ListItem>
                        <asp:ListItem Value="1">خدمة العملاء</asp:ListItem>
                        <asp:ListItem Value="2">المبيعات</asp:ListItem>
                        <asp:ListItem Value="3">التسويق</asp:ListItem>
                        <asp:ListItem Value="4">المصنع</asp:ListItem>
                    </asp:DropDownList>
                </div>

                <div class="form-group">
                    <label for="ddl_type">نوع الشكوي</label>
                    <asp:DropDownList ID="ddl_type" runat="server" CssClass="form-control btn-sm">
                        <asp:ListItem Value="0">إختر نوع الشكوي</asp:ListItem>
                        <asp:ListItem Value="1">مندوب</asp:ListItem>
                        <asp:ListItem Value="2">مشرف</asp:ListItem>
                        <asp:ListItem Value="3">شركة</asp:ListItem>
                        <asp:ListItem Value="4">منتج</asp:ListItem>
                    </asp:DropDownList>
                </div>

                  <div class="checkbox">
                      <label>
                          <input type="checkbox" runat="server" ID="chk_send_mail" /> 
                          إرسال ميل
                      </label>
                  </div>
                  <div class="form-group">
                  <asp:Button ID="btn_save" runat="server" Text="حفظ" CssClass="btn btn-primary" OnClick="btn_save_Click" />
                </div>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                  <asp:GridView ID="GridView1" runat="server"></asp:GridView>
              </div>
              <!-- /.tab-pane -->

            </div>
            <!-- /.tab-content -->
          </div>
        </div>
      </div>
</asp:Content>
