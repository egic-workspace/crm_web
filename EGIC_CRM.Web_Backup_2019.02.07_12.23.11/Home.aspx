﻿<%@ Page Title="اختر القسم" Language="C#" MasterPageFile="~/Login.Master" AutoEventWireup="true" CodeFile="Home.aspx.cs" Inherits="Home" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <link href="/Content/css/select2.min.css" rel="stylesheet" />
    <style type="text/css">
        .select2-container--default .select2-selection--single{
            border-radius: 0 3px 3px 0!important;
        }
    </style>
    <div class="login-box" style="margin: 0 auto 0 auto!important;">
        <a href="/" class="login-logo">
            <img src="Content/images/EGIC.jpg" style="width: 100%" />
            <div style="font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif!important;">
                <strong style="color: #725f58!important;">EGIC</strong>
                <small style="color: #f28b2f!important;">CRM</small>
            </div>
        </a>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg text-center">
                <strong>نظام إدارة خدمة العملاء</strong>
                <br />
                <br />
                اختر النظام
            </p>
             
            <form runat="server">
                <div class="row">
                    <div class="input-group input-group-sm">
                        <asp:DropDownList runat="server" ID="ddlModuls" class="form-control select2" Style="width:100%;">
                        </asp:DropDownList>
                        <span class="input-group-btn">
                            <asp:Button runat="server" ID="btnOpenUrl" class="btn btn-success btn-flat" Text="فتح!" style="border-radius: 3px 0 0 3px!important;height: 28px;" OnClick="btnOpenUrl_Click"/>
                        </span>
                    </div>
                </div>
                <script src="/Content/js/select2.full.min.js"></script>
                <script type="text/javascript">
                    $(function () {
                        $('.select2').select2()
                    })
                </script>
            </form>

        </div>
        <!-- /.login-box-body -->
    </div>


</asp:Content>
