﻿using System;
using System.Collections.Generic;
using System.Linq;

public partial class Main : System.Web.UI.MasterPage
{
    private readonly UserService _userService;

    public Main()
    {
        _userService = new UserService();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrWhiteSpace(CurrentContext.CurrentUsername))
            Response.Redirect("~/Default");

        if (!this.IsPostBack)
        {
            //CurrentContext.MainMenuItems = (List<MainMenuItem>)Session["UserModuleMenuItems"];
            //else
            if (CurrentContext.MainMenuItems == null)
            {
                string moduleName = "";
                var urlPathParts = Context.Request.Path.Split('/');
                for (int i = 0; i < urlPathParts.Length; i++)
                    if (urlPathParts[i].ToUpper() == "MODULES")
                    {
                        moduleName = urlPathParts[i + 1];
                        break;
                    }
                CurrentContext.MainMenuItems = _userService.GetUserMainMenuItems(CurrentContext.CurrentUsername, moduleName);
                //Session["UserModuleMenuItems"] = CurrentContext.MainMenuItems;
            }
            lblUsername.Text = CurrentContext.CurrentUsername;
            var CurrentUrlParts = Request.Url.AbsolutePath.Split('/');
            CurrentContext.CurrentPageUrl = CurrentUrlParts[CurrentUrlParts.Length - 1].Replace(".aspx", "");
        }
    }

    protected void BtnLogout_Click(object sender, EventArgs e)
    {
        CurrentContext.CurrentUsername = string.Empty;
        CurrentContext.MainMenuItems = null;
        //Session["UserModuleMenuItems"] = null;
        Response.Redirect("~/Default");
    }
}