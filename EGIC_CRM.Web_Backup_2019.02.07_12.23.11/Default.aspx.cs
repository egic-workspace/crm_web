﻿
using System;
using System.Linq;
using System.Web.UI;

public partial class _Default : Page
{
    #region Fields

    private readonly UserService _userService;

    #endregion

    #region CTOR

    public _Default()
    {
        _userService = new UserService();
    }

    #endregion

    #region Utilities

    private string ValidateCredential(string username, string password)
    {
        if (string.IsNullOrWhiteSpace(username) || string.IsNullOrWhiteSpace(password))
            return "يجب ادخال اسم المستخدم وكلمة المرور";

        string errorMessage = _userService.ValidateCredential(username, password);
        return errorMessage;

    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        string username = txtUsername.Text,
            password = txtPassword.Text;

        string errorMessage = ValidateCredential(username, password);
        if (!string.IsNullOrWhiteSpace(errorMessage))
        {
            lblErrorMessage.Visible = true;
            lblErrorMessage.InnerText = errorMessage;
            return;
        }

        CurrentContext.CurrentUsername = username;
        Response.Redirect("Home.aspx");
    }
}